#!/bin/bash

## Script for building Xen from source

XEN_ROOT="/local/sme/workspace/xen-4.10.0"

## Dependencies for building xen from source:
## https://wiki.xen.org/wiki/Compiling_Xen_From_Source#Build_Dependencies_-_Debian_.2F_Ubuntu

echo "==== Installing dependencies ===="
(
sudo apt install bcc bin86 gawk bridge-utils iproute libcurl3 libcurl4-openssl-dev bzip2 module-init-tools; #transfig tgif;
sudo apt install texinfo texlive-latex-base texlive-latex-recommended texlive-fonts-extra texlive-fonts-recommended pciutils-dev;
sudo apt install make gcc libc6-dev zlib1g-dev python python-dev python-twisted libncurses5-dev patch libsdl-dev libjpeg62-dev; #libvncserver-dev 
sudo apt install iasl libbz2-dev e2fslibs-dev git-core uuid-dev ocaml ocaml-findlib libx11-dev bison flex xz-utils libyajl-dev;
sudo apt install gettext libpixman-1-dev libaio-dev markdown pandoc
## not mentioned in the link, but was required
sudo apt install liblzma-dev
#sudo apt install libnl-3-dev libnl-route-3-dev
sudo apt install libsystemd-dev
## required for libc6-dev-i386 for sys/cdefs.h
sudo apt install gcc-multilib
) | tee installdep.out 2>&1

## configure
echo "==== Run configure ===="
echo "./configure --enable-systemd > config.out 2>&1"
./configure --enable-systemd > config.out 2>&1

## first build and install from this script itself
echo "==== Compiling ===="
$XEN_ROOT/xbuild.sh
echo "==== Installing ===="
$XEN_ROOT/xinstall.sh

## post-installation
## need to remove xendomains in init.d
## bug mentioned @ https://blog.werk21.de/en/2018/02/08/build-xen-hypervisor-410-and-xen-tools-ubuntu-1604-pvh
#sudo su
echo "==== Fixing grub config ===="
sudo sed -i '/\#GRUB_CMDLINE_XEN=\"\"/a GRUB_CMDLINE_XEN=\"psr=cmt:1,cat:1 loglvl=all guest_loglvl=all com1=115200,8n1,0x2f8,4 console=com1,vga noreboot\"' /etc/default/grub.d/xen.cfg
sudo sed -i '/GRUB_CMDLINE_XEN=\"psr/a GRUB_DISABLE_OS_PROBER=true' /etc/default/grub.d/xen.cfg
sudo sed -i '/\#GRUB_CMDLINE_LINUX_XEN_REPLACE=/a GRUB_CMDLINE_LINUX_XEN_REPLACE=\"\$GRUB_CMDLINE_LINUX console=hvc0 console=ttyS1,115200n8 earlyprintk=serial,ttyS1,115200n8\"' /etc/default/grub.d/xen.cfg
echo "sudo update-grub"
sudo update-grub

echo "==== Updating ldconfig ===="
sudo /sbin/ldconfig

echo "==== Setting systemctl services ===="
sudo rm /etc/init.d/xendomains

sudo systemctl enable xen-qemu-dom0-disk-backend.service
sudo systemctl enable xen-init-dom0.service
sudo systemctl enable xenconsoled.service
sudo systemctl enable xenstored.service
sudo systemctl enable xendomains.service

echo "==== Finished xen setup ===="
