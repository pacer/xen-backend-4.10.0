#include "timerobj.h"
#include "xprofile.h"
#include "xdebug.h"
#include "xstats.h"
#include "xstats_decl.h"
#include "sme_guest.h"
#include "pacer_obj.h"


void
timer_obj_set_profile(timer_obj_t *tobj, uint8_t type, profq_elem_t *gprof,
    sme_htable_t *pmap_htbl, nic_txdata_t **nic_txdata_ptr_arr)
{
  if (!tobj)
    return;

  tobj->true_expires = 0;
  tobj->nic_txdata_ptr_arr = nic_txdata_ptr_arr;
  tobj->profq_addr = gprof;
  if (gprof != NULL)
    init_xprofile(&tobj->xen_profp, gprof, pmap_htbl);
  tobj->timer_idx = -1;
  tobj->type = type;
}

void
timer_obj_set_timer(timer_obj_t *tobj, uint64_t true_expires,
    int16_t timer_idx, s_time_t early_delta)
{
  if (!tobj)
    return;

  tobj->true_expires = true_expires;
  tobj->timer_idx = timer_idx;
  set_timer(&tobj->xtimer, true_expires - early_delta);
}

void
timer_obj_set_timestamp(timer_obj_t *tobj, uint64_t true_expires,
    int16_t timer_idx, ptimer_heap_t *pth)
{
  if (!tobj)
    return;

  tobj->true_expires = true_expires;
  tobj->timer_idx = timer_idx;
  set_ptimer(pth, &tobj->ptimer, true_expires, tobj);
}

void
reset_timer_obj(timer_obj_t *tobj)
{
  unsigned long xprof_flag = 0;

  if (!tobj)
    return;

#if CONFIG_TIMERQ_SPINLOCK
  spin_lock_irqsave(&tobj->xen_profp.timers_lock, xprof_flag);
#elif CONFIG_SAVE_IRQ
  local_irq_save(xprof_flag);
#endif

  cleanup_xprofile_unlocked(&tobj->xen_profp);

#if CONFIG_TIMERQ_SPINLOCK
  spin_unlock_irqrestore(&tobj->xen_profp.timers_lock, xprof_flag);
#elif CONFIG_SAVE_IRQ
  local_irq_restore(xprof_flag);
#endif

  tobj->true_expires = -1;
  tobj->profq_addr = NULL;
  tobj->timer_idx = -1;
  tobj->type = 0;
}

int
init_timerq(timerq_t *tq, int qsize, void (*timer_fn) (void *))
{
  int i;

  if (!tq)
    return -EINVAL;

  tq->queue = xzalloc_array(timer_obj_t, qsize);
  if (!tq->queue)
    return -ENOMEM;

  atomic_set(&tq->first, 0);
  tq->qsize = qsize;

  for (i = 0; i < qsize; i++) {
    init_timer(&(tq->queue[i].xtimer), timer_fn, &(tq->queue[i]), XEN_PACER_CORE);
    atomic_set(&(tq->queue[i].next), i+1);
    memset((void *) &(tq->queue[i].ptimer), 0, sizeof(ptimer_t));
  }

  atomic_set(&(tq->queue[qsize-1].next), -1);

  return 0;
}

int
cleanup_timerq(ptimer_heap_t *pth, timerq_t *tq)
{
  int i;
  if (!tq)
    return -EINVAL;

  if (tq->queue && tq->qsize > 0) {
    for (i = 0; i < tq->qsize; i++) {
      kill_timer(&(tq->queue[i].xtimer));
      remove_from_ptimer_heap(pth, &tq->queue[i].ptimer);
      reset_timer_obj(&(tq->queue[i]));
    }

    xfree(tq->queue);
  }

  return 0;
}

int
pop_one_timer(timerq_t *tq, int32_t *idx)
{
  int ret = 0;
  int curr_first = 0;
  int first_next = 0;

  if (!tq || !tq->queue)
    return -EINVAL;

retry:
  curr_first = atomic_read(&tq->first);
  if (curr_first < 0) {
    xprintk(LVL_EXP, "%s", "Out of free timers!!");
    return curr_first;
  }

  first_next = atomic_read(&(tq->queue[curr_first].next));
  ret = atomic_cmpxchg(&tq->first, curr_first, first_next);
  if (ret != curr_first)
    goto retry;

  *idx = curr_first;
  atomic_set(&(tq->queue[curr_first].next), -1);

  xprintk(LVL_DBG, "caller %pS pop idx %d new first %d"
      , __builtin_return_address(0), *idx, atomic_read(&tq->first));
  return 0;
}

int
push_one_timer(timerq_t *tq, int32_t idx)
{
  int ret = 0;
  int curr_first = 0;
  timer_obj_t *ins_tobj = NULL;

  if (!tq || !tq->queue)
    return -EINVAL;

  ins_tobj = &tq->queue[idx];

retry:
  curr_first = atomic_read(&tq->first);
  atomic_set(&ins_tobj->next, curr_first);
  ret = atomic_cmpxchg(&tq->first, curr_first, idx);
  if (ret != curr_first)
    goto retry;

  xprintk(LVL_DBG, "caller %pS old first %d push idx %d"
      , __builtin_return_address(0), curr_first, atomic_read(&tq->first));
  return 0;
}

void
init_active_timerq(active_timerq_t *atq, timer_obj_t *queue)
{
  if (!atq || !queue)
    return;

  atq->queue = queue;
  atomic_set(&atq->first, -1);
  spin_lock_init(&atq->active_lock);
}

void
cleanup_active_timerq(ptimer_heap_t *pth, active_timerq_t *atq, timerq_t *tq)
{
  int32_t next_idx;
  int32_t next_idx2;
  int cnt = 0;
  timer_obj_t *tobj = NULL;
#if SME_DEBUG_LVL <= LVL_INFO
  int guest_prof_slot_idx = 0;
  profq_elem_t *gprof = NULL;
  xprofile_t *xprof = NULL;
  char x_out_ipstr[16], x_dst_ipstr[16];
  uint16_t x_out_port = 0, x_dst_port = 0;
#endif

  if (!atq || !tq)
    return;

#if CONFIG_TIMERQ_SPINLOCK
  // critical section 0
  spin_lock(&atq->active_lock);
#endif

  next_idx = atomic_read(&atq->first);
  while (next_idx >= 0) {
    tobj = &atq->queue[next_idx];
    next_idx2 = atomic_read(&tobj->next);
    // critical section 1: spin lock on xprofile
    kill_timer(&(tobj->xtimer));
    remove_from_ptimer_heap(pth, &tobj->ptimer);
#if SME_DEBUG_LVL <= LVL_INFO
    gprof = tobj->profq_addr;
    xprof = &tobj->xen_profp;
    guest_prof_slot_idx = gprof->profq_idx;
    ipstr(xprof->conn_id.out_ip, x_out_ipstr);
    ipstr(xprof->conn_id.dst_ip, x_dst_ipstr);
    x_out_port = xprof->conn_id.out_port;
    x_dst_port = xprof->conn_id.dst_port;
    xprintk(LVL_INFO, "Free T%d G%d [%s %u, %s %u] state %d NXT %d CWM %d P %d "
        "R %d D %d max_seqno %u"
        , next_idx, guest_prof_slot_idx, x_out_ipstr, x_out_port
        , x_dst_ipstr, x_dst_port, gprof->state, gprof->next_timer_idx
        , gprof->cwnd_watermark, gprof->num_timers
        , atomic_read(&gprof->real_counter), gprof->dummy_counter, gprof->max_seqno
        );
#endif
    reset_timer_obj(tobj);

    // critical section 2: cmpxchg
    push_one_timer(tq, next_idx);
    next_idx = next_idx2;
    cnt++;
  }

#if CONFIG_TIMERQ_SPINLOCK
  spin_unlock(&atq->active_lock);
#endif

  xprintk(LVL_INFO, "Cleaned %d elems in active timerq", cnt);
}

// caller holds atq->active_lock
int
active_timerq_insert_idx_at_head(active_timerq_t *atq, int32_t tobj_idx)
{
  int before_first = 0;

  if (!atq)
    return -EINVAL;

  before_first = atomic_read(&atq->first);
  atomic_set(&(atq->queue[tobj_idx].next), atomic_read(&atq->first));
  atomic_set(&atq->first, tobj_idx);

  return 0;
}

// caller holds atq->active_lock
int
active_timerq_remove_idx_unlocked(active_timerq_t *atq, int32_t rm_idx)
{
  int ret = 0;
  int found = 0;
  int32_t next_idx = 0;
  int32_t prev_idx = 0;
  timer_obj_t *prev_tobj = NULL;
  timer_obj_t *rm_tobj = NULL;

  if (!atq)
    return -EINVAL;

  rm_tobj = &atq->queue[rm_idx];

  // active list is empty
  if (atomic_read(&atq->first) < 0)
    goto exit;

  prev_idx = -1;
  next_idx = atomic_read(&atq->first);
  while (next_idx >= 0) {
    if (next_idx == rm_idx) {
      found = 1;
      break;
    }

    prev_idx = next_idx;
    prev_tobj = &atq->queue[next_idx];
    next_idx = atomic_read(&prev_tobj->next);
  }

  // this should not happen, but we just return an error for now
  if (!found) {
    ret = -ENOENT;
    goto exit;
  }

  ret = 0;

  // freeing first element in active list
  if (prev_idx == -1) {
    atomic_set(&atq->first, atomic_read(&rm_tobj->next));
    atomic_set(&rm_tobj->next, -1);
    goto exit;
  }

  // freeing non-first element in active list
  atomic_set(&prev_tobj->next, atomic_read(&rm_tobj->next));
  atomic_set(&rm_tobj->next, -1);

exit:

  return ret;
}

int
active_timerq_remove_idx(active_timerq_t *atq, int32_t rm_idx)
{
  int ret = 0;

  spin_lock(&atq->active_lock);

  ret = active_timerq_remove_idx_unlocked(atq, rm_idx);

  spin_unlock(&atq->active_lock);

  return ret;
}

int
active_timerq_update_profile(void *pobj_p, active_timerq_t *atq, timerq_t *tq,
    profq_elem_t *gprof, sme_htable_t *pmap_htbl, int prof_extn,
    s_time_t epoch, ptimer_heap_t *pth)
{
  timer_obj_t *lookup_tobj = NULL;
//  int32_t next_idx = 0;
//  int found = 0;
  int ret = 0;
  xprofile_t *xprof = NULL;
  uint64_t next_ts = 0;
  uint64_t prev_ts = 0;
  unsigned long atq_flag = 0;
#if CONFIG_TIMERQ_SPINLOCK
  unsigned long prof_flag = 0;
#endif

#if CONFIG_HEAP_SPINLOCK
  unsigned long heap_flag = 0;
#endif

#if SME_DEBUG_LVL <= LVL_DBG
  int guest_prof_slot_idx = 0;
  uint64_t old_prev_ts = 0;
  char x_out_ipstr[16];
  char x_dst_ipstr[16];
  char out_ipstr[16];
  char dst_ipstr[16];
  char x_hash[64];
  char hash[64];
#endif

  pacer_obj_t *pobj = (pacer_obj_t *) pobj_p;

  SME_INIT_TIMER(heap_insert_delay);
  SME_INIT_TIMER(heap_remove_delay);

  if (!atq || !gprof || !pmap_htbl)
    return -EINVAL;

#if CONFIG_TIMERQ_SPINLOCK
  spin_lock_irqsave(&atq->active_lock, atq_flag);
//  xprintk(LVL_DBG, "%s", "ATQ LOCK");
#elif CONFIG_SAVE_IRQ
  local_irq_save(atq_flag);
#endif

#if 1
  lookup_tobj = &tq->queue[gprof->tobj_idx];
#else
  next_idx = atomic_read(&atq->first);
  while (next_idx >= 0) {
    lookup_tobj = &atq->queue[next_idx];
    if (lookup_tobj->profq_addr == gprof) {
      found = 1;
      break;
    }

    next_idx = atomic_read(&lookup_tobj->next);
  }

  if (!found) {
#if CONFIG_TIMERQ_SPINLOCK
//    xprintk(LVL_DBG, "%s", "ATQ UNLOCK");
    spin_unlock_irqrestore(&atq->active_lock, atq_flag);
#elif CONFIG_SAVE_IRQ
    local_irq_restore(atq_flag);
#endif

    return -ENOENT;
  }
#endif

  xprof = &lookup_tobj->xen_profp;

#if CONFIG_TIMERQ_SPINLOCK
  spin_lock_irqsave(&xprof->timers_lock, prof_flag);
//  xprintk(LVL_DBG, "%s", "PROF LOCK");
#endif

  ret = replace_xprofile(xprof, gprof, pmap_htbl, prof_extn, epoch);
  if (ret < 0) {
#if CONFIG_TIMERQ_SPINLOCK
//    xprintk(LVL_DBG, "%s", "PROF UNLOCK");
    spin_unlock_irqrestore(&xprof->timers_lock, prof_flag);
//    xprintk(LVL_DBG, "%s", "ATQ UNLOCK");
    spin_unlock_irqrestore(&atq->active_lock, atq_flag);
#elif CONFIG_SAVE_IRQ
    local_irq_restore(atq_flag);
#endif

    return ret;
  }

#if SME_DEBUG_LVL <= LVL_DBG
  // values before profile update
  ipstr(xprof->conn_id.out_ip, x_out_ipstr);
  ipstr(xprof->conn_id.dst_ip, x_dst_ipstr);
  ipstr(gprof->conn_id.out_ip, out_ipstr);
  ipstr(gprof->conn_id.dst_ip, dst_ipstr);
  prt_hash(xprof->prof_id.byte, sizeof(ihash_t), x_hash);
  prt_hash(gprof->id_ihash.byte, sizeof(ihash_t), hash);
  ret = xprofile_get_prev_ts_unlocked(xprof, gprof, &old_prev_ts);
#endif

  // profile switch from default to custom due to marker
  if (!prof_extn) {

    // switch porfile
    reset_xprofile_unlocked(xprof, gprof, pmap_htbl, PIDR_REPLACED);

    /*
     * if previous timer has not expired, update timestamp, since in the
     * replaced profile, the timestamp might be earlier than that in the
     * default profile. if previous timer has expired, we need not do
     * anything more, and simply return. either the profile has not been
     * replaced above, because it's too late, or the next timer is already
     * picked from the replaced profile (XXX: check the last part).
     */
    ret = xprofile_get_prev_ts_unlocked(xprof, gprof, &prev_ts);
    if (ret >= 0 && !is_timeslot_expired(prev_ts, epoch - EPOCH_SIZE)) {
      if (HYPACE_CFG < HP_BATCH) {
        timer_obj_set_timer(lookup_tobj, prev_ts, ret, PACER_SPIN_THRESHOLD);
      } else {
#if CONFIG_HEAP_SPINLOCK
        spin_lock_irqsave(&pth->heap_lock, heap_flag);
#endif

        SME_START_TIMER(heap_remove_delay);

        remove_from_ptimer_heap(pth, &lookup_tobj->ptimer);

        SME_POBJ_END_TIMER_ARR(heap_remove_delay, gprof->hypace_idx);

        SME_START_TIMER(heap_insert_delay);

        timer_obj_set_timestamp(lookup_tobj, prev_ts, ret, pth);

        SME_POBJ_END_TIMER_ARR(heap_insert_delay, gprof->hypace_idx);

#if CONFIG_HEAP_SPINLOCK
        spin_unlock_irqrestore(&pth->heap_lock, heap_flag);
#endif
      }

#if SME_DEBUG_LVL <= LVL_DBG
      guest_prof_slot_idx = lookup_tobj->profq_addr->profq_idx;
      xprintk(LVL_DBG, "REPLACE T(%d:%d) G%d [%s %u, %s %u]"
        " TS %lu %ld truetime %ld\n"
        "prof [%s %u, %s %u] reqts %lu %ld\n"
        "first timer %lu => %lu hash %s => %s, ret = %d"
        , next_idx, lookup_tobj->timer_idx, guest_prof_slot_idx
        , x_out_ipstr, xprof->conn_id.out_port, x_dst_ipstr, xprof->conn_id.dst_port
        , xprof->req_ts, xprof->req_stime, lookup_tobj->true_expires
        , out_ipstr, gprof->conn_id.out_port, dst_ipstr, gprof->conn_id.dst_port
        , gprof->req_ts, get_s_time_fixed2(gprof->req_ts)
        , old_prev_ts, prev_ts, x_hash, hash, ret
        );
#endif
    }

#if CONFIG_TIMERQ_SPINLOCK
    spin_unlock_irqrestore(&xprof->timers_lock, prof_flag);
    spin_unlock_irqrestore(&atq->active_lock, atq_flag);
#elif CONFIG_SAVE_IRQ
    local_irq_restore(atq_flag);
#endif

    return 0;
  }

  /*
   * profile extension due to retransmission
   */
  ret = extend_xprofile_unlocked(xprof, gprof);

  ret = xprofile_get_prev_ts_unlocked(xprof, gprof, &prev_ts);

  /*
   * previous timer has not expired, no need to set a new timer.
   * if extending the profile, on expiry of the previous timer,
   * xen_pacer will automatically set new timer from new profile.
   */
  if (ret >= 0 && !is_timeslot_expired(prev_ts, epoch - EPOCH_SIZE)) {
#if CONFIG_TIMERQ_SPINLOCK
//    xprintk(LVL_DBG, "%s", "PROF UNLOCK");
    spin_unlock_irqrestore(&xprof->timers_lock, prof_flag);
//    xprintk(LVL_DBG, "%s", "ATQ UNLOCK");
    spin_unlock_irqrestore(&atq->active_lock, atq_flag);
#elif CONFIG_SAVE_IRQ
    local_irq_restore(atq_flag);
#endif

    return 0;
  }

  ret = xprofile_pick_next_ts_unlocked(xprof, lookup_tobj->profq_addr, &next_ts);
  if (ret >= 0) {
    if (HYPACE_CFG < HP_BATCH) {
      timer_obj_set_timer(lookup_tobj, next_ts, ret, PACER_SPIN_THRESHOLD);
    } else {
#if CONFIG_HEAP_SPINLOCK
      spin_lock_irqsave(&pth->heap_lock, heap_flag);
#endif

      SME_START_TIMER(heap_remove_delay);

      remove_from_ptimer_heap(pth, &lookup_tobj->ptimer);

      SME_POBJ_END_TIMER_ARR(heap_remove_delay, gprof->hypace_idx);

      SME_START_TIMER(heap_insert_delay);

      timer_obj_set_timestamp(lookup_tobj, next_ts, ret, pth);

      SME_POBJ_END_TIMER_ARR(heap_insert_delay, gprof->hypace_idx);

#if CONFIG_HEAP_SPINLOCK
      spin_unlock_irqrestore(&pth->heap_lock, heap_flag);
#endif
    }
#if SME_DEBUG_LVL <= LVL_DBG
    xprintk(LVL_INFO, "%s [%s %u, %s %u] reset T%d val %ld truetime %ld epoch %ld"
      , (prof_extn ? "EXTEND" : "REPLACE")
      , x_out_ipstr, xprof->conn_id.out_port, x_dst_ipstr, xprof->conn_id.dst_port
      , ret, (next_ts - PACER_SPIN_THRESHOLD), next_ts, epoch);
#endif
  }

#if CONFIG_TIMERQ_SPINLOCK
//  xprintk(LVL_DBG, "%s", "PROF UNLOCK");
  spin_unlock_irqrestore(&xprof->timers_lock, prof_flag);
//  xprintk(LVL_DBG, "%s", "ATQ UNLOCK");
  spin_unlock_irqrestore(&atq->active_lock, atq_flag);
#elif CONFIG_SAVE_IRQ
  local_irq_restore(atq_flag);
#endif

  return 0;
}

int
active_timerq_update_cwnd(void *pobj_p, active_timerq_t *atq, timerq_t *tq,
    profq_elem_t *gprof, int cmd, s_time_t epoch, ptimer_heap_t *pth)
{
  uint64_t next_ts = 0;
  uint64_t prev_ts = 0;
  timer_obj_t *lookup_tobj = NULL;
  profile_t *p = NULL;
  profile_int_t *p_int = NULL;
  s_time_t ev_stime = 0;
  s_time_t req_stime = 0;
  s_time_t pause_time = 0;
  s_time_t gap_time = 0;
  int pause_idx = 0;
  int pause_req_idx = 0, pause_frame_idx = 0;
  int req_it = 0;
  int num_frames_till_pause_idx = 0;
  int num_frames_in_pause_time = 0;
  int unpause_idx = 0;
  s_time_t cwnd_latency = 0;
//  int found = 0;
//  int32_t next_idx = 0;
  int ret = 0;
  xprofile_t *xprof = NULL;
  char x_out_ipstr[16];
  char x_dst_ipstr[16];
  int guest_prof_slot_idx = 0;
  int timerq_idx = 0;
  int threshold = 0;
  unsigned long atq_flag = 0;
#if CONFIG_TIMERQ_SPINLOCK
  unsigned long prof_flag = 0;
#endif

#if CONFIG_HEAP_SPINLOCK
  unsigned long heap_flag = 0;
#endif

  pacer_obj_t *pobj = (pacer_obj_t *) pobj_p;

  SME_INIT_TIMER(heap_insert_delay);
  SME_INIT_TIMER(heap_remove_delay);

#if SME_DEBUG_LVL <= LVL_INFO
  int was_paused = 0;
#endif

  if (!atq || !gprof)
    return -EINVAL;

#if SME_DEBUG_LVL <= LVL_INFO
  was_paused = atomic_read(&gprof->is_paused);
#endif

#if CONFIG_TIMERQ_SPINLOCK
  spin_lock_irqsave(&atq->active_lock, atq_flag);
//  xprintk(LVL_DBG, "%s", "ATQ LOCK");
#elif CONFIG_SAVE_IRQ
  local_irq_save(atq_flag);
#endif

#if 1
  lookup_tobj = &tq->queue[gprof->tobj_idx];
#else
  next_idx = atomic_read(&atq->first);
  while (next_idx >= 0) {
    lookup_tobj = &atq->queue[next_idx];
    if (lookup_tobj->profq_addr == gprof) {
      found = 1;
      break;
    }

    next_idx = atomic_read(&lookup_tobj->next);
  }

  if (!found) {
#if CONFIG_TIMERQ_SPINLOCK
//    xprintk(LVL_DBG, "%s", "ATQ UNLOCK");
    spin_unlock_irqrestore(&atq->active_lock, atq_flag);
#elif CONFIG_SAVE_IRQ
    local_irq_restore(atq_flag);
#endif

    return -ENOENT;
  }
#endif

  timerq_idx = lookup_tobj - &tq->queue[0];
  guest_prof_slot_idx = lookup_tobj->profq_addr->profq_idx;
  xprof = &lookup_tobj->xen_profp;
  ipstr(xprof->conn_id.out_ip, x_out_ipstr);
  ipstr(xprof->conn_id.dst_ip, x_dst_ipstr);

#if CONFIG_TIMERQ_SPINLOCK
  spin_lock_irqsave(&xprof->timers_lock, prof_flag);
//  xprintk(LVL_DBG, "%s", "PROF LOCK");
#endif

  atomic_set(&gprof->is_paused, 0);

  if (cmd == P_UPDATE_CWND_ACK) {
    threshold = CWND_UPDATE_LATENCY;
    ev_stime = get_s_time_fixed2(gprof->ack_ts);
    pause_idx = gprof->next_timer_idx;
  } else if (cmd == P_UPDATE_CWND_LOSS) {
    threshold = REXMIT_UPDATE_LATENCY;
    ev_stime = get_s_time_fixed2(gprof->loss_ts);
    pause_idx = gprof->cwnd_watermark - 1;
  }

  p = &xprof->local_profile;
  p_int = (profile_int_t *) p->priv;
  req_stime = xprof->req_stime;
  ret = get_req_frame_idx_from_timer_idx(&xprof->local_profile, pause_idx,
      &pause_req_idx, &pause_frame_idx);
  if (ret < 0) {
    xprintk(LVL_DBG, "INVALID T(%d:%d) G%d [%s %u, %s %u]\n"
        "NXT %d CWM %d pause %d P %d T %d R %d D %d cwnd latency shift %ld"
        , timerq_idx, lookup_tobj->timer_idx, guest_prof_slot_idx
        , x_out_ipstr, xprof->conn_id.out_port
        , x_dst_ipstr, xprof->conn_id.dst_port
        , gprof->next_timer_idx, gprof->cwnd_watermark, pause_idx
        , gprof->num_timers, gprof->tail
        , atomic_read(&gprof->real_counter), gprof->dummy_counter
        , gprof->cwnd_latency_shift
        );
#if CONFIG_TIMERQ_SPINLOCK
//    xprintk(LVL_DBG, "%s", "PROF UNLOCK");
    spin_unlock_irqrestore(&xprof->timers_lock, prof_flag);
//    xprintk(LVL_DBG, "%s", "ATQ UNLOCK");
    spin_unlock_irqrestore(&atq->active_lock, atq_flag);
#elif CONFIG_SAVE_IRQ
    local_irq_restore(atq_flag);
#endif

    return -XP_COMPL;
  }

  for (req_it = 0; req_it < pause_req_idx; req_it++) {
    num_frames_till_pause_idx += p_int->num_out_frames[req_it];
  }
  num_frames_till_pause_idx += pause_frame_idx;

  /*
   * pause_idx = next_timer_idx
   *
   * pause_req_idx = burst in which we paused
   *
   * pause_frame_idx = frame # in the above burst where we paused
   *
   * pause_time = (req_stime + latency of frame in the burst in which we paused)
   *
   * gap_time = (ack_stime + threshold) - pause_time
   *
   * num_frames_in_pause_time = frames that could have been actually sent from
   * the paused burst until the ACK opened the CWND (in hypervisor, so we add +1)
   *
   * unpause_idx = value of next_timer_idx if we had continued playing the
   * paused burst until the ACK opened the CWND
   *
   * cwnd_latency = num_frames_in_pause_time * spacing of the paused burst
   *
   * gprof->cwnd_latency_shift += cwnd_latency
   * total shift in the profiles due to one or more cwnd shutdowns in TCP.
   *
   * we add cwnd_latency_shift into the first out frame latency of every burst
   * in a profile. so all subsequent frames will be sent at a time with an
   * additional delay of the cwnd_latency.
   *
   */

  pause_time = xprofile_get_timer_at_idx(xprof, gprof, pause_idx);
  gap_time = ev_stime + threshold - pause_time;

  /*
   * gap_time < 0: ACK opened cwnd before next timer expiry
   * (when it would have paused)
   *
   * gap >= 0: cwnd closed in between a multi-packet burst, the profile
   * shifted by at least one slot
   *
   * gap >= 0 && spacing == 0: single packet burst
   *
   */

  if (gap_time < 0) {
    cwnd_latency = 0;
    unpause_idx = pause_idx;
  } else if (p_int->spacing[pause_req_idx] == 0) {
    cwnd_latency = gap_time;
    unpause_idx = pause_idx;
  } else {
    num_frames_in_pause_time = (gap_time/p_int->spacing[pause_req_idx]) + 1;
    cwnd_latency = num_frames_in_pause_time * p_int->spacing[pause_req_idx];
    unpause_idx = num_frames_till_pause_idx + num_frames_in_pause_time;
  }

  ret = xprofile_get_prev_ts_unlocked(xprof, gprof, &prev_ts);

#if SME_DEBUG_LVL <= LVL_INFO
  xprintk(LVL_EXP, "T(%d:%d) G%d xprof [%s %u, %s %u] CMD %d epoch %ld\n"
      "evTS %ld reqTS %ld pauseTS %ld paused %d #frames %ld latency %lu "
      "spacing %lu gap %ld prevTS %ld ret %d\n"
      "CWM %d pause %d(%d,%d) unpause %d #frames till pause %d #frames in pause %d "
      "NXT %d P %d T %d R %d D %d cwnd lat shift %ld => %ld "
      "pause_req_idx %d"
      // tobj->timer_idx may not have been updated yet (due to profile pause),
      // timeslot is reflected more accurately by gprof->next_timer_idx
      , timerq_idx, lookup_tobj->timer_idx, guest_prof_slot_idx
      , x_out_ipstr, xprof->conn_id.out_port, x_dst_ipstr, xprof->conn_id.dst_port
      , cmd, epoch
      , ev_stime, req_stime, was_paused ? xprof->last_pause_stime : pause_time
      , was_paused, p_int->num_out_frames[pause_req_idx]
      , p_int->latency[pause_req_idx], p_int->spacing[pause_req_idx], gap_time
      , prev_ts, ret
      , gprof->cwnd_watermark, pause_idx, pause_req_idx, pause_frame_idx
      , unpause_idx, num_frames_till_pause_idx, num_frames_in_pause_time
      , gprof->next_timer_idx, gprof->num_timers, gprof->tail
      , atomic_read(&gprof->real_counter), gprof->dummy_counter
      , gprof->cwnd_latency_shift
      , (gprof->cwnd_latency_shift + cwnd_latency)
      , pause_req_idx
      );
#endif

  // accumulate the shifts due to each instance of congestion window shutdown
  xprof->cwnd_shift_idx = pause_idx;
  xprof->cwnd_shift_val = cwnd_latency;

//  gprof->cwnd_latency_shift += cwnd_latency;

  /*
   * previous timer has not expired, no need to set a new timer.
   * on timer expiry, xen pacer will automatically set new timer from new profile.
   * XXX: once we fix cwnd handling, we should hopefully not require this
   */
  if (ret >= 0 && !is_timeslot_expired(prev_ts, epoch - EPOCH_SIZE)) {
//    atomic_set(&gprof->is_paused, 0);

#if CONFIG_TIMERQ_SPINLOCK
//    xprintk(LVL_DBG, "%s", "PROF UNLOCK");
    spin_unlock_irqrestore(&xprof->timers_lock, prof_flag);
//    xprintk(LVL_DBG, "%s", "ATQ UNLOCK");
    spin_unlock_irqrestore(&atq->active_lock, atq_flag);
#elif CONFIG_SAVE_IRQ
    local_irq_restore(atq_flag);
#endif

    return 0;
  }

  ret = xprofile_pick_next_ts_unlocked(xprof, gprof, &next_ts);
  if (ret >= 0) {
//    atomic_set(&gprof->is_paused, 0);
    if (HYPACE_CFG < HP_BATCH) {
      timer_obj_set_timer(lookup_tobj, next_ts, ret, PACER_SPIN_THRESHOLD);
    } else {
#if CONFIG_HEAP_SPINLOCK
      spin_lock_irqsave(&pth->heap_lock, heap_flag);
#endif

      SME_START_TIMER(heap_remove_delay);

      remove_from_ptimer_heap(pth, &lookup_tobj->ptimer);

      SME_POBJ_END_TIMER_ARR(heap_remove_delay, gprof->hypace_idx);

      SME_START_TIMER(heap_insert_delay);

      timer_obj_set_timestamp(lookup_tobj, next_ts, ret, pth);

      SME_POBJ_END_TIMER_ARR(heap_insert_delay, gprof->hypace_idx);

#if CONFIG_HEAP_SPINLOCK
      spin_unlock_irqrestore(&pth->heap_lock, heap_flag);
#endif
    }
    xprintk(LVL_INFO, "T(%d:%d) G%d [%s %u, %s %u] shifted val %ld truetime %ld epoch %ld"
      , timerq_idx, lookup_tobj->timer_idx, gprof->profq_idx
      , x_out_ipstr, xprof->conn_id.out_port, x_dst_ipstr, xprof->conn_id.dst_port
      , (next_ts - PACER_SPIN_THRESHOLD), next_ts, epoch);
  }

#if CONFIG_TIMERQ_SPINLOCK
//  xprintk(LVL_DBG, "%s", "PROF UNLOCK");
  spin_unlock_irqrestore(&xprof->timers_lock, prof_flag);
//  xprintk(LVL_DBG, "%s", "ATQ UNLOCK");
  spin_unlock_irqrestore(&atq->active_lock, atq_flag);
#elif CONFIG_SAVE_IRQ
  local_irq_restore(atq_flag);
#endif

  if (cmd == P_UPDATE_CWND_ACK) {
    SME_POBJ_ADD_COUNT_POINT_ARR(ack_update, NOW() - ev_stime, gprof->hypace_idx);
  } else if (cmd == P_UPDATE_CWND_LOSS) {
    SME_POBJ_ADD_COUNT_POINT_ARR(rtx_update, NOW() - ev_stime, gprof->hypace_idx);
  }

  return 0;
}

#if 0
int
active_timerq_free_profile(active_timerq_t *atq, profq_elem_t *gprof,
    xprofq_hdr_t *guest_profq_hdr, xprofq_freelist2_t *guest_profq_fl,
    timerq_t *pac_timerq)
{
  int32_t next_idx = 0;
  timer_obj_t *lookup_tobj = NULL;
  int found = 0;
  int ret = 0;
  int timerq_idx = 0, guest_prof_slot_idx = 0;
  xprofile_t *xprof = NULL;
#if SME_DEBUG_LVL <= LVL_INFO
  char x_out_ipstr[16];
  char x_dst_ipstr[16];
  uint16_t x_out_port = 0, x_dst_port = 0;
#endif

  if (!atq || !gprof || !guest_profq_hdr || !guest_profq_fl || !pac_timerq)
    return -EINVAL;

#if CONFIG_TIMERQ_SPINLOCK
  spin_lock(&atq->active_lock);
#endif

  next_idx = atomic_read(&atq->first);
  while (next_idx >= 0) {
    lookup_tobj = &atq->queue[next_idx];
    if (lookup_tobj->profq_addr == gprof) {
      found = 1;
      break;
    }

    next_idx = atomic_read(&lookup_tobj->next);
  }

  if (!found) {
#if CONFIG_TIMERQ_SPINLOCK
    spin_unlock(&atq->active_lock);
#endif
    return -ENOENT;
  }

  timerq_idx = lookup_tobj - &pac_timerq->queue[0];
  guest_prof_slot_idx = lookup_tobj->profq_addr->profq_idx;
  xprof = &lookup_tobj->xen_profp;

#if CONFIG_TIMERQ_SPINLOCK
  // not required?
  spin_lock(&xprof->timers_lock);
#endif

#if SME_DEBUG_LVL <= LVL_INFO
  ipstr(xprof->conn_id.out_ip, x_out_ipstr);
  ipstr(xprof->conn_id.dst_ip, x_dst_ipstr);
  x_out_port = xprof->conn_id.out_port;
  x_dst_port = xprof->conn_id.dst_port;
  xprintk(LVL_INFO, "Free T%d G%d [%s %u, %s %u] state %d paused %d "
      "NXT %d CWM %d P %d R %d D %d max_seqno %u"
      , timerq_idx, guest_prof_slot_idx, x_out_ipstr, xprof->conn_id.out_port
      , x_dst_ipstr, xprof->conn_id.dst_port
      , gprof->state, atomic_read(&gprof->is_paused)
      , gprof->next_timer_idx, gprof->cwnd_watermark, gprof->num_timers
      , atomic_read(&gprof->real_counter), gprof->dummy_counter, gprof->max_seqno
      );
#endif

  stop_timer(&(lookup_tobj->xtimer));

#if CONFIG_HEAP_SPINLOCK
  spin_lock(&pac_obj.global_ptimer_heap.heap_lock);
#endif

  remove_from_ptimer_heap(&pac_obj.global_ptimer_heap, &lookup_tobj->ptimer);

#if CONFIG_HEAP_SPINLOCK
  spin_unlock(&pac_obj.global_ptimer_heap.heap_lock);
#endif

  cleanup_xprofile_unlocked(&lookup_tobj->xen_profp);
  gprof->state = PSTATE_XEN_COMPL;
  atomic_set(&gprof->is_paused, 0);
  // remove timer from active timerq
  ret = active_timerq_remove_idx_unlocked(atq, timerq_idx);
  if (ret >= 0) {
    lookup_tobj->true_expires = -1;
//    lookup_tobj->profq_addr = NULL;
    lookup_tobj->timer_idx = -1;
    lookup_tobj->type = 0;

#if CONFIG_TIMERQ_SPINLOCK
    spin_unlock(&(lookup_tobj->xen_profp.timers_lock));
    spin_unlock(&atq->active_lock);
#endif

    // push timer into free timerq
    push_one_timer(pac_timerq, timerq_idx);
  } else {

#if CONFIG_TIMERQ_SPINLOCK
    spin_unlock(&(lookup_tobj->xen_profp.timers_lock));
    spin_unlock(&atq->active_lock);
#endif

  }

  // push shared profile's slot index into shared freelist
  put_one_profq_freelist2(guest_profq_fl, lookup_tobj->profq_addr->profq_idx);

#if SME_DEBUG_LVL <= LVL_INFO
  xprintk(LVL_INFO, "Freed T%d G%d [%s %u, %s %u]"
      , timerq_idx, guest_prof_slot_idx
      , x_out_ipstr, x_out_port, x_dst_ipstr, x_dst_port
      );
#endif

  return 0;
}
#endif

