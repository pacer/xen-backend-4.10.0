/*
 * xnicq_hdr.h
 *
 * created on: Nov 18, 2018
 * author: aasthakm
 *
 * xen local version of shared nicq_hdr and nic_freelist structs
 */

#ifndef __XNICQ_HDR_H__
#define __XNICQ_HDR_H__

#include "sme_guest.h"

typedef struct xglobal_nicq_hdr {
  int qsize;
  nic_txdata_t **queue;
} xglobal_nicq_hdr_t;

int nicq_remove(nicq_hdr_t *nicq_hdr, xglobal_nicq_hdr_t *xgnicq_hdr, uint64_t key,
    profq_elem_t *gprof);
void init_x_global_nicq_hdr(xglobal_nicq_hdr_t *xgnicq_hdr,
    global_nicq_hdr_t *global_nicq);
void cleanup_x_global_nicq_hdr(xglobal_nicq_hdr_t *xgnicq_hdr);

static inline int
nicq_remove_first(nicq_hdr_t *nicq_hdr, xglobal_nicq_hdr_t *xgnicq_hdr, profq_elem_t *gprof)
{
  return nicq_remove(nicq_hdr, xgnicq_hdr, 0, gprof);
}

typedef struct xnic_freelist {
  int fl_size;
  uint32_t *prod_idx_p;
  uint32_t *cons_idx_p;
  nic_fl_elem_t **nfl_arr_p;
} xnic_freelist_t;

void init_x_nic_freelist(xnic_freelist_t *xnfl, nic_freelist_t *global_nfl);
void cleanup_x_nic_freelist(xnic_freelist_t *xnfl);
int put_one_nic_freelist(xnic_freelist_t *xnfl, nic_fl_elem_t *nelem);

static inline int
free_nicq_slot(xnic_freelist_t *xnfl, int idx)
{
  nic_fl_elem_t nelem = { idx };
  return put_one_nic_freelist(xnfl, &nelem);
}

typedef struct xnic_txintq {
  int qsize;
  nic_txint_elem_t **queue_p;
} xnic_txintq_t;

void init_x_nic_txintq(xnic_txintq_t *xnic_txintq, nic_txintq_t *guest_txintq);
void cleanup_x_nic_txintq(xnic_txintq_t *xnic_txintq);
#endif /* __XNICQ_HDR_H__ */
