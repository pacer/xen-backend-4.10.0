/*
 * sme_spinlock.h
 *
 * created on: May 08, 2018
 * author: aasthakm
 */

#ifndef __SME_SPINLOCK_H__
#define __SME_SPINLOCK_H__

#include <asm/atomic.h>

//#define atomic_fetch_inc(v) arch_fetch_and_add(&(v)->counter, 1)
static inline int atomic_fetch_inc(atomic_t *v)
{
  return arch_fetch_and_add(&(v)->counter, 1);
}

typedef struct tkt_lock {
  atomic_t next_tkt;
  atomic_t now_serving;
} tkt_lock_t;

int init_tkt_lock(tkt_lock_t *lock);
void acquire_tkt_lock(tkt_lock_t *lock);
void release_tkt_lock(tkt_lock_t *lock);
void acquire_tkt_lock_bh(tkt_lock_t *lock);
void release_tkt_lock_bh(tkt_lock_t *lock);

#endif /* __SME_SPINLOCK_H__ */
