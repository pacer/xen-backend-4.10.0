/*
 * xprofile.c
 *
 * created on: Mar 23, 2018
 * author: aasthakm
 *
 * xprofile implementation
 */

#include <xen/kernel.h>

#include "timerobj.h"
#include "xdebug.h"

extern ihash_t DEFAULT_PROFILE_IHASH;
extern ihash_t DEFAULT_HTTP_PROF_IHASH;
extern ihash_t DEFAULT_HTTPS_PROF_IHASH;
extern ihash_t DEFAULT_MW_PROF_IHASH;
extern ihash_t DEFAULT_VIDEO_PROF_IHASH;
extern ihash_t pid1, pid2, pid3;
extern xprofq_hdr_t guest_profq_hdr;

#if 0
void
init_default_timers(xprofile_t *xprof)
{
  uint16_t num_out_reqs = 3;
#if CONFIG_XPROF_TIMER_ARRAY
//  uint64_t num_out_frames[1] = { 1 };
//  uint64_t spacing[1] = 0;
  // profile for the initial sequence
  // SYN, SYNACK, ACK, PSHACK, ACK, RSP
  uint64_t latency[3] = { 100000000, 10000000, 10000000 }; // 100ms, 100ms, 100ms

  xprof->timers = xzalloc_array(uint64_t, num_out_reqs);
  xprof->timers[0] = xprof->req_stime + latency[0];
  xprof->timers[1] = xprof->timers[0] + latency[1];
  xprof->timers[2] = xprof->timers[1] + latency[2];
#endif
  xprof->num_timers = num_out_reqs;
  xprof->next_timer_idx = 0;
}
#endif

int64_t
xprofile_get_timer_at_idx(xprofile_t *xprof, profq_elem_t *gprof, uint16_t idx)
{
  int ret = 0;
  int req_it = 0, frame_it = 0, timer_it = 0;
//  int prev_timer_count = 0, timer_count = 0;
  uint64_t next_ts = 0;
  profile_t *prof = NULL;
  profile_int_t *p_int = NULL;
  if (!xprof)
    return -1;

  prof = &xprof->local_profile;
  p_int = (profile_int_t *) prof->priv;

  // first get the "index" into the profile request number and frame number
  ret = get_req_frame_idx_from_timer_idx(prof, idx, &req_it, &frame_it);
  if (ret < 0)
    return ret;

  // get the latency and additional spacing based on req_it and frame_it
  next_ts = xprof->req_stime + gprof->cwnd_latency_shift + p_int->latency[req_it];
  if (frame_it == 0) {
    xprintk(LVL_DBG, "R%d:F%d calc TS %ld, req %lu %ld lat %ld spac %ld"
        , req_it, frame_it, next_ts, xprof->req_ts, xprof->req_stime
        , p_int->latency[req_it], p_int->spacing[req_it]);
    return next_ts;
  }

  for (timer_it = 0; timer_it < frame_it; timer_it++) {
    next_ts += p_int->spacing[req_it];
  }

  xprintk(LVL_DBG, "R%d:F%d calc TS %ld, req %lu %ld lat %ld spac %ld"
      , req_it, frame_it, next_ts, xprof->req_ts, xprof->req_stime
      , p_int->latency[req_it], p_int->spacing[req_it]);
  return next_ts;
}

void
init_xprofile_timers(xprofile_t *xprof, profq_elem_t *gprof, profile_t *prof)
{
  int req_it;
#if CONFIG_XPROF_TIMER_ARRAY
  int frame_it, timer_it;
#endif
  int total_timers = 0;
  profile_int_t *p_int = NULL;
  if (!xprof || !prof)
    return;

  p_int = (profile_int_t *) prof->priv;
  for (req_it = 0; req_it < p_int->num_out_reqs; req_it++) {
    total_timers += p_int->num_out_frames[req_it];
  }

  xprintk(LVL_DBG, "Total timers %d", total_timers);

#if CONFIG_XPROF_TIMER_ARRAY
  xprof->timers = xzalloc_array(uint64_t, total_timers);
  timer_it = 0;
  for (req_it = 0; req_it < p_int->num_out_reqs; req_it++) {
    for (frame_it = 0; frame_it < p_int->num_out_frames[req_it]; frame_it++) {
      if (frame_it == 0) {
        xprof->timers[timer_it] = get_s_time_fixed2(xprof->req_ts)
          + p_int->latency[req_it];
        xprintk(LVL_DBG, "lat %ld spac %ld"
            , p_int->latency[req_it], p_int->spacing[req_it]);
      } else {
        xprof->timers[timer_it] = xprof->timers[timer_it-1]
          + p_int->spacing[req_it];
      }
      timer_it++;
    }
  }

  for (timer_it = 0; timer_it < total_timers; timer_it++) {
    xprintk(LVL_DBG, "T%d %ld", timer_it, xprof->timers[timer_it]);
  }
#endif

  gprof->num_timers = total_timers;
}

int
check_profile_prefix(xprofile_t *xprof, profq_elem_t *gprof,
    sme_htable_t *pmap_htbl, s_time_t deadline)
{
  profile_t *local_prof = &xprof->local_profile;
  profile_t *guest_prof = NULL;
  profile_int_t *local_p_int, *guest_p_int;
  int local_req_idx = 0, guest_req_idx = 0;
  int local_frame_idx = 0, guest_frame_idx = 0;
  int req_it = 0;
  int guest_ret = 0, local_ret = 0;
  s_time_t guest_replace_ts = 0, local_replace_ts = 0;
  s_time_t min_ts = 0;
  int next_timer_idx = 0;
  int ret = 0;
  uint64_t prev_ts = 0;

  guest_prof = htable_lookup(pmap_htbl, (void *) &gprof->id_ihash,
      sizeof(ihash_t));
  if (!guest_prof)
    return -XP_INVAL;

  guest_p_int = (profile_int_t *) guest_prof->priv;
  local_p_int = (profile_int_t *) local_prof->priv;

  next_timer_idx = gprof->next_timer_idx;
  ret = xprofile_get_prev_ts_unlocked(xprof, gprof, &prev_ts);
  if (ret >= 0 && !is_timeslot_expired(prev_ts, deadline - EPOCH_SIZE))
    next_timer_idx--;

  guest_ret = get_req_frame_idx_from_timer_idx(guest_prof, next_timer_idx,
      &guest_req_idx, &guest_frame_idx);
  local_ret = get_req_frame_idx_from_timer_idx(local_prof, next_timer_idx,
      &local_req_idx, &local_frame_idx);

  xprintk(LVL_DBG,
      "Epoch %ld req %ld T%d pivot idx %d local (%d, %d, %d, %ld)"
      " guest (%d, %d, %d, %ld)"
      , deadline, xprof->req_stime, gprof->next_timer_idx, next_timer_idx
      , local_req_idx, local_frame_idx, local_ret, local_p_int->num_out_frames[1]
      , guest_req_idx, guest_frame_idx, guest_ret, guest_p_int->num_out_frames[1]);

  // guest profile is shorter than local profile, and local profile is
  // already beyond the last burst of guest profile.
  if (guest_ret < 0)
    return -XP_INVAL;

  // matching burst prefix have same number of requests and frames
  if (guest_req_idx != local_req_idx || guest_frame_idx != local_frame_idx)
    return -XP_INVAL;

  // the burst at req_idx has not yet started, so only bursts upto req_idx-1
  // must match. we can replace burst at req_idx if further checks allow.
  if (guest_frame_idx == 0) {
    guest_req_idx--;
    local_req_idx--;
  }

  // for each burst upto req_idx, compare latency and spacing values.
  // we need not compare frame values, since they are checked above.
  for (req_it = 0; req_it < guest_req_idx; req_it++) {
    if (guest_p_int->latency[req_it] != local_p_int->latency[req_it]
        || guest_p_int->spacing[req_it] != local_p_int->spacing[req_it])
      return -XP_INVAL;
  }

  // get the timestamp of the first slot of the next unscheduled burst.
  // if neither bursts have expired, then we can replace the profile.
  guest_replace_ts = xprof->req_stime + guest_p_int->latency[guest_req_idx+1];
  local_replace_ts = xprof->req_stime + local_p_int->latency[local_req_idx+1];
  min_ts = min_t(s_time_t, guest_replace_ts, local_replace_ts);
  if (is_timeslot_expired(min_ts, deadline - EPOCH_SIZE))
    return -XP_EXPRD;

  return 0;
}

void
reset_xprofile_unlocked(xprofile_t *xprof, profq_elem_t *gprof,
    sme_htable_t *pmap_htbl, int replace_status)
{
  profile_t *p = NULL;
  if (!xprof || !gprof)
    return;

  // TODO: read from the profile DB shared in memory between guest and xen
  p = htable_lookup(pmap_htbl, (void *) &gprof->id_ihash, sizeof(ihash_t));
  if (!p || !p->priv)
    return;

  xprof->prof_id = gprof->id_ihash;
  xprof->conn_id = gprof->conn_id;
  xprof->req_ts = gprof->req_ts;
  xprof->req_stime = get_s_time_fixed2(gprof->req_ts);
  xprof->id = gprof->id;
  xprof->pid_replace_status = replace_status;
  xprof->x_real_count = 0;
  xprof->x_dummy_count = 0;
#if CONFIG_ALLOC_LOCAL_PROF
  cleanup_profile(&xprof->local_profile);
  clone_profile(p, &xprof->local_profile);
#else
  xprof->local_profile.priv = p->priv;
#endif

#if CONFIG_XPROF_TIMER_ARRAY
  if (xprof->timers)
    xfree(xprof->timers);
#endif

  init_xprofile_timers(xprof, gprof, p);
}

void
init_xprofile(xprofile_t *xprof, profq_elem_t *gprof, sme_htable_t *pmap_htbl)
{
  unsigned long xprof_flag = 0;

  if (!xprof || !gprof)
    return;

  INIT_LIST_HEAD(&xprof->xprof_listp);
  spin_lock_init(&xprof->timers_lock);

#if CONFIG_TIMERQ_SPINLOCK
  spin_lock_irqsave(&xprof->timers_lock, xprof_flag);
#elif CONFIG_SAVE_IRQ
  local_irq_save(xprof_flag);
#endif

  reset_xprofile_unlocked(xprof, gprof, pmap_htbl, PIDR_UNTESTED);

#if CONFIG_TIMERQ_SPINLOCK
  spin_unlock_irqrestore(&xprof->timers_lock, xprof_flag);
#elif CONFIG_SAVE_IRQ
  local_irq_restore(xprof_flag);
#endif
}

int
xprofile_pick_next_ts(xprofile_t *xprof, profq_elem_t *gprof,
    uint64_t *ts)
{
  int ret = 0;
  unsigned long xprof_flag = 0;

  if (!xprof || !ts)
    return -XP_NOENT;

#if CONFIG_TIMERQ_SPINLOCK
  spin_lock_irqsave(&xprof->timers_lock, xprof_flag);
//  xprintk(LVL_DBG, "%s", "PROF LOCK");
#elif CONFIG_SAVE_IRQ
  local_irq_save(xprof_flag);
#endif

  ret = xprofile_pick_next_ts_unlocked(xprof, gprof, ts);

#if CONFIG_TIMERQ_SPINLOCK
//  xprintk(LVL_DBG, "%s", "PROF UNLOCK");
  spin_unlock_irqrestore(&xprof->timers_lock, xprof_flag);
#elif CONFIG_SAVE_IRQ
  local_irq_restore(xprof_flag);
#endif

  return ret;
}

int
extend_xprofile_unlocked(xprofile_t *xprof, profq_elem_t *gprof)
{
  int ret = 0;
  s_time_t curr_time = 0;
  s_time_t last_time = 0;
  s_time_t cmp_time = 0;
  s_time_t gap_time = 0;
  s_time_t rexmit_time = 0;
  int num_frames_in_gap_time = 0;
  profile_int_t *p_int = NULL;
  int last_req_idx = 0, last_frame_idx = 0;
  int last_idx = 0;
  int rexmit_idx = 0;

  /*
   * retransmission appends a new slot in the profile
   */
  last_idx = gprof->num_timers-1;
  p_int = (profile_int_t *) xprof->local_profile.priv;
  ret = get_req_frame_idx_from_timer_idx(&xprof->local_profile,
      last_idx, &last_req_idx, &last_frame_idx);
  if (ret < 0)
    return ret;

  curr_time = NOW();

  last_time = xprofile_get_timer_at_idx(xprof, gprof, last_idx);
  if (!is_timeslot_expired(last_time, curr_time + REXMIT_UPDATE_LATENCY)) {
    // profile has not exhausted yet
    p_int->num_out_frames[last_req_idx] += 1;
    gprof->num_timers += 1;
    return 0;
  }

  cmp_time = max_t(s_time_t, curr_time, last_time);
  rexmit_time = NOW();
  gap_time = rexmit_time + REXMIT_UPDATE_LATENCY - cmp_time;

  num_frames_in_gap_time = (gap_time/p_int->spacing[last_req_idx]) + 1;
  // nxt is expected to be on the last slot of current profile
  rexmit_idx = gprof->num_timers + num_frames_in_gap_time;

  p_int->num_out_frames[last_req_idx] += 1;
  gprof->num_timers += 1;

  return 0;
}

int
replace_xprofile(xprofile_t *xprof, profq_elem_t *prof,
    sme_htable_t *pmap_htbl, int prof_extn, s_time_t deadline)
{
  int ret = 0;
  int conn_changed = 0;
  int prof_changed = 0;
  int ts_changed = 0;

  if (!xprof || !prof)
    return -XP_NOENT;

  // preempt unnecessary memcmp's
  if (xprof->pid_replace_status != PIDR_UNTESTED
      && xprof->pid_replace_status != PIDR_UNCHANGED) {
    return -XP_REPLC;
  }

  conn_changed =
    memcmp((void *) &xprof->conn_id, (void *) &prof->conn_id, sizeof(conn_t));

  /*
   * slot in the profile queue has been reused for a completely different conn
   */
  if (conn_changed != 0)
    return -XP_INVAL;

  ts_changed = (xprof->req_ts != prof->req_ts);
  prof_changed =
    memcmp((void *) &xprof->prof_id, (void *) &prof->id_ihash, sizeof(ihash_t));

  if (!ts_changed && !prof_changed && !prof_extn)
    return -XP_NOCHG;

  /*
   * update start time for the default profile
   */
  if (ts_changed && !prof_changed && !prof_extn)
    return 0;

  if (!prof_extn) {
    /*
     * profile changed, check if new profile's prefix matches with the
     * default profile's prefix, and the suffix has already expired or not.
     */
    ret = check_profile_prefix(xprof, prof, pmap_htbl, deadline);
    return ret;
  }

  // prof extn is allowed unconditionally
  return 0;
}

/*
 * =================================
 * unlocked versions of the API,
 * caller holds the locks explicitly
 * =================================
 */

void
cleanup_xprofile_unlocked(xprofile_t *xprof)
{
#if CONFIG_XPROF_TIMER_ARRAY
  if (xprof->timers)
    xfree(xprof->timers);
  xprof->timers = NULL;
#endif
#if CONFIG_ALLOC_LOCAL_PROF
  cleanup_profile(&xprof->local_profile);
#else
  xprof->local_profile.priv = NULL;
#endif
}

int
xprofile_pick_next_ts_unlocked(xprofile_t *xprof,
    profq_elem_t *gprof, uint64_t *ts)
{
  int ret = 0;

  if (!xprof || !ts)
    return -XP_NOENT;

  if (gprof->num_timers == 0) {
    return -XP_INVAL;
  }

#if CONFIG_XPROF_TIMER_ARRAY
  if (xprof->timers == NULL)
    return -XP_INVAL;
#endif

  if (gprof->next_timer_idx >= gprof->num_timers
      /* || (gprof->next_timer_idx >= gprof->cwnd_watermark &&
        gprof->cwnd_watermark <= 0) */) {
    // profile both completed and paused
    if (gprof->next_timer_idx >= gprof->cwnd_watermark &&
        gprof->cwnd_watermark >= gprof->num_timers)
      atomic_set(&gprof->is_paused, 1);

    return -XP_COMPL;
  }

  if (gprof->next_timer_idx >= gprof->cwnd_watermark && gprof->cwnd_watermark >= 0) {
    return -XP_PAUSE;
  }

#if CONFIG_XPROF_TIMER_ARRAY
  *ts = xprof->timers[gprof->next_timer_idx];
#else
  if (gprof->next_timer_idx == xprof->cwnd_shift_idx) {
    gprof->cwnd_latency_shift += xprof->cwnd_shift_val;
    xprof->cwnd_shift_val = 0;
    xprof->cwnd_shift_idx = 0;
  }
  *ts = xprofile_get_timer_at_idx(xprof, gprof, gprof->next_timer_idx);
#endif

  ret = gprof->next_timer_idx;
  gprof->next_timer_idx++;

  return ret;
}

int
xprofile_get_prev_ts_unlocked(xprofile_t *xprof, profq_elem_t *gprof, uint64_t *ts)
{
  int ret = 0;

  if (!xprof || !ts)
    return -XP_NOENT;

  if (gprof->num_timers == 0)
    return -XP_INVAL;

  // already the first index (we have not started playing the profile)
  if (gprof->next_timer_idx == 0)
    return -XP_UINIT;

  *ts = xprofile_get_timer_at_idx(xprof, gprof, gprof->next_timer_idx-1);
  ret = gprof->next_timer_idx-1;
  return ret;
}

void
xprofile_update_packet_counter(xprofile_t *xprof, int value, int type)
{
  int curr_val = 0;
  int ret = 0;

  if (!xprof)
    return;

  if (type == PKT_REAL) {
    do {
      curr_val = xprof->x_real_count;
      ret = cmpxchg(&xprof->x_real_count, curr_val, curr_val+value);
    } while (ret != curr_val);
  } else {
    do {
      curr_val = xprof->x_dummy_count;
      ret = cmpxchg(&xprof->x_dummy_count, curr_val, curr_val+value);
    } while (ret != curr_val);
  }
}

void
xprofile_update_gprof_packet_counter(xprofile_t *xprof, profq_elem_t *gprof,
    int value, int type, int from_xprof)
{
  int curr_val = 0;
  int ret = 0;

  if (!xprof || !gprof)
    return;

  if (type == PKT_REAL) {
    if (!from_xprof) {
      atomic_add(value, &gprof->real_counter);
    } else {
      do {
        curr_val = xprof->x_real_count;
        ret = cmpxchg(&xprof->x_real_count, curr_val, 0);
      } while (ret != curr_val);

      atomic_add(curr_val, &gprof->real_counter);
    }
  } else {
    if (!from_xprof) {
      gprof->dummy_counter += value;
    } else {
      do {
        curr_val = xprof->x_dummy_count;
        ret = cmpxchg(&xprof->x_dummy_count, curr_val, 0);
      } while (ret != curr_val);

      gprof->dummy_counter += curr_val;
    }
  }
}

/*
 * ==============================
 * API to alloc and lookup
 * profile_t objects in hashtable
 * ==============================
 */

// get the "index" into the profile (request number and frame number)
int
get_req_frame_idx_from_timer_idx(profile_t *p, uint16_t timer_idx,
    int *req_idx, int *frame_idx)
{
  int req_it = 0, frame_it = 0;
  int prev_timer_count = 0, timer_count = 0;
  profile_int_t *p_int = NULL;

  if (!p)
    return -1;

  p_int = p->priv;
  if (!p_int)
    return -1;

  prev_timer_count = 0;
  frame_it = -1;

  for (req_it = 0; req_it < p_int->num_out_reqs; req_it++) {
    timer_count += p_int->num_out_frames[req_it];
    if (timer_idx < timer_count) {
      frame_it = timer_idx - prev_timer_count;
      break;
    }

    prev_timer_count = timer_count;
  }

  if (frame_it < 0) {
    xprintk(LVL_ERR, "No more timers, requested idx %d max %d caller1 %pS caller2 %pS"
        , timer_idx, timer_count, __builtin_return_address(0)
        , __builtin_return_address(1));
    return -1;
  }

  *req_idx = req_it;
  *frame_idx = frame_it;

  return 0;
}

uint64_t
get_latency_for_req(profile_t *p, int req_idx)
{
  profile_int_t *p_int;
  if (!p || !p->priv)
    return -1;

  p_int = (profile_int_t *) p->priv;
  return p_int->latency[req_idx];
}

static void
__init_profile_int(profile_int_t *p, uint16_t num_out_reqs,
    uint16_t *num_extra_slots, uint64_t *num_out_frames,
    uint64_t *spacing, uint64_t *latency)
{
  if (!p)
    return;

  p->num_out_reqs = num_out_reqs;
  p->num_extra_slots = num_extra_slots;
  p->num_out_frames = num_out_frames;
  p->spacing = spacing;
  p->latency = latency;
}

int
init_profile(profile_t *p, uint16_t id, ihash_t id_ihash, uint16_t num_out_reqs,
    uint16_t *num_extra_slots, uint64_t *num_out_frames,
    uint64_t *spacing, uint64_t *latency)
{
  profile_int_t *p_int;

  if (!p) {
    xprintk(LVL_ERR, "%s", "Fatal error!!!");
    return -EINVAL;
  }

  memset(p, 0, sizeof(profile_t));
  p->id_ihash = id_ihash;

  p_int = (profile_int_t *) xzalloc(profile_int_t);

  if (!p_int) {
    xprintk(LVL_ERR, "%s", "memory allocation error");
    return -ENOMEM;
  }

  __init_profile_int(p_int, num_out_reqs, num_extra_slots, num_out_frames,
      spacing, latency);
  p->priv = (void *) p_int;
  INIT_LIST_HEAD(&p->profile_listp);

  return 0;
}

void
cleanup_profile(profile_t *prof)
{
  profile_int_t *p_int;
  if (!prof)
    return;

  p_int = (profile_int_t *) prof->priv;

  // nothing to free
  if (!p_int)
    return;

  if (p_int->num_extra_slots)
    xfree(p_int->num_extra_slots);
  if (p_int->num_out_frames)
    xfree(p_int->num_out_frames);
  if (p_int->spacing)
    xfree(p_int->spacing);
  if (p_int->latency)
    xfree(p_int->latency);
  xfree(p_int);
  prof->priv = NULL;
}

void
clone_profile(profile_t *src_p, profile_t *dst_p)
{
  profile_int_t *src_p_int = NULL;
  profile_int_t *dst_p_int = NULL;
  uint16_t num_out_reqs = 0;

  if (!src_p || !dst_p)
    return;

  src_p_int = (profile_int_t *) src_p->priv;
  if (!src_p_int)
    return;

  dst_p_int = (profile_int_t *) xzalloc(profile_int_t);
  if (!dst_p_int)
    return;

  num_out_reqs = src_p_int->num_out_reqs;

  dst_p_int->num_extra_slots = xzalloc_array(uint16_t, num_out_reqs);
  dst_p_int->num_out_frames = xzalloc_array(uint64_t, num_out_reqs);
  dst_p_int->latency = xzalloc_array(uint64_t, num_out_reqs);
  dst_p_int->spacing = xzalloc_array(uint64_t, num_out_reqs);
  if (!dst_p_int->num_extra_slots || !dst_p_int->num_out_frames
      || !dst_p_int->latency || !dst_p_int->spacing)
    goto error;

  memset(dst_p, 0, sizeof(profile_t));
  dst_p->id_ihash = src_p->id_ihash;

  dst_p_int->num_out_reqs = num_out_reqs;
  memcpy((void *) dst_p_int->num_extra_slots, (void *) src_p_int->num_extra_slots,
      sizeof(uint16_t)*num_out_reqs);
  memcpy((void *) dst_p_int->num_out_frames, (void *) src_p_int->num_out_frames,
      sizeof(uint64_t)*num_out_reqs);
  memcpy((void *) dst_p_int->latency, (void *) src_p_int->latency,
      sizeof(uint64_t)*num_out_reqs);
  memcpy((void *) dst_p_int->spacing, (void *) src_p_int->spacing,
      sizeof(uint64_t)*num_out_reqs);

  dst_p->priv = (void *) dst_p_int;
  INIT_LIST_HEAD(&dst_p->profile_listp);

  return;

error:
  if (dst_p_int->num_extra_slots)
    xfree(dst_p_int->num_extra_slots);
  if (dst_p_int->num_out_frames)
    xfree(dst_p_int->num_out_frames);
  if (dst_p_int->latency)
    xfree(dst_p_int->latency);
  if (dst_p_int->spacing)
    xfree(dst_p_int->spacing);
  xfree(dst_p_int);
}

void
print_profile(profile_t *p)
{
  int req_it;
  char sbuf[1024];
  char hbuf[64];
  int hbuf_len = 64;
  int slen = 1024;
  int soff = 0;
  profile_int_t *p_int = NULL;
  if (!p)
    return;
	
	memset(sbuf, 0, slen);
  soff = 0;

	memset(hbuf, 0, hbuf_len);
  prt_hash(p->id_ihash.byte, sizeof(ihash_t), hbuf);

  xprintk(LVL_INFO, "Profile ihash sent by guest: %s", hbuf);
  p_int = (profile_int_t *) p->priv;
  snprintf(sbuf + soff, slen-soff, "# reqs: %d\n", p_int->num_out_reqs);
  soff = strnlen(sbuf, slen);

  snprintf(sbuf + soff, slen-soff, "# out frames --- ");
  soff = strnlen(sbuf, slen);
  for (req_it = 0; req_it < p_int->num_out_reqs; req_it++) {
    snprintf(sbuf + soff, slen-soff, "%ld ", p_int->num_out_frames[req_it]);
    soff = strnlen(sbuf, slen);
  }
  snprintf(sbuf + soff, slen-soff, "\n");
  soff = strnlen(sbuf, slen);

  snprintf(sbuf + soff, slen-soff, "spacing --- ");
  soff = strnlen(sbuf, slen);
  for (req_it = 0; req_it < p_int->num_out_reqs; req_it++) {
    snprintf(sbuf + soff, slen-soff, "%lu ", p_int->spacing[req_it]);
    soff = strnlen(sbuf, slen);
  }
  snprintf(sbuf + soff, slen-soff, "\n");
  soff = strnlen(sbuf, slen);

  snprintf(sbuf + soff, slen-soff, "latency --- ");
  soff = strnlen(sbuf, slen);
  for (req_it = 0; req_it < p_int->num_out_reqs; req_it++) {
    snprintf(sbuf + soff, slen-soff, "%lu ", p_int->latency[req_it]);
    soff = strnlen(sbuf, slen);
  }
  snprintf(sbuf + soff, slen-soff, "\n");
  soff = strnlen(sbuf, slen);

  xprintk(LVL_DBG, "%s", sbuf);
}

void
init_one_profile(profile_t *prof, ihash_t pid)
{
  int i;
  uint16_t num_out_reqs;
  uint16_t *num_extra_slots;
  uint64_t *num_out_frames;
  uint64_t *latency, *spacing;
  uint64_t l_arr[100];

  memset((void *) &l_arr, 0, sizeof(uint64_t)*100);
  if (memcmp(pid.byte, DEFAULT_PROFILE_IHASH.byte, sizeof(ihash_t)) == 0) {
    num_out_reqs = 3;
    l_arr[0] = 10000000;
    l_arr[1] = 200000000;
    l_arr[2] = 300000000;
  } else if (memcmp(pid.byte, DEFAULT_HTTP_PROF_IHASH.byte, sizeof(ihash_t)) == 0) {
    num_out_reqs = 1;
    l_arr[0] = 200000;
//    l_arr[1] = 100000000;
  } else if (memcmp(pid.byte, DEFAULT_HTTPS_PROF_IHASH.byte, sizeof(ihash_t)) == 0) {
    num_out_reqs = 4;
    l_arr[0] = 200000;
    l_arr[1] = 1000000;
    l_arr[2] = 6000000;
    l_arr[3] = 10000000;
  } else if (memcmp(pid.byte, DEFAULT_MW_PROF_IHASH.byte, sizeof(ihash_t)) == 0) {
    num_out_reqs = 4;
    l_arr[0] = 200000;
    l_arr[1] = 1000000;
    l_arr[2] = 6000000;
    l_arr[3] = 10000000;
  } else if (memcmp(pid.byte, DEFAULT_VIDEO_PROF_IHASH.byte, sizeof(ihash_t)) == 0) {
    num_out_reqs = 50;
    l_arr[0] = 200000;
    l_arr[1] = 1000000;
    l_arr[2] = 6000000;
    l_arr[3] = 10000000;
    for (i = 4; i < 50; i++) {
      l_arr[i] = l_arr[i-1] + 5000000;
    }
  } else if (memcmp(pid.byte, pid1.byte, sizeof(ihash_t)) == 0) {
    num_out_reqs = 3;
    l_arr[0] = 200000;
    l_arr[1] = 20000000;
    l_arr[2] = 200500000;
  } else if (memcmp(pid.byte, pid2.byte, sizeof(ihash_t)) == 0) {
    num_out_reqs = 5;
    l_arr[0] = 200000;
    l_arr[1] = 20000000;
    l_arr[2] = 20050000;
    l_arr[3] = 20100000;
    l_arr[4] = 20200000;
  } else if (memcmp(pid.byte, pid3.byte, sizeof(ihash_t)) == 0) {
    num_out_reqs = 15;
    l_arr[0] = 200000;
    l_arr[1] = 20000000;
    l_arr[2] = 30000000;
    l_arr[3] = 40000000;
    l_arr[4] = 200000000;
    l_arr[5] = 210000000;
    l_arr[6] = 210500000;
    l_arr[7] = 211000000;
    l_arr[8] = 211500000;
    l_arr[9] = 211550000;
    l_arr[10] = 211600000;
    l_arr[11] = 211650000;
    l_arr[12] = 214000000;
    l_arr[13] = 215000000;
    l_arr[14] = 216000000;
//    for (i = 2; i < 15; i++)
//      l_arr[i] = l_arr[i-1] + 50000;
  } else {
    num_out_reqs = 4;
    l_arr[0] = 10000000;
    l_arr[1] = 200000000;
    l_arr[2] = 200005000;
    l_arr[3] = 200010000;
  }

  num_extra_slots = xzalloc_array(uint16_t, num_out_reqs);
  num_out_frames = xzalloc_array(uint64_t, num_out_reqs);
  latency = xzalloc_array(uint64_t, num_out_reqs);
  spacing = xzalloc_array(uint64_t, num_out_reqs);
  for (i = 0; i < num_out_reqs; i++) {
    num_extra_slots[i] = 0;
    num_out_frames[i] = 1;
    spacing[i] = 0;
    latency[i] = l_arr[i];
  }

  memset(prof, 0, sizeof(profile_t));
  init_profile(prof, 0, pid, num_out_reqs, num_extra_slots, num_out_frames,
      spacing, latency);

  print_profile(prof);
}

void
init_profiles(void *htable)
{
  profile_t *prof = NULL;
  sme_htable_t *htbl = (sme_htable_t *) htable;

  prof = xzalloc(profile_t);
  init_one_profile(prof, DEFAULT_PROFILE_IHASH);
  htable_insert(htbl, (void *) &prof->id_ihash, sizeof(ihash_t),
      &prof->profile_listp);

  prof = xzalloc(profile_t);
  init_one_profile(prof, DEFAULT_HTTP_PROF_IHASH);
  htable_insert(htbl, (void *) &prof->id_ihash, sizeof(ihash_t),
      &prof->profile_listp);

  prof = xzalloc(profile_t);
  init_one_profile(prof, DEFAULT_HTTPS_PROF_IHASH);
  htable_insert(htbl, (void *) &prof->id_ihash, sizeof(ihash_t),
      &prof->profile_listp);

  prof = xzalloc(profile_t);
  init_one_profile(prof, DEFAULT_MW_PROF_IHASH);
  htable_insert(htbl, (void *) &prof->id_ihash, sizeof(ihash_t),
      &prof->profile_listp);

  prof = xzalloc(profile_t);
  init_one_profile(prof, DEFAULT_VIDEO_PROF_IHASH);
  htable_insert(htbl, (void *) &prof->id_ihash, sizeof(ihash_t),
      &prof->profile_listp);

  prof = xzalloc(profile_t);
  init_one_profile(prof, pid1);
  htable_insert(htbl, (void *) &prof->id_ihash, sizeof(ihash_t),
      &prof->profile_listp);

  prof = xzalloc(profile_t);
  init_one_profile(prof, pid2);
  htable_insert(htbl, (void *) &prof->id_ihash, sizeof(ihash_t),
      &prof->profile_listp);

  prof = xzalloc(profile_t);
  init_one_profile(prof, pid3);
  htable_insert(htbl, (void *) &prof->id_ihash, sizeof(ihash_t),
      &prof->profile_listp);
}

void
init_ihashes(void)
{
  uint32_t def_prof_id = 100;
  char *profid = NULL;

  memset(DEFAULT_PROFILE_IHASH.byte, 0, sizeof(ihash_t));
  memcpy(DEFAULT_PROFILE_IHASH.byte, (void *) &def_prof_id, sizeof(uint32_t));

  profid = "http";
  memset(DEFAULT_HTTP_PROF_IHASH.byte, 0, sizeof(ihash_t));
  memcpy(DEFAULT_HTTP_PROF_IHASH.byte, profid, strlen(profid));

  profid = "https";
  memset(DEFAULT_HTTPS_PROF_IHASH.byte, 0, sizeof(ihash_t));
  memcpy(DEFAULT_HTTPS_PROF_IHASH.byte, profid, strlen(profid));

  profid = "MW";
  memset(DEFAULT_MW_PROF_IHASH.byte, 0, sizeof(ihash_t));
  memcpy(DEFAULT_MW_PROF_IHASH.byte, profid, strlen(profid));

  profid = "VIDEO";
  memset(DEFAULT_VIDEO_PROF_IHASH.byte, 0, sizeof(ihash_t));
  memcpy(DEFAULT_VIDEO_PROF_IHASH.byte, profid, strlen(profid));

  profid = "0x1234567890";
  memset(pid1.byte, 0, sizeof(ihash_t));
  memcpy(pid1.byte, profid, strlen(profid));

  def_prof_id = 2048;
  memset(pid2.byte, 0, sizeof(ihash_t));
  memcpy(pid2.byte, (void *) &def_prof_id, sizeof(uint32_t));

  def_prof_id = 16384;
  memset(pid3.byte, 0, sizeof(ihash_t));
  memcpy(pid3.byte, (void *) &def_prof_id, sizeof(uint32_t));

}

int
init_one_profile_from_buf(profile_t *p, char *buf, int buf_len)
{
  int ret = 0;
  int i;
  uint16_t off = 0;
  ihash_t prof_id_ihash;
  uint16_t num_out_reqs;
  uint16_t *num_extra_slots = NULL;
  uint64_t *num_out_frames = NULL;
  uint64_t *spacing = NULL;
  uint64_t *latency = NULL;

  if (!p)
    return -EINVAL;

  prof_id_ihash = ((ihash_t *) (buf + off))[0];
  off += sizeof(ihash_t);

  num_out_reqs = ((uint16_t *) (buf + off))[0];
  off += sizeof(uint16_t);

  num_extra_slots = xzalloc_array(uint16_t, num_out_reqs);
  num_out_frames = xzalloc_array(uint64_t, num_out_reqs);
  spacing = xzalloc_array(uint64_t, num_out_reqs);
  latency = xzalloc_array(uint64_t, num_out_reqs);
  if (!num_extra_slots || !num_out_frames || !spacing || !latency) {
    ret = -ENOMEM;
    goto error;
  }

  for (i = 0; i < num_out_reqs; i++) {
    num_extra_slots[i] = ((uint16_t *) (buf + off))[0];
    off += sizeof(uint16_t);
  }

  for (i = 0; i < num_out_reqs; i++) {
    num_out_frames[i] = ((uint64_t *) (buf + off))[0];
    off += sizeof(uint64_t);
  }

  for (i = 0; i < num_out_reqs; i++) {
    spacing[i] = ((uint64_t *) (buf + off))[0];
    off += sizeof(uint64_t);
  }

  for (i = 0; i < num_out_reqs; i++) {
    latency[i] = ((uint64_t *) (buf + off))[0];
    off += sizeof(uint64_t);
  }

  ret = init_profile(p, 0, prof_id_ihash, num_out_reqs, num_extra_slots,
      num_out_frames, spacing, latency);
  if (ret < 0)
    goto error;

  return 0;

error:
  if (num_extra_slots)
    xfree(num_extra_slots);
  if (num_out_frames)
    xfree(num_out_frames);
  if (spacing)
    xfree(spacing);
  if (latency)
    xfree(latency);
  return ret;
}

int
init_profiles_from_buf(sme_htable_t *pmap_htbl, char *buf, int buf_len)
{
  int ret = 0;
  int i;
  int off = 0;
  int prof_len_off = 0;
  uint16_t n_prof;
  uint64_t prof_off;
  char *prof_buf = NULL;
  profile_t *p = NULL;

  // skip the connection information for now
  off += ((4 * sizeof(uint32_t)) + (4 * sizeof(uint16_t)));

  n_prof = ((uint16_t *) (buf + off))[0];
  off += sizeof(uint16_t);

  xprintk(LVL_INFO, "# profiles %d", n_prof);

  prof_len_off = off;
  for (i = 0; i < n_prof; i++) {
    prof_off = ((uint64_t *) (buf + prof_len_off))[i];
    prof_buf = buf + prof_off;
    p = xzalloc(profile_t);
    if (!p) {
      ret = -ENOMEM;
      break;
    }

    ret = init_one_profile_from_buf(p, prof_buf, buf_len);
    if (ret < 0)
      break;

//    print_profile(p);

    htable_insert(pmap_htbl, (void *) &p->id_ihash, sizeof(ihash_t),
        &p->profile_listp);
  }

  if (ret < 0)
    goto error;

  return 0;

error:
  cleanup_htable(pmap_htbl);
  return ret;
}

/*
 * ====================================
 * profile hashtable specific functions
 * ====================================
 */

uint32_t
pmap_hash(void *key, int keylen, int modulo)
{
  uint32_t h = 0;
  ihash_t *ikey = (ihash_t *) key;
  memcpy((void *) &h, ikey->byte, sizeof(uint32_t));
  return h;
}

int
pmap_lookup(list_t *pmap_list, void *key, int klen, void **entry)
{
  profile_t *prof = NULL;
  ihash_t *ikey = (ihash_t *)key;
  list_for_each_entry(prof, pmap_list, profile_listp) {
    if (memcmp((void *) &prof->id_ihash, ikey, sizeof(ihash_t)) == 0) {
      *entry = prof;
      return 0;
    }
  }

  *entry = NULL;
  return -EINVAL;
}

int
pmap_free(list_t *pmap_list)
{
  list_t *prof_it, *prof_it2;
  profile_t *prof = NULL;

  list_for_each_safe(prof_it, prof_it2, pmap_list) {
    prof = list_entry(prof_it, profile_t, profile_listp);
    list_del_init(&prof->profile_listp);
    cleanup_profile(prof);
    xfree(prof);
    prof = NULL;
  }
  return 0;
}

hash_fn_t pmap_hash_fn = {
  .hash = pmap_hash,
  .lookup = pmap_lookup,
  .free = pmap_free,
};
