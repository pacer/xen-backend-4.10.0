/*
 * pacer.h
 *
 * created on: Jan 19, 2019
 * author: aasthakm
 *
 * xen_pacer API
 */

#ifndef __PACER_H__
#define __PACER_H__

#include "timerobj.h"
#include "pacer_obj.h"

void xen_pacer(void *arg);

int dequeue_profiles(pacer_obj_t *pobj, int n_dequeue_ops, s_time_t curr_epoch_start,
    u64 curr_epoch_tsc, int *other_count, int *other_overflow);
void xen_pacer_batch(void *arg);
void xen_pacer_batch2(void *arg);
void xen_pacer_batch3(void *arg);

#endif /* __PACER_H__ */
