#ifndef __OPENSSL_ST_H__
#define __OPENSSL_ST_H__

#include <asm/types.h>
#include <xen/xmalloc.h>
#include "xdebug.h"
#include "xconfig.h"

#define EVP_MAX_KEY_LENGTH    64
#define EVP_MAX_IV_LENGTH     16
#define EVP_MAX_BLOCK_LENGTH  32

#define STACK_OF(type)  struct stack_st_##type

struct evp_cipher_st;
struct evp_md_st;
struct engine_st;

struct asn1_string_st {
  int length;
  int type;
  unsigned char *data;
  long flags;
};
typedef struct asn1_string_st ASN1_INTEGER;
typedef struct asn1_string_st ASN1_ENUMERATED;
typedef struct asn1_string_st ASN1_BIT_STRING;
typedef struct asn1_string_st ASN1_OCTET_STRING;
typedef struct asn1_string_st ASN1_PRINTABLESTRING;
typedef struct asn1_string_st ASN1_T61STRING;
typedef struct asn1_string_st ASN1_IA5STRING;
typedef struct asn1_string_st ASN1_GENERALSTRING;
typedef struct asn1_string_st ASN1_UNIVERSALSTRING;
typedef struct asn1_string_st ASN1_BMPSTRING;
typedef struct asn1_string_st ASN1_UTCTIME;
typedef struct asn1_string_st ASN1_TIME;
typedef struct asn1_string_st ASN1_GENERALIZEDTIME;
typedef struct asn1_string_st ASN1_VISIBLESTRING;
typedef struct asn1_string_st ASN1_UTF8STRING;
typedef struct asn1_string_st ASN1_STRING;
typedef int ASN1_BOOLEAN;
typedef int ASN1_NULL;

typedef struct asn1_object_st {
  const char *sn, *ln;
  int nid;
  int length;
  const unsigned char *data;
  int flags;
} ASN1_OBJECT;

// just an opaque pointer
typedef struct ASN1_VALUE_st ASN1_VALUE;

typedef struct asn1_type_st {
  int type;
  union {
    char *ptr;
    ASN1_BOOLEAN boolean;
    ASN1_STRING *asn1_string;
    ASN1_OBJECT *object;
    ASN1_INTEGER *integer;
    ASN1_ENUMERATED *enumerated;
    ASN1_BIT_STRING *bit_string;
    ASN1_OCTET_STRING *octet_string;
    ASN1_PRINTABLESTRING *printablestring;
    ASN1_T61STRING *t61string;
    ASN1_IA5STRING *ia5string;
    ASN1_GENERALSTRING *generalstring;
    ASN1_BMPSTRING *bmpstring;
    ASN1_UNIVERSALSTRING *universalstring;
    ASN1_UTCTIME *utctime;
    ASN1_GENERALIZEDTIME *generalizedtime;
    ASN1_VISIBLESTRING *visiblestring;
    ASN1_UTF8STRING *utf8string;
    ASN1_STRING *set;
    ASN1_STRING *sequence;
    ASN1_VALUE *asn1_value;
  } value;
} ASN1_TYPE;

typedef struct crypto_ex_data_st {
  STACK_OF(void) *sk;
} CRYPTO_EX_DATA;

typedef int (*ENGINE_CIPHERS_PTR) (struct engine_st *,
    const struct evp_cipher_st **, const int **, int);
typedef int (*ENGINE_DIGESTS_PTR) (struct engine_st *,
    const struct evp_md_st **, const int **, int);

typedef struct engine_st {
  const char *id;
  const char *name;
  void *rsa_meth;
  void *dst_meth;
  void *dh_meth;
  void *ec_meth;
  void *rand_meth;
  ENGINE_CIPHERS_PTR ciphers;
  ENGINE_DIGESTS_PTR digests;
  void *pkey_meths;
  void *pkey_asn1_meths;
  void *destroy;
  void *init;
  void *finish;
  void *ctrl;
  void *load_privkey;
  void *load_pubkey;
  void *load_ssl_client_cert;
  void *cmd_defns;
  int flags;
  int struct_ref;
  int func_ref;
  CRYPTO_EX_DATA ex_data;
  struct engine_st *prev;
  struct engine_st *next;
} ENGINE;

typedef struct evp_cipher_ctx_st {
  struct evp_cipher_st *cipher;
  struct engine_st *engine;
  int encrypt;
  int buf_len;
  unsigned char oiv[EVP_MAX_IV_LENGTH];
  unsigned char iv[EVP_MAX_IV_LENGTH];
  unsigned char buf[EVP_MAX_BLOCK_LENGTH];
  int num;
  void *app_data;
  int key_len;
  unsigned long flags;
  void *cipher_data;
  int final_used;
  int block_mask;
  unsigned char final[EVP_MAX_BLOCK_LENGTH];
} EVP_CIPHER_CTX;

typedef struct evp_cipher_st {
  int nid;
  int block_size;
  int key_len;
  int iv_len;
  unsigned long flags;
  int (*init) (EVP_CIPHER_CTX *ctx, unsigned char *key,
              unsigned char *iv, int enc);
  int (*do_cipher) (EVP_CIPHER_CTX *ctx, unsigned char *out,
                    unsigned char *in, size_t in1);
  int (*cleanup) (EVP_CIPHER_CTX *ctx);
  int ctx_size;
  int (*set_asn1_parameters) (EVP_CIPHER_CTX *ctx, ASN1_TYPE *);
  int (*get_asn1_parameters) (EVP_CIPHER_CTX *ctx, ASN1_TYPE *);
  int (*ctrl) (EVP_CIPHER_CTX *, int type, int arg, void *ptr);
  void *app_data;
} EVP_CIPHER;

// replace in hypervisor/kernel appropriately
#define OPENSSL_zalloc(type) \
  xzalloc(type)
#define OPENSSL_zalloc_array(type, num) \
  xzalloc_array(type, num)
#define OPENSSL_free(addr)  \
  xfree(addr)
#define OPENSSL_cleanse(addr, len)  \
  memset(addr, 0, len)

// assertion handling
static inline void OPENSSL_die(const char *message, char *file, int line)
{
  xprintk(LVL_EXP, "%s:%d internal error %s\n", file, line, message);
//  exit(-1);
}

#define OPENSSL_FILE  __FILE__
#define OPENSSL_LINE  __LINE__

#define OPENSSL_assert(e) \
  (void) ((e) ? 0 : (OPENSSL_die("assertion failed: " #e, OPENSSL_FILE, OPENSSL_LINE), 1))

// something related to wrap and buffer overflows
#define EVP_CIPHER_CTX_FLAG_WRAP_ALLOW  0x1

// cipher modes
#define EVP_CIPH_ECB_MODE   0x1
#define EVP_CIPH_CBC_MODE   0x2
#define EVP_CIPH_CTR_MODE   0x5
#define EVP_CIPH_WRAP_MODE  0x10002
#define EVP_CIPH_MODE       0xF0007

// cipher flags
#define EVP_CIPH_CUSTOM_IV        0x10
#define EVP_CIPH_ALWAYS_CALL_INIT 0x20
#define EVP_CIPH_CTRL_INIT        0x40
#define EVP_CIPH_NO_PADDING       0x100
#define EVP_CIPH_FLAG_LENGTH_BITS 0x2000
#define EVP_CIPH_FLAG_CUSTOM_CIPHER 0x100000

// ctrl() values
#define EVP_CTRL_INIT       0x0

// related to error handling and reporting
#define EVP_F_EVP_DECRYPTFINAL_EX       100
#define EVP_F_EVP_CIPHERINIT_EX         123
#define EVP_F_EVP_CIPHER_CTX_CTRL       124
#define EVP_F_EVP_ENCRYPTFINAL_EX       127
#define EVP_F_AESNI_INIT_KEY            165
#define EVP_F_EVP_DECRYPTUPDATE         166
#define EVP_F_EVP_ENCRYPTUPDATE         167
#define EVP_F_EVP_ENCRYPTDECRYPTUPDATE  219

#define EVP_R_BAD_DECRYPT                       100
#define EVP_R_WRONG_FINAL_BLOCK_LENGTH          109
#define EVP_R_NO_CIPHER_SET                     131
#define EVP_R_CTRL_NOT_IMPLEMENTED              132
#define EVP_R_CTRL_OPERATION_NOT_IMPLEMENTED    133
#define EVP_R_INITIALIZATION_ERROR              134
#define EVP_R_DATA_NOT_MULTIPLE_OF_BLOCK_LENGTH 138
#define EVP_R_AES_KEY_SETUP_FAILED              143
#define EVP_R_INVALID_OPERATION                 148
#define EVP_R_PARTIALLY_OVERLAPPING             162
#define EVP_R_WRAP_MODE_NOT_ALLOWED             170

// generic errors
#define ERR_R_FATAL           64
#define ERR_R_MALLOC_FAILURE  (1|ERR_R_FATAL)

// library errors
#define ERR_LIB_EVP   6

// simplified version of original library function
static inline void ERR_put_error(int lib, int func, int reason,
    const char *file, int line)
{
  xprintk(LVL_EXP, "[%s:%d] Err lib %d f %d r %d", file, line, lib, func, reason);
//  exit(-1);
}

#define ERR_PUT_error(a,b,c,d,e)  ERR_put_error(a,b,c,d,e)
#define EVPerr(f,r)   ERR_PUT_error(ERR_LIB_EVP,(f),(r),OPENSSL_FILE,OPENSSL_LINE)

// AES-256 related values
#define NID_aes_256_ctr     906

#define AES_ENCRYPT     1
#define AES_DECRYPT     0

#define AES_MAXNR       14
#define AES_BLOCK_SIZE  16

typedef void (*block128_f) (const unsigned char in[16],
    unsigned char out[16], const void *key);
typedef void (*cbc128_f) (const unsigned char *in, unsigned char *out,
    size_t len, const void *key, unsigned char ivec[16], int enc);
typedef void (*ctr128_f) (const unsigned char *in, unsigned char *out,
    size_t blocks, const void *key, const unsigned char ivec[16]);

typedef struct aes_key_st {
  unsigned int rd_key[4 * (AES_MAXNR + 1)];
  int rounds;
} AES_KEY;

typedef struct {
  union {
    long align; // originally double, which is not a recognized type in kernel
    AES_KEY ks;
  } ks;
  block128_f block;
  union {
    cbc128_f cbc;
    ctr128_f ctr;
  } stream;
} EVP_AES_KEY;

#define EVP_C_DATA(kstruct, ctx)  \
  ((kstruct *) EVP_CIPHER_CTX_get_cipher_data(ctx))

#define STRICT_ALIGNMENT  1
#ifndef PEDANTIC
#undef STRICT_ALIGNMENT
#endif


#if 0
#undef GETU32
#define GETU32(p) (*((u32 *) (p)))
#endif

#if !defined(PEDANTIC)
#define BSWAP4(x) \
  ({ u32 ret_=(x);  \
   asm ("bswapl %0" : "+r"(ret_));  \
   ret_; })
#endif

#if defined(BSWAP4) && !defined(STRICT_ALIGNMENT)
#define GETU32(p)   BSWAP4(*(const u32 *) (p))
#define PUTU32(p,v) *(u32 *) (p) = BSWAP4(v)
#else
#define GETU32(p)   ((u32)(p)[0]<<24|(u32)(p)[1]<<16|(u32)(p)[2]<<8|(u32)(p)[3])
#define PUTU32(p,v) ((p)[0]=(u8)((v)>>24),(p)[1]=(u8)((v)>>16),(p)[2]=(u8)((v)>>8),(p)[3]=(u8)(v))
#endif

#define AESNI_CAPABLE 1

extern EVP_CIPHER_CTX *EVP_CIPHER_CTX_new(void);
extern int EVP_CIPHER_CTX_reset_shallow(EVP_CIPHER_CTX *ctx);
extern void EVP_CIPHER_CTX_free(EVP_CIPHER_CTX *ctx);
extern int EVP_EncryptInit_ex(EVP_CIPHER_CTX *ctx, EVP_CIPHER *cipher,
    ENGINE *impl, unsigned char *key, unsigned char *iv);
extern int EVP_DecryptInit_ex(EVP_CIPHER_CTX *ctx, EVP_CIPHER *cipher,
    ENGINE *impl, unsigned char *key, unsigned char *iv);
extern int EVP_EncryptUpdate(EVP_CIPHER_CTX *ctx, unsigned char *out, int *outl,
    unsigned char *in, int inl);
extern int EVP_EncryptFinal_ex(EVP_CIPHER_CTX *ctx, unsigned char *out, int *outl);
extern int EVP_DecryptUpdate(EVP_CIPHER_CTX *ctx, unsigned char *out, int *outl,
    unsigned char *in, int inl);
extern int EVP_DecryptFinal_ex(EVP_CIPHER_CTX *ctx, unsigned char *out, int *outl);

extern EVP_CIPHER *EVP_aes_256_ctr(void);

extern unsigned int OPENSSL_ia32cap_P[4];

#endif /* __OPENSSL_ST_H__ */
