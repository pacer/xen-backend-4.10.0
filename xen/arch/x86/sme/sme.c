#include <xen/hypercall.h>
#include <asm/hypercall.h>
#include <xen/guest_access.h>

#include <asm/cpufeature.h>

#include "xtxdata.h"
#include "mem_trans.h"
#include "timerobj.h"
#include "xprofq_hdr.h"
#include "xnicq_hdr.h"
#include "xstats.h"
#include "statq.h"
#include "xdebug.h"

#include "pacer_obj.h"
#include "pacer_utils.h"
#include "pacer.h"
#include "bench.h"
#include "tofree_q.h"

DEFINE_XEN_GUEST_HANDLE(sme_pacer_arg_t);
DEFINE_XEN_GUEST_HANDLE(msg_hdr_t);
DEFINE_XEN_GUEST_HANDLE(prof_update_arg_t);
int hc_set_addr_result;
uint32_t PMAP_HTBL_SIZE = 0;
uint32_t PAC_TIMERQ_SIZE = 0;
uint32_t NIC_DATA_QSIZE = 0;
uint32_t CWND_UPDATE_LATENCY = 0;
uint32_t REXMIT_UPDATE_LATENCY = 0;
uint32_t PACER_SPIN_THRESHOLD = 0;
uint32_t GLOBAL_NIC_DATA_QSIZE = 0;
uint32_t XEN_PACER_CORE = 0;
uint32_t HYPACE_CFG = 0;
uint32_t EPOCH_SIZE = 0;
uint32_t PTIMER_HEAP_SIZE = 0;
uint32_t MAX_DEQUEUE_TIME = 0;
uint32_t MAX_PROF_FREE_TIME = 0;
int N_HYPACE = 0;
int TCP_MTU = 0;
int CONFIG_SW_CSUM = 0;
int MAX_HP_PER_IRQ = 1;
int MAX_PKTS_PER_EPOCH = 0;
int MAX_OTHER_PER_EPOCH = 0;
s64 ALLOWANCE = 0;

#if SME_CONFIG_STAT
#if 0
SME_DECL_STAT(spin_count);
SME_DECL_STAT(delayed);
SME_DECL_STAT(event_delay);
SME_DECL_STAT_ARR(delay_after_spin, MAX_HP_ARGS);
SME_DECL_STAT_ARR(relevant_delay_after_spin, MAX_HP_ARGS);
SME_DECL_STAT_ARR(wakeup_delay, MAX_HP_ARGS);
SME_DECL_STAT_ARR(relevant_db_wakeup_delay, MAX_HP_ARGS);

SME_DECL_STAT(active_lock_delay);
SME_DECL_STAT(timer_lock_delay);
SME_DECL_STAT_ARR(heap_insert_delay, MAX_HP_ARGS);
SME_DECL_STAT_ARR(heap_remove_delay, MAX_HP_ARGS);

SME_DECL_STAT_ARR(dq_one_prof, MAX_HP_ARGS);
SME_DECL_STAT_ARR(dq_insert_active, MAX_HP_ARGS);

SME_DECL_STAT_ARR(pkt_prep_delay, MAX_HP_ARGS);
SME_DECL_STAT_ARR(nicq_rm_delay, MAX_HP_ARGS);
SME_DECL_STAT_ARR(prep_real_delay, MAX_HP_ARGS);
SME_DECL_STAT_ARR(prep_dummy_delay, MAX_HP_ARGS);
SME_DECL_STAT_ARR(dq_prof_count, MAX_HP_ARGS);
SME_DECL_STAT_ARR(free_prof_count, MAX_HP_ARGS);
SME_DECL_STAT_ARR(free_prof_delay, MAX_HP_ARGS);
SME_DECL_STAT_ARR(free_dummy_cnt, MAX_HP_ARGS);
SME_DECL_STAT_ARR(free_real_cnt, MAX_HP_ARGS);
SME_DECL_STAT_ARR(first_dummy_idx, MAX_HP_ARGS);
SME_DECL_STAT_ARR(last_real_idx, MAX_HP_ARGS);

SME_DECL_STAT_ARR(real_count, MAX_HP_ARGS);
SME_DECL_STAT_ARR(dummy_count, MAX_HP_ARGS);
SME_DECL_STAT_ARR(io_count, MAX_HP_ARGS);
SME_DECL_STAT_ARR(prof_switch_count, MAX_HP_ARGS);
SME_DECL_STAT_ARR(pkt_overflow_count, MAX_HP_ARGS);
SME_DECL_STAT_ARR(other_overflow_count, MAX_HP_ARGS);

SME_DECL_STAT_ARR(tot_io_delay, MAX_HP_ARGS);
SME_DECL_STAT_ARR(tot_spin_delay, MAX_HP_ARGS);
SME_DECL_STAT_ARR(real_io_delay, MAX_HP_ARGS);
SME_DECL_STAT_ARR(db_delay, MAX_HP_ARGS);
SME_DECL_STAT_ARR(relevant_db_delay, MAX_HP_ARGS);
SME_DECL_STAT_ARR(db_delay2, MAX_HP_ARGS);
SME_DECL_STAT_ARR(tot_prep_delay, MAX_HP_ARGS);

SME_DECL_STAT_ARR(sched_install, MAX_HP_ARGS);
SME_DECL_STAT_ARR(ack_update, MAX_HP_ARGS);
SME_DECL_STAT_ARR(rtx_update, MAX_HP_ARGS);
SME_DECL_STAT_ARR(prof_update1_delay, MAX_HP_ARGS);
SME_DECL_STAT_ARR(prof_update2_delay, MAX_HP_ARGS);
SME_DECL_STAT_ARR(cwnd_update3_delay, MAX_HP_ARGS);
SME_DECL_STAT_ARR(cwnd_update4_delay, MAX_HP_ARGS);

SME_DECL_STAT_ARR(nic_overflow_delay, MAX_HP_ARGS);
SME_DECL_STAT_ARR(bufenc, MAX_HP_ARGS);
SME_DECL_STAT_ARR(cpyenc, MAX_HP_ARGS);

#if 0
uint64_t nic_overflow_start;
uint64_t nic_overflow_end;
uint32_t nic_overflow_bd_start;
uint32_t nic_overflow_pkt_start;
uint32_t nic_overflow_prep_start;
#endif
#endif
#endif

#if 0
timerq_t pac_timerq;
timer_obj_t doorbell_timer;
active_timerq_t active_timerq;
#endif

uint64_t event_ts;
pacer_obj_t *pac_obj[MAX_HP_ARGS];

#if 0
xprofq_hdr_t guest_profq_hdr;
xprofq_freelist2_t guest_profq_fl;
xtxdata_t guest_txdata;
xglobal_nicq_hdr_t guest_nicq;
xglobal_nicq_hdr_t guest_dummy_nicq;
xnic_freelist_t guest_nfl;
xnic_txintq_t guest_txintq;
#endif
ihash_t DEFAULT_PROFILE_IHASH;
ihash_t DEFAULT_HTTP_PROF_IHASH;
ihash_t DEFAULT_HTTPS_PROF_IHASH;
ihash_t DEFAULT_MW_PROF_IHASH;
ihash_t DEFAULT_VIDEO_PROF_IHASH;
ihash_t pid1, pid2, pid3;
sme_htable_t pmap_htbl;
#if 0
union db_prod global_real_db;
union db_prod old_real_db;
generic_q_t nicstall_q;
#endif
s_time_t global_db_write_time = 0;
#if HYPACE_CONFIG_STAT
//sme_stat ***multid_stat;
//sme_stat ****prof_stat;
multi_stat_t ****prof_stat;
generic_q_t spurious_q;
int N_FREE = 20;
int N_DEQUEUE = 20;
int N_PREP = 30;
int N_OTHER = 30;
int N_PKT = 4;
int N_DB = 2;
#endif
ptimer_heap_t global_ptimer_heap;
int global_dummy_nicq_prod_idx = 0;
u16 old_real_bd_prod = 0;
u16 old_real_pkt_prod = 0;
tofreeq_t prof_tofreeq;
prof_update_q_t prof_updateq;

void
init_nic_txdata_arr(pacer_obj_t *pac_obj, sme_pacer_arg_t *xarg)
{
  pac_obj->nic_txdata_ptr_arr = xzalloc_array(nic_txdata_t *,
      (pac_obj->profq_hdr_vaddr->qsize-2) * NIC_DATA_QSIZE);
  xprintk(LVL_DBG, "nic ptr arr %p ptr[0] %p ptr[1] %p"
      , pac_obj->nic_txdata_ptr_arr, pac_obj->nic_txdata_ptr_arr[0]
      , pac_obj->nic_txdata_ptr_arr[1]
      );
}

void
cleanup_nic_txdata_arr(pacer_obj_t *pac_obj)
{
  int i;
  int n_elems = (pac_obj->profq_hdr_vaddr->qsize - 2) * NIC_DATA_QSIZE;

  if (pac_obj->nic_txdata_ptr_arr) {
    for (i = 0; i < n_elems; i++) {
      if (pac_obj->nic_txdata_ptr_arr[i])
        unmap_domain_page_global(pac_obj->nic_txdata_ptr_arr[i]);
    }

    xfree(pac_obj->nic_txdata_ptr_arr);
  }
}

void
init_vaddrs(pacer_obj_t *pac_obj, sme_pacer_arg_t *xarg, int hp_i)
{
  void *txdata_vaddr = NULL;
  void *db_vaddr = NULL;
  void *profq_hdr_vaddr = NULL;
  void *profq_fl_vaddr = NULL;
  void *nicq_vaddr = NULL;
  void *dummy_nicq_vaddr = NULL;
  void *nic_fl_vaddr = NULL;
  void *nic_txint_vaddr = NULL;

#if 0
  void *pkt_prod_vaddr = NULL;
  void *bd_prod_vaddr = NULL;
  void *swi_tx_pkt_prod_vaddr = NULL, *swi_tx_bd_prod_vaddr = NULL;
  void *txdesc_vaddr = NULL;
  uint32_t db_val = 0;
#endif

  txdata_vaddr = get_vaddr_from_guest_maddr(xarg->hparr[hp_i].txdata_maddr);
  db_vaddr = ioremap((paddr_t) xarg->hparr[hp_i].doorbell_maddr, 4);
  profq_hdr_vaddr = get_vaddr_from_guest_maddr(xarg->hparr[hp_i].profq_hdr_maddr);
  profq_fl_vaddr = get_vaddr_from_guest_maddr(xarg->hparr[hp_i].profq_fl_maddr);
  nicq_vaddr = get_vaddr_from_guest_maddr(xarg->hparr[hp_i].nicq_maddr);
  dummy_nicq_vaddr = get_vaddr_from_guest_maddr(xarg->hparr[hp_i].dummy_nicq_maddr);
  nic_fl_vaddr = get_vaddr_from_guest_maddr(xarg->hparr[hp_i].nic_fl_maddr);
  nic_txint_vaddr = get_vaddr_from_guest_maddr(xarg->hparr[hp_i].nic_txint_maddr);

  xprintk(LVL_DBG, "profq [%0lx %p] txdata ring sz %u\n"
      "doorbell [%0lx %p] txdata [%0lx %p]\n"
      , xarg->hparr[hp_i].profq_hdr_maddr, profq_hdr_vaddr
      , ((struct bnx2x_fp_txdata *) txdata_vaddr)->tx_ring_size
      , xarg->hparr[hp_i].doorbell_maddr, db_vaddr
      , xarg->hparr[hp_i].txdata_maddr, txdata_vaddr
      );

  pac_obj->txdata = txdata_vaddr;
  pac_obj->db_vaddr = db_vaddr;
  pac_obj->profq_hdr_vaddr = profq_hdr_vaddr;
  pac_obj->profq_fl_vaddr = profq_fl_vaddr;
  pac_obj->global_nicq_vaddr = nicq_vaddr;
  pac_obj->global_dummy_nicq_vaddr = dummy_nicq_vaddr;
  pac_obj->global_nic_fl_vaddr = nic_fl_vaddr;
  pac_obj->nic_txintq_vaddr = nic_txint_vaddr;

  xprintk(LVL_INFO,
      "nicq [%0lx %p] size %d #md %d dummy nicq [%0lx %p] size %d #md %d\n"
      "nicfl [%0lx %p] size %d #md %d"
      , xarg->hparr[hp_i].nicq_maddr, pac_obj->global_nicq_vaddr
      , pac_obj->global_nicq_vaddr->qsize, pac_obj->global_nicq_vaddr->n_md
      , xarg->hparr[hp_i].dummy_nicq_maddr, pac_obj->global_dummy_nicq_vaddr
      , pac_obj->global_dummy_nicq_vaddr->qsize
      , pac_obj->global_dummy_nicq_vaddr->n_md
      , xarg->hparr[hp_i].nic_fl_maddr, pac_obj->global_nic_fl_vaddr
      , pac_obj->global_nic_fl_vaddr->fl_size, pac_obj->global_nic_fl_vaddr->n_md
      );

#if CONFIG_NIC_QUEUE == CONFIG_NQ_STATIC
  init_nic_txdata_arr(pac_obj, xarg);
#endif

#if 0
  pkt_prod_vaddr =
    txdata_vaddr + offsetof(struct bnx2x_fp_txdata, tx_pkt_prod);
  bd_prod_vaddr =
    txdata_vaddr + offsetof(struct bnx2x_fp_txdata, tx_bd_prod);
  swi_tx_pkt_prod_vaddr =
    txdata_vaddr + offsetof(struct bnx2x_fp_txdata, swi_tx_pkt_prod);
  swi_tx_bd_prod_vaddr =
    txdata_vaddr + offsetof(struct bnx2x_fp_txdata, swi_tx_bd_prod);

  db_val = readl(db_vaddr);
  txdesc_vaddr = get_vaddr_from_guest_maddr(pac_obj->txdata->tx_desc_mapping);
  xprintk(LVL_DBG,
      "prod from grant table %p %u %p %u\n"
      "swi %p %u %p %u doorbell %u txdata prod %u "
      "txdesc maddr %0lx vaddr %p vaddr in struct %p\n"
      , pkt_prod_vaddr, *(uint16_t *) pkt_prod_vaddr
      , bd_prod_vaddr, *(uint16_t *) bd_prod_vaddr
      , swi_tx_pkt_prod_vaddr, *(uint16_t *) swi_tx_pkt_prod_vaddr
      , swi_tx_bd_prod_vaddr, *(uint16_t *) swi_tx_bd_prod_vaddr
      , db_val, pac_obj->txdata->tx_db.data.prod
      , pac_obj->txdata->tx_desc_mapping, txdesc_vaddr
      , pac_obj->txdata->tx_desc_ring
      );
  unmap_domain_page_global(txdesc_vaddr);
#endif
}

void
cleanup_vaddr(pacer_obj_t *pac_obj)
{
  if (pac_obj->txdata)
    unmap_domain_page_global(pac_obj->txdata);
  if (pac_obj->profq_fl_vaddr)
    unmap_domain_page_global(pac_obj->profq_fl_vaddr);
  if (pac_obj->profq_hdr_vaddr)
    unmap_domain_page_global(pac_obj->profq_hdr_vaddr);
  if (pac_obj->db_vaddr)
    iounmap(pac_obj->db_vaddr);
  if (pac_obj->global_nicq_vaddr)
    unmap_domain_page_global(pac_obj->global_nicq_vaddr);
  if (pac_obj->global_dummy_nicq_vaddr)
    unmap_domain_page_global(pac_obj->global_dummy_nicq_vaddr);
  if (pac_obj->global_nic_fl_vaddr)
    unmap_domain_page_global(pac_obj->global_nic_fl_vaddr);
  if (pac_obj->nic_txintq_vaddr)
    unmap_domain_page_global(pac_obj->nic_txintq_vaddr);

#if CONFIG_NIC_QUEUE == CONFIG_NQ_STATIC
  cleanup_nic_txdata_arr(pac_obj);
#endif
}

static void
init_all_stats(pacer_obj_t *pobj)
{
#if SME_CONFIG_STAT
  uint64_t start, end, step, dist_len;

  SME_PTR_STAT(spin_count, pobj);
  SME_PTR_STAT(delayed, pobj);
  SME_PTR_STAT(event_delay, pobj);
  SME_PTR_STAT_ARR(delay_after_spin, pobj);
  SME_PTR_STAT_ARR(relevant_delay_after_spin, pobj);
  SME_PTR_STAT_ARR(wakeup_delay, pobj);
  SME_PTR_STAT_ARR(relevant_db_wakeup_delay, pobj);

  SME_PTR_STAT(active_lock_delay, pobj);
  SME_PTR_STAT(timer_lock_delay, pobj);
  SME_PTR_STAT_ARR(heap_insert_delay, pobj);
  SME_PTR_STAT_ARR(heap_remove_delay, pobj);

  SME_PTR_STAT_ARR(dq_one_prof, pobj);
  SME_PTR_STAT_ARR(dq_insert_active, pobj);

  SME_PTR_STAT_ARR(pkt_prep_delay, pobj);
  SME_PTR_STAT_ARR(nicq_rm_delay, pobj);
  SME_PTR_STAT_ARR(prep_real_delay, pobj);
  SME_PTR_STAT_ARR(prep_dummy_delay, pobj);
  SME_PTR_STAT_ARR(dq_prof_count, pobj);
  SME_PTR_STAT_ARR(free_prof_count, pobj);
  SME_PTR_STAT_ARR(free_prof_delay, pobj);
  SME_PTR_STAT_ARR(free_dummy_cnt, pobj);
  SME_PTR_STAT_ARR(free_real_cnt, pobj);
  SME_PTR_STAT_ARR(first_dummy_idx, pobj);
  SME_PTR_STAT_ARR(last_real_idx, pobj);

  SME_PTR_STAT_ARR(real_count, pobj);
  SME_PTR_STAT_ARR(dummy_count, pobj);
  SME_PTR_STAT_ARR(io_count, pobj);
  SME_PTR_STAT_ARR(prof_switch_count, pobj);
  SME_PTR_STAT_ARR(pkt_overflow_count, pobj);
  SME_PTR_STAT_ARR(other_overflow_count, pobj);

  SME_PTR_STAT_ARR(tot_io_delay, pobj);
  SME_PTR_STAT_ARR(tot_spin_delay, pobj);
  SME_PTR_STAT_ARR(real_io_delay, pobj);
  SME_PTR_STAT_ARR(db_delay, pobj);
  SME_PTR_STAT_ARR(relevant_db_delay, pobj);
  SME_PTR_STAT_ARR(db_delay2, pobj);
  SME_PTR_STAT_ARR(tot_prep_delay, pobj);

  SME_PTR_STAT_ARR(sched_install, pobj);
  SME_PTR_STAT_ARR(ack_update, pobj);
  SME_PTR_STAT_ARR(rtx_update, pobj);
  SME_PTR_STAT_ARR(prof_update1_delay, pobj);
  SME_PTR_STAT_ARR(prof_update2_delay, pobj);
  SME_PTR_STAT_ARR(cwnd_update3_delay, pobj);
  SME_PTR_STAT_ARR(cwnd_update4_delay, pobj);

  SME_PTR_STAT_ARR(nic_overflow_delay, pobj);
  SME_PTR_STAT_ARR(bufenc, pobj);
  SME_PTR_STAT_ARR(cpyenc, pobj);

  event_ts = 0;

  start = 0;
  end = 1000000000;
  dist_len = 20;
  step = (end - start) / dist_len;

  SME_INIT_STAT(spin_count, "#spins", start, end, step);
  SME_INIT_STAT(delayed, "delay (ns)", start, end, step);
  SME_INIT_STAT(event_delay, "event delay (ns)", start, end, step);

  end = 20000;
  step = (end - start) / dist_len;
  SME_POBJ_INIT_STAT_ARR(delay_after_spin, start, 10000, 500, 1, pobj->pbo_idx,
      "delay after spin %d (ns)");
  SME_POBJ_INIT_STAT_ARR(relevant_delay_after_spin, start, 10000, 500, 1, pobj->pbo_idx,
      "relevant delay_after_spin %d (ns)");

  SME_INIT_STAT(active_lock_delay, "active list lock acquire (ns)", start, 1000, 50);
  SME_INIT_STAT(timer_lock_delay, "timer lock acquire (ns)", start, 1000, 50);
  SME_POBJ_INIT_STAT_ARR(heap_insert_delay, start, EPOCH_SIZE, 1000, 1, pobj->pbo_idx,
      "heap insert %d (ns)");
  SME_POBJ_INIT_STAT_ARR(heap_remove_delay, start, EPOCH_SIZE, 1000, 1, pobj->pbo_idx,
      "heap remove %d (ns)");

  SME_POBJ_INIT_STAT_ARR(dq_one_prof, start, end, step, 1, pobj->pbo_idx,
      "prof dequeue latency %d (ns)");
  SME_POBJ_INIT_STAT_ARR(dq_insert_active, start, end, step, 1, pobj->pbo_idx,
      "insert active latency %d (ns)");

  SME_POBJ_INIT_STAT_ARR(pkt_prep_delay, start, end, step, 1, pobj->pbo_idx,
      "pkt prep latency %d (ns)");
  SME_POBJ_INIT_STAT_ARR(nicq_rm_delay, start, end, step, 1, pobj->pbo_idx,
      "per-flow nicq latency %d (ns)");
  SME_POBJ_INIT_STAT_ARR(prep_real_delay, start, end, step, 1, pobj->pbo_idx,
      "real prepare latency %d (ns)");
  SME_POBJ_INIT_STAT_ARR(prep_dummy_delay, start, end, step, 1, pobj->pbo_idx,
      "dummy prepare latency %d (ns)");
  SME_POBJ_INIT_STAT_ARR(dq_prof_count, 0, 10, 1, 1, pobj->pbo_idx,
      "#profiles dequeued per epoch %d");
  SME_POBJ_INIT_STAT_ARR(free_prof_count, 0, 10, 1, 1, pobj->pbo_idx,
      "#profiles freed per epoch %d");
  SME_POBJ_INIT_STAT_ARR(free_prof_delay, start, end, step, 1, pobj->pbo_idx,
      "free profile latency %d (ns)");
  SME_POBJ_INIT_STAT_ARR(free_dummy_cnt, 0, 500, 10, 1, pobj->pbo_idx, "#dummy in prof %d");
  SME_POBJ_INIT_STAT_ARR(free_real_cnt, 0, 500, 10, 1, pobj->pbo_idx, "#real in prof %d");
  SME_POBJ_INIT_STAT_ARR(first_dummy_idx, 0, 550, 10, 1, pobj->pbo_idx, "first dummy %d");
  SME_POBJ_INIT_STAT_ARR(last_real_idx, 0, 550, 10, 1, pobj->pbo_idx, "last real %d");

  SME_POBJ_INIT_STAT_ARR(real_count, 0, EPOCH_SIZE/1000, 1, 1, pobj->pbo_idx,
      "real per batch %d (count)");
  SME_POBJ_INIT_STAT_ARR(dummy_count, 0, EPOCH_SIZE/1000, 1, 1, pobj->pbo_idx,
      "dummy per batch %d (count)");
  SME_POBJ_INIT_STAT_ARR(io_count, 0, EPOCH_SIZE/1000, 1, 1, pobj->pbo_idx,
      "total per batch %d (count)");
  SME_POBJ_INIT_STAT_ARR(prof_switch_count, 0, 1, 1, 1, pobj->pbo_idx,
      "prof switch %d (count)");
  SME_POBJ_INIT_STAT_ARR(pkt_overflow_count, 0, EPOCH_SIZE/1000, 1, 1, pobj->pbo_idx,
      "pkt overflow %d (count)");
  SME_POBJ_INIT_STAT_ARR(other_overflow_count, 0, EPOCH_SIZE/1000, 1, 1, pobj->pbo_idx,
      "other overflow %d (count)");

  SME_POBJ_INIT_STAT_ARR(tot_io_delay, 0, 1000, 100, 1, pobj->pbo_idx,
      "nic io latency %d (ns)");
  SME_POBJ_INIT_STAT_ARR(real_io_delay, start, end, step, 1, pobj->pbo_idx,
      "nic real io latency %d (ns)");

  end = EPOCH_SIZE;
  step = 1000;
  SME_POBJ_INIT_STAT_ARR(wakeup_delay, start, end, step, 1, pobj->pbo_idx,
      "wakeup delay %d (ns)");
  SME_POBJ_INIT_STAT_ARR(relevant_db_wakeup_delay, start, end, step, 1, pobj->pbo_idx,
      "relevant db wakeup delay %d (ns)");
  SME_POBJ_INIT_STAT_ARR(tot_spin_delay, start, end, step, 1, pobj->pbo_idx,
      "nic spin before io latency %d (ns)");

  SME_POBJ_INIT_STAT_ARR(db_delay, start, end, step, 1, pobj->pbo_idx,
      "db delay %d (ns)");
  SME_POBJ_INIT_STAT_ARR(relevant_db_delay, 0, 1000, 10, 1, pobj->pbo_idx,
      "relevant db delay %d (ns)");
  SME_POBJ_INIT_STAT_ARR(db_delay2, start, end, step, 1, pobj->pbo_idx,
      "db irq delay %d (ns)");
  SME_POBJ_INIT_STAT_ARR(tot_prep_delay, start, end, step, 1, pobj->pbo_idx,
      "tot prep delay %d (ns)");

  SME_POBJ_INIT_STAT_ARR(sched_install, start, 100000, 10000, 1, pobj->pbo_idx,
      "sched install latency %d (ns)");
  SME_POBJ_INIT_STAT_ARR(ack_update, start, 100000, 10000, 1, pobj->pbo_idx,
      "ack cwnd update latency %d (ns)");
  SME_POBJ_INIT_STAT_ARR(rtx_update, start, 100000, 10000, 1, pobj->pbo_idx,
      "rtx cwnd update latency %d (ns)");

  SME_POBJ_INIT_STAT_ARR(prof_update1_delay, start, 10000, 1000, 1, pobj->pbo_idx,
      "e1 replace prof latency %d (ns)");
  SME_POBJ_INIT_STAT_ARR(prof_update2_delay, start, 10000, 1000, 1, pobj->pbo_idx,
      "e2 extend prof latency %d (ns)");
  SME_POBJ_INIT_STAT_ARR(cwnd_update3_delay, start, 10000, 1000, 1, pobj->pbo_idx,
      "e3 cwnd ack latency %d (ns)");
  SME_POBJ_INIT_STAT_ARR(cwnd_update4_delay, start, 10000, 1000, 1, pobj->pbo_idx,
      "e4 cwnd loss latency %d (ns)");

  SME_POBJ_INIT_STAT_ARR(nic_overflow_delay, 0, 1000000000, 10000000, 1, pobj->pbo_idx,
      "NIC stall %d (ns)");
  SME_POBJ_INIT_STAT_ARR(bufenc, 0, 10000, 1000, 1, pobj->pbo_idx,
      "BUF ENC %d (ns)");
  SME_POBJ_INIT_STAT_ARR(cpyenc, 0, 10000, 1000, 1, pobj->pbo_idx,
      "CPY ENC %d (ns)");
#endif
}

static void
cleanup_all_stats(pacer_obj_t *pobj)
{
#if SME_CONFIG_STAT
  SME_PTR_STAT(spin_count, pobj);
  SME_PTR_STAT(delayed, pobj);
  SME_PTR_STAT(event_delay, pobj);
  SME_PTR_STAT_ARR(delay_after_spin, pobj);
  SME_PTR_STAT_ARR(relevant_delay_after_spin, pobj);
  SME_PTR_STAT_ARR(wakeup_delay, pobj);
  SME_PTR_STAT_ARR(relevant_db_wakeup_delay, pobj);

  SME_PTR_STAT(active_lock_delay, pobj);
  SME_PTR_STAT(timer_lock_delay, pobj);
  SME_PTR_STAT_ARR(heap_insert_delay, pobj);
  SME_PTR_STAT_ARR(heap_remove_delay, pobj);

  SME_PTR_STAT_ARR(dq_one_prof, pobj);
  SME_PTR_STAT_ARR(dq_insert_active, pobj);

  SME_PTR_STAT_ARR(pkt_prep_delay, pobj);
  SME_PTR_STAT_ARR(nicq_rm_delay, pobj);
  SME_PTR_STAT_ARR(prep_real_delay, pobj);
  SME_PTR_STAT_ARR(prep_dummy_delay, pobj);
  SME_PTR_STAT_ARR(dq_prof_count, pobj);
  SME_PTR_STAT_ARR(free_prof_count, pobj);
  SME_PTR_STAT_ARR(free_prof_delay, pobj);
  SME_PTR_STAT_ARR(free_dummy_cnt, pobj);
  SME_PTR_STAT_ARR(free_real_cnt, pobj);
  SME_PTR_STAT_ARR(first_dummy_idx, pobj);
  SME_PTR_STAT_ARR(last_real_idx, pobj);

  SME_PTR_STAT_ARR(real_count, pobj);
  SME_PTR_STAT_ARR(dummy_count, pobj);
  SME_PTR_STAT_ARR(io_count, pobj);
  SME_PTR_STAT_ARR(prof_switch_count, pobj);
  SME_PTR_STAT_ARR(pkt_overflow_count, pobj);
  SME_PTR_STAT_ARR(other_overflow_count, pobj);

  SME_PTR_STAT_ARR(tot_io_delay, pobj);
  SME_PTR_STAT_ARR(tot_spin_delay, pobj);
  SME_PTR_STAT_ARR(real_io_delay, pobj);
  SME_PTR_STAT_ARR(db_delay, pobj);
  SME_PTR_STAT_ARR(relevant_db_delay, pobj);
  SME_PTR_STAT_ARR(db_delay2, pobj);
  SME_PTR_STAT_ARR(tot_prep_delay, pobj);

  SME_PTR_STAT_ARR(sched_install, pobj);
  SME_PTR_STAT_ARR(ack_update, pobj);
  SME_PTR_STAT_ARR(rtx_update, pobj);
  SME_PTR_STAT_ARR(prof_update1_delay, pobj);
  SME_PTR_STAT_ARR(prof_update2_delay, pobj);
  SME_PTR_STAT_ARR(cwnd_update3_delay, pobj);
  SME_PTR_STAT_ARR(cwnd_update4_delay, pobj);

  SME_PTR_STAT_ARR(nic_overflow_delay, pobj);
  SME_PTR_STAT_ARR(bufenc, pobj);
  SME_PTR_STAT_ARR(cpyenc, pobj);

  SME_DEST_STAT(spin_count, 1);
  SME_DEST_STAT(delayed, 1);
  SME_DEST_STAT(event_delay, 1);
  SME_DEST_STAT_ARR(delay_after_spin, 1, 1);
  SME_DEST_STAT_ARR(relevant_delay_after_spin, 1, 1);
  SME_DEST_STAT_ARR(relevant_db_wakeup_delay, 1, 1);

  SME_DEST_STAT(active_lock_delay, 1);
  SME_DEST_STAT(timer_lock_delay, 1);
  SME_DEST_STAT_ARR(heap_insert_delay, 1, 1);
  SME_DEST_STAT_ARR(heap_remove_delay, 1, 1);

  SME_DEST_STAT_ARR(dq_one_prof, 1, 1);
  SME_DEST_STAT_ARR(dq_insert_active, 1, 1);

  SME_DEST_STAT_ARR(pkt_prep_delay, 1, 1);
  SME_DEST_STAT_ARR(nicq_rm_delay, 1, 1);
  SME_DEST_STAT_ARR(prep_real_delay, 1, 1);
  SME_DEST_STAT_ARR(prep_dummy_delay, 1, 1);
  SME_DEST_STAT_ARR(dq_prof_count, 1, 1);
  SME_DEST_STAT_ARR(free_prof_count, 1, 1);
  SME_DEST_STAT_ARR(free_prof_delay, 1, 1);
  SME_DEST_STAT_ARR(free_dummy_cnt, 1, 1);
  SME_DEST_STAT_ARR(free_real_cnt, 1, 1);
  SME_DEST_STAT_ARR(first_dummy_idx, 1, 1);
  SME_DEST_STAT_ARR(last_real_idx, 1, 1);

  SME_DEST_STAT_ARR(real_count, 1, 1);
  SME_DEST_STAT_ARR(dummy_count, 1, 1);
  SME_DEST_STAT_ARR(io_count, 1, 1);
  SME_DEST_STAT_ARR(prof_switch_count, 1, 1);
  SME_DEST_STAT_ARR(pkt_overflow_count, 1, 1);
  SME_DEST_STAT_ARR(other_overflow_count, 1, 1);
  SME_DEST_STAT_ARR(tot_io_delay, 1, 1);
  SME_DEST_STAT_ARR(real_io_delay, 1, 1);
  SME_DEST_STAT_ARR(wakeup_delay, 1, 1);
  SME_DEST_STAT_ARR(db_delay, 1, 1);
  SME_DEST_STAT_ARR(relevant_db_delay, 1, 1);
  SME_DEST_STAT_ARR(tot_spin_delay, 1, 1);
  SME_DEST_STAT_ARR(tot_prep_delay, 1, 1);
  SME_DEST_STAT_ARR(db_delay2, 1, 1);

  SME_DEST_STAT_ARR(sched_install, 1, 1);
  SME_DEST_STAT_ARR(ack_update, 1, 1);
  SME_DEST_STAT_ARR(rtx_update, 1, 1);

  SME_DEST_STAT_ARR(prof_update1_delay, 1, 1);
  SME_DEST_STAT_ARR(prof_update2_delay, 1, 1);
  SME_DEST_STAT_ARR(cwnd_update3_delay, 1, 1);
  SME_DEST_STAT_ARR(cwnd_update4_delay, 1, 1);

  SME_DEST_STAT_ARR(nic_overflow_delay, 1, 1);
  SME_DEST_STAT_ARR(bufenc, 1, 1);
  SME_DEST_STAT_ARR(cpyenc, 1, 1);
#endif
}

long
do_pacer_set_addrs(XEN_GUEST_HANDLE_PARAM(void) arg)
{
  int ret = 0;
  sme_pacer_arg_t xarg;
  struct cpuinfo_x86 *boot_cpuinfo_p = &boot_cpu_data;
  int i;

#if HYPACE_CONFIG_STAT
  multi_stat_t ****prof_stat = NULL;

  int prep_i, other_i, pkt_i, db_i;
  char *name_arr[2] = { "PKT", "TOT" };
//  int free_i, dequeue_i, prep_i;
#endif

  XEN_GUEST_HANDLE_PARAM(sme_pacer_arg_t) xops =
    guest_handle_cast(arg, sme_pacer_arg_t);

  hc_set_addr_result = 0;

  if (guest_handle_is_null(xops))
    return -EINVAL;

  if (!guest_handle_okay(xops, sizeof(sme_pacer_arg_t))) {
    xprintk(LVL_INFO, "xops.p %p", xops.p);
    return -EFAULT;
  }

  ret = __copy_from_guest(&xarg, xops, 1);
  if ( ret != 0 )
  {
    xprintk(LVL_ERR, "args copy failed, ret %d", ret);
    return ret;
  }

  N_HYPACE = xarg.n_hp_args;
  PMAP_HTBL_SIZE = xarg.pmap_htbl_size;
  PAC_TIMERQ_SIZE = xarg.pac_timerq_size;
  NIC_DATA_QSIZE = xarg.nic_data_qsize;
  CWND_UPDATE_LATENCY = xarg.cwnd_update_latency;
  REXMIT_UPDATE_LATENCY = xarg.rexmit_update_latency;
  PACER_SPIN_THRESHOLD = xarg.pacer_spin_threshold;
  GLOBAL_NIC_DATA_QSIZE = xarg.global_nic_data_qsize;
  XEN_PACER_CORE = xarg.hparr[0].pacer_core;
  EPOCH_SIZE = xarg.epoch_size;
  HYPACE_CFG = xarg.pacer_config;
  PTIMER_HEAP_SIZE = xarg.ptimer_heap_size;
  MAX_DEQUEUE_TIME = xarg.max_dequeue_time;
  MAX_PROF_FREE_TIME = xarg.max_prof_free_time;
  TCP_MTU = xarg.config_tcp_mtu;
  CONFIG_SW_CSUM = xarg.config_sw_csum;
  MAX_HP_PER_IRQ = xarg.max_hp_per_irq;
  MAX_PKTS_PER_EPOCH = xarg.max_pkts_per_epoch;
  MAX_OTHER_PER_EPOCH = xarg.max_other_per_epoch;
#if SME_CONFIG_STAT_SCALE == SCALE_NS
  ALLOWANCE = EPOCH_SIZE - PACER_SPIN_THRESHOLD;
#else
  ALLOWANCE = ns_diff_to_rdtsc(EPOCH_SIZE, PACER_SPIN_THRESHOLD);
#endif
  xprintk(LVL_EXP, "PMAP_HTBL_SIZE %u, PAC_TIMERQ_SIZE %u GLOBAL QSIZE %u "
      "PTIMER_HEAP_SIZE %u\n"
      "NIC_DATA_QSIZE %u SW_CSUM %d TCP_MTU %d ALLOWANCE %u %lu %lu "
      "PKTS_PER_EPOCH %u OTHER_PER_EPOCH %u #HYPACE %d\n"
      "CWND_LAT %u REXMIT_LAT %u PACER_SPIN %u PACER_CORE %u EPOCH_SIZE %u "
      "DEQUEUE_TIME %u FREE_TIME %u HP_PER_IRQ %d hypace cfg %d"
      , PMAP_HTBL_SIZE, PAC_TIMERQ_SIZE, GLOBAL_NIC_DATA_QSIZE, PTIMER_HEAP_SIZE
      , NIC_DATA_QSIZE, CONFIG_SW_CSUM, TCP_MTU
      , (EPOCH_SIZE - PACER_SPIN_THRESHOLD)
      , ns_diff_to_rdtsc(EPOCH_SIZE, PACER_SPIN_THRESHOLD)
      , tsc_ticks2ns(ALLOWANCE)
      , MAX_PKTS_PER_EPOCH, MAX_OTHER_PER_EPOCH, N_HYPACE
      , CWND_UPDATE_LATENCY, REXMIT_UPDATE_LATENCY, PACER_SPIN_THRESHOLD
      , XEN_PACER_CORE, EPOCH_SIZE, MAX_DEQUEUE_TIME, MAX_PROF_FREE_TIME
      , MAX_HP_PER_IRQ, HYPACE_CFG
      );

  xprintk(LVL_EXP, "sz pacer_obj %lu", sizeof(pacer_obj_t));
  xprintk(LVL_EXP, "CPU caps %0llx mfence idx %d val %d tsc idx %d val %d "
      "rdtscp idx %d val %d\n"
      "const tsc idx %d val %d nonstop tsc idx %d val %d "
      "reliable tsc idx %d val %d deadline tsc idx %d val %d\n"
      "invariant tsc idx %d val %d tsc adjust idx %d val %d"
      "XSAVE %d XSAVEOPT %d FXSR %d\n"
      "CR0 0x%0lx [MP 0x%x EM 0x%x TS 0x%x] "
      "CR4 0x%0lx [OSFXSR 0x%x OSXMMEXCPT 0x%0x OSXSAVE 0x%x]"
      , *(unsigned long long *) boot_cpuinfo_p->x86_capability
      , (int) X86_FEATURE_MFENCE_RDTSC
      , cpu_has(boot_cpuinfo_p, X86_FEATURE_MFENCE_RDTSC)
      , (int) X86_FEATURE_TSC
      , cpu_has(boot_cpuinfo_p, X86_FEATURE_TSC)
      , (int) X86_FEATURE_RDTSCP
      , cpu_has(boot_cpuinfo_p, X86_FEATURE_RDTSCP)
      , (int) X86_FEATURE_CONSTANT_TSC
      , cpu_has(boot_cpuinfo_p, X86_FEATURE_CONSTANT_TSC)
      , (int) X86_FEATURE_NONSTOP_TSC
      , cpu_has(boot_cpuinfo_p, X86_FEATURE_NONSTOP_TSC)
      , (int) X86_FEATURE_TSC_RELIABLE
      , cpu_has(boot_cpuinfo_p, X86_FEATURE_TSC_RELIABLE)
      , (int) X86_FEATURE_TSC_DEADLINE
      , cpu_has(boot_cpuinfo_p, X86_FEATURE_TSC_DEADLINE)
      , (int) X86_FEATURE_ITSC
      , cpu_has(boot_cpuinfo_p, X86_FEATURE_ITSC)
      , (int) X86_FEATURE_TSC_ADJUST
      , cpu_has(boot_cpuinfo_p, X86_FEATURE_TSC_ADJUST)
      , cpu_has(boot_cpuinfo_p, X86_FEATURE_XSAVE)
      , cpu_has(boot_cpuinfo_p, X86_FEATURE_XSAVEOPT)
      , cpu_has(boot_cpuinfo_p, X86_FEATURE_FXSR)
      , read_cr0()
      , (int) (read_cr0() & X86_CR0_MP)
      , (int) (read_cr0() & X86_CR0_EM)
      , (int) (read_cr0() & X86_CR0_TS)
      , read_cr4()
      , (int) (read_cr4() & X86_CR4_OSFXSR)
      , (int) (read_cr4() & X86_CR4_OSXMMEXCPT)
      , (int) (read_cr4() & X86_CR4_OSXSAVE)

      );

  for (i = 0; i < N_HYPACE; i++) {
    pac_obj[i] = xzalloc(pacer_obj_t);
  }
  /*
   * init profile hashtable
   */
  init_htable(&pmap_htbl, PMAP_HTBL_SIZE, &pmap_hash_fn);
#if 0
  init_ihashes();
  init_profiles((void *) &pmap_htbl);
#endif

  for (i = 0; i < N_HYPACE; i++) {
    init_vaddrs(pac_obj[i], &xarg, i);
  }

  /*
   * ideally should be initialized only once,
   * for now separate instance for every hypace core.
   */
  for (i = 0; i < N_HYPACE; i++) {
    init_x_profq_hdr(&pac_obj[i]->guest_profq_hdr, pac_obj[i]->profq_hdr_vaddr,
        pac_obj[i]->nic_txdata_ptr_arr);
    init_x_profq_freelist(&pac_obj[i]->guest_profq_fl, pac_obj[i]->profq_fl_vaddr);
  }

  for (i = 0; i < N_HYPACE; i++) {
    /*
     * each pac_obj maps a separate physical NIC queue
     */
    init_x_txdata(&pac_obj[i]->guest_txdata, pac_obj[i]->txdata, &xarg, i,
        &pac_obj[i]->global_real_db, &pac_obj[i]->old_real_db,
        &pac_obj[i]->old_real_bd_prod, &pac_obj[i]->old_real_pkt_prod);

    /*
     * each pac_obj maps and uses its own vaddrs for shared nic datastructures
     */
    init_x_global_nicq_hdr(&pac_obj[i]->guest_nicq, pac_obj[i]->global_nicq_vaddr);
    init_x_global_nicq_hdr(&pac_obj[i]->guest_dummy_nicq,
        pac_obj[i]->global_dummy_nicq_vaddr);
    init_x_nic_freelist(&pac_obj[i]->guest_nfl, pac_obj[i]->global_nic_fl_vaddr);
    init_x_nic_txintq(&pac_obj[i]->guest_txintq, pac_obj[i]->nic_txintq_vaddr);

    /*
     * init ptimer heap
     */
    init_ptimer_heap(&pac_obj[i]->global_ptimer_heap, PTIMER_HEAP_SIZE);
    init_tofreeq(&pac_obj[i]->prof_tofreeq, PAC_TIMERQ_SIZE);
    init_prof_update_q(&pac_obj[i]->prof_updateq, PAC_TIMERQ_SIZE);

    pac_obj[i]->pbo_idx = i;
    pac_obj[i]->fpu_set = 0;
    pac_obj[i]->global_dummy_nicq_prod_idx = 0;
    pac_obj[i]->nic_overflow_start = 0;
    pac_obj[i]->nic_overflow_end = 0;
    pac_obj[i]->nic_overflow_bd_start = 0;
    pac_obj[i]->nic_overflow_pkt_start = 0;
    pac_obj[i]->nic_overflow_prep_start = 0;

#if 0
    init_generic_q(pac_obj[i].pkts_q, i, SPURIOUS_EVENTS_QSIZE, pkts_elem_t,
        do_print_pkts_q);
#endif

#if HYPACE_CONFIG_STAT
    init_generic_q(pac_obj[i]->nicstall_q, i, SPURIOUS_EVENTS_QSIZE, nicstall_elem_t,
        do_print_nicstall_q);
    xprintk(LVL_EXP, "nicstall queue %p"
        , pac_obj[i]->nicstall_q.elem_arr
        );
#endif

#if CONFIG_PACER_CRYPTO == CRYPTO_MBEDTLS
    {
      int key_i;
    // Initialize key to 0xaa...aa
    for (key_i = 0; key_i < 32; key_i++) {
      pac_obj[i]->key[key_i] = 0xaa;
    }
    pac_obj[i]->nonce[0] = 0;
    pac_obj[i]->nonce[1] = 0;
    pac_obj[i]->nonce[2] = 0;
    memset(pac_obj[i]->stream_block, 0, 16);
    mbedtls_aes_setkey_enc(&pac_obj[i]->ctx, pac_obj[i]->key, 256);
    }
#endif

#if CONFIG_PACER_CRYPTO == CRYPTO_OPENSSL
    {
      unsigned char *key = (unsigned char *) "01234567890123456789012345678901";
      unsigned char *iv = (unsigned char *) "0123456789012345";
      init_aes(&pac_obj[i]->e_ctx, &pac_obj[i]->d_ctx,
          &pac_obj[i]->e_state, &pac_obj[i]->d_state, key, iv);
      memcpy(&pac_obj[i]->key, key, 32);
      memcpy(&pac_obj[i]->iv, iv, 16);
    }
#endif

  }

  for (i = 0; i < N_HYPACE; i++) {
    init_all_stats(pac_obj[i]);
  }

#if HYPACE_CONFIG_STAT
  N_PREP = EPOCH_SIZE / 1000;
  N_OTHER = EPOCH_SIZE / 1000;
  for (i = 0; i < N_HYPACE; i++) {
    prof_stat = (multi_stat_t ****) xzalloc_array(multi_stat_t ***, N_DB);
    for (db_i = 0; db_i < N_DB; db_i++) {
      prof_stat[db_i] = (multi_stat_t ***) xzalloc_array(multi_stat_t **, N_PKT);
      for (pkt_i = 0; pkt_i < N_PKT; pkt_i++) {
        prof_stat[db_i][pkt_i] = (multi_stat_t **) xzalloc_array(multi_stat_t *, N_PREP);
        for (prep_i = 0; prep_i < N_PREP; prep_i++) {
          prof_stat[db_i][pkt_i][prep_i] = (multi_stat_t *) xzalloc_array(multi_stat_t, N_OTHER);
          for (other_i = 0; other_i < N_OTHER; other_i++) {
            init_multi_stats_arr(&(prof_stat[db_i][pkt_i][prep_i][other_i]),
                0, EPOCH_SIZE, 1000, name_arr,
                "db %d type %d prep %d other %d (ns)", db_i, pkt_i, prep_i, other_i);
          }
        }
      }
    }
    pac_obj[i]->prof_stat = prof_stat;
  }

  for (i = 0; i < N_HYPACE; i++) {
    init_generic_q(pac_obj[i]->spurious_q, i, SPURIOUS_EVENTS_QSIZE, spurious_elem_t,
        do_print_spurious_q);
    xprintk(LVL_EXP, "spurious queue %p"
        , pac_obj[i]->spurious_q.elem_arr
        );
  }
#if 0
  multid_stat = (sme_stat ***) xzalloc_array(sme_stat **, N_FREE);
  for (free_i = 0; free_i < N_FREE; free_i++) {
    multid_stat[free_i] = (sme_stat **) xzalloc_array(sme_stat *, N_DEQUEUE);
    for (dequeue_i = 0; dequeue_i < N_DEQUEUE; dequeue_i++) {
      multid_stat[free_i][dequeue_i] = (sme_stat *) xzalloc_array(sme_stat, N_PREP);
      for (prep_i = 0; prep_i < N_PREP; prep_i++) {
        init_sme_stats_arr(&(multid_stat[free_i][dequeue_i][prep_i]),
            0, 20000, 1000, "free %d dq %d prep %d (ns)",
            free_i, dequeue_i, prep_i);
      }
    }
  }
#endif
#endif

  /*
   * init timerqueue
   */
  switch (HYPACE_CFG) {
    case HP_BATCH:
      {
        for (i = 0; i < N_HYPACE; i++) {
          init_timerq(&pac_obj[i]->pac_timerq, PAC_TIMERQ_SIZE, &xen_pacer_batch);
          init_active_timerq(&pac_obj[i]->active_timerq,
              pac_obj[i]->pac_timerq.queue);
          init_timer(&pac_obj[i]->doorbell_timer.xtimer, xen_pacer_batch,
              pac_obj[i], XEN_PACER_CORE + i);
          pac_obj[i]->global_db_write_time =
            get_s_time_fixed_at_cpu(0, XEN_PACER_CORE + i);
          timer_obj_set_timer(&pac_obj[i]->doorbell_timer,
              pac_obj[i]->global_db_write_time + EPOCH_SIZE, 0,
              PACER_SPIN_THRESHOLD);
        }
      }
      break;
#if 0
    case HP_BATCH2:
      {
        init_timerq(&pac_obj[0]->pac_timerq, PAC_TIMERQ_SIZE, &xen_pacer_batch2);
        init_active_timerq(&pac_obj[0]->active_timerq, pac_obj[0]->pac_timerq.queue);
        init_timer(&pac_obj[0]->doorbell_timer.xtimer, xen_pacer_batch2, pac_obj[0],
            XEN_PACER_CORE);
        pac_obj[0]->global_db_write_time = get_s_time_fixed_at_cpu(0, XEN_PACER_CORE);
        timer_obj_set_timer(&pac_obj[0]->doorbell_timer,
            pac_obj[0]->global_db_write_time + EPOCH_SIZE, 0, 0);
      }
      break;
    case HP_BATCH3:
      {
        init_timerq(&pac_obj[0]->pac_timerq, PAC_TIMERQ_SIZE, &xen_pacer_batch3);
        init_active_timerq(&pac_obj[0]->active_timerq, pac_obj[0]->pac_timerq.queue);
        init_timer(&pac_obj[0]->doorbell_timer.xtimer, xen_pacer_batch3, pac_obj[0],
            XEN_PACER_CORE);
        pac_obj[0]->global_db_write_time = get_s_time_fixed_at_cpu(0, XEN_PACER_CORE);
        timer_obj_set_timer(&pac_obj[0]->doorbell_timer,
            pac_obj[0]->global_db_write_time + EPOCH_SIZE, 0, 0);
      }
      break;
    case HP_ORIG:
    default:
      {
        init_timerq(&pac_timerq, PAC_TIMERQ_SIZE, &xen_pacer);
        init_active_timerq(&active_timerq, pac_timerq.queue);
        global_db_write_time = get_s_time_fixed_at_cpu(0, XEN_PACER_CORE);
      }
      break;
#endif
  }

  if (HYPACE_CFG) {
    for (i = 0; i < N_HYPACE; i++) {
      xprintk(LVL_EXP, "First hypace[%d] timer @ %ld"
          , i, pac_obj[i]->global_db_write_time + EPOCH_SIZE);
    }
  }

  hc_set_addr_result = 1;

  return 0;
}

long
do_pacer_unset_addrs(void)
{
  int i;
#if HYPACE_CONFIG_STAT
  int prep_i, other_i, db_i, pkt_i;
  multi_stat_t ****prof_stat = NULL;
//  int free_i, dequeue_i, prep_i;
#endif

  // if set addr hypercall failed, our datastructures have not been
  // initialized. avoid cleanup.
  if (hc_set_addr_result == 0)
    return -EFAULT;

  if (HYPACE_CFG) {
    for (i = 0; i < N_HYPACE; i++) {
      kill_timer(&pac_obj[i]->doorbell_timer.xtimer);
    }
  }

#if CONFIG_PACER_CRYPTO == CRYPTO_OPENSSL
  /*
   * cleanup crpyto memory
   */
  for (i = 0; i < N_HYPACE; i++) {
    free_aes(&pac_obj[i]->e_ctx, &pac_obj[i]->d_ctx);
  }
#endif

  /*
   * cleanup timerqueue
   */
  // TODO: implement cleanup properly
  for (i = 0; i < N_HYPACE; i++) {
    cleanup_active_timerq(&pac_obj[i]->global_ptimer_heap,
        &pac_obj[i]->active_timerq, &pac_obj[i]->pac_timerq);
    cleanup_timerq(&pac_obj[i]->global_ptimer_heap, &pac_obj[i]->pac_timerq);
    cleanup_prof_update_q(&pac_obj[i]->prof_updateq);
    cleanup_tofreeq(&pac_obj[i]->prof_tofreeq);
    cleanup_ptimer_heap(&pac_obj[i]->global_ptimer_heap);
  }
  cleanup_htable(&pmap_htbl);

  for (i = 0; i < N_HYPACE; i++) {
    cleanup_x_nic_txintq(&pac_obj[i]->guest_txintq);
    cleanup_x_nic_freelist(&pac_obj[i]->guest_nfl);
    cleanup_x_global_nicq_hdr(&pac_obj[i]->guest_nicq);
    cleanup_x_global_nicq_hdr(&pac_obj[i]->guest_dummy_nicq);
  }

  for (i = 0; i < N_HYPACE; i++) {
    cleanup_x_profq_freelist(&pac_obj[i]->guest_profq_fl);
    cleanup_x_profq_hdr(&pac_obj[i]->guest_profq_hdr);
  }

  for (i = 0 ; i < N_HYPACE; i++) {
    cleanup_x_txdata(&pac_obj[i]->guest_txdata, pac_obj[i]->txdata, 1);
  }

  if (HYPACE_CFG) {
    for (i = 0; i < N_HYPACE; i++) {
      xprintk(LVL_EXP, "REAL DB %u => %u ? %u bd_prod %u ? %u pkt_prod %u ? %u"
          , pac_obj[i]->txdata->tx_db.data.prod, pac_obj[i]->old_real_db.data.prod
          , pac_obj[i]->global_real_db.data.prod
          , pac_obj[i]->old_real_bd_prod, *(pac_obj[i]->guest_txdata.tx_bd_prod_p)
          , pac_obj[i]->old_real_pkt_prod, *(pac_obj[i]->guest_txdata.tx_pkt_prod_p)
          );
#if 1
      if (pac_obj[i]->old_real_bd_prod != *(pac_obj[i]->guest_txdata.tx_bd_prod_p)) {
        *(pac_obj[i]->guest_txdata.tx_bd_prod_p) = pac_obj[i]->old_real_bd_prod;
        *(pac_obj[i]->guest_txdata.tx_pkt_prod_p) = pac_obj[i]->old_real_pkt_prod;
        pac_obj[i]->txdata->tx_db.raw = pac_obj[i]->old_real_db.raw;
      }
#else
      writel((u32) pac_obj[i].txdata->tx_db.data.prod, pac_obj[i].db_vaddr);
#endif
      barrier();
      smp_mb();
    }
  }

  for (i = 0; i < N_HYPACE; i++) {
    cleanup_vaddr(pac_obj[i]);
  }

  xprintk(LVL_DBG, "%s", "unmapped and freed memory");

#if HYPACE_CONFIG_STAT
  for (i = 0; i < N_HYPACE; i++) {
    print_generic_q(pac_obj[i]->spurious_q);
    cleanup_generic_q(pac_obj[i]->spurious_q);
  }
  for (i = 0; i < N_HYPACE; i++) {
    print_generic_q(pac_obj[i]->nicstall_q);
    cleanup_generic_q(pac_obj[i]->nicstall_q);
  }
#endif

#if HYPACE_CONFIG_STAT
  for (i = 0; i < N_HYPACE; i++) {
    prof_stat = pac_obj[i]->prof_stat;
    xprintk(LVL_EXP, "=== HYPACE %d PROFILE ===", i);
    for (db_i = 0; db_i < N_DB; db_i++) {
      for (pkt_i = 0; pkt_i < N_PKT; pkt_i++) {
        for (prep_i = 0; prep_i < N_PREP; prep_i++) {
          for (other_i = 0; other_i < N_OTHER; other_i++) {
            print_multi_distribution(&(prof_stat[db_i][pkt_i][prep_i][other_i]));
            free_multi_stats(&(prof_stat[db_i][pkt_i][prep_i][other_i]));
          }
          xfree(prof_stat[db_i][pkt_i][prep_i]);
        }
        xfree(prof_stat[db_i][pkt_i]);
      }
      xfree(prof_stat[db_i]);
    }
    xfree(pac_obj[i]->prof_stat);
    pac_obj[i]->prof_stat = prof_stat = NULL;
  }

#if 0
  for (free_i = 0; free_i < N_FREE; free_i++) {
    for (dequeue_i = 0; dequeue_i < N_DEQUEUE; dequeue_i++) {
      for (prep_i = 0; prep_i < N_PREP; prep_i++) {
        print_distribution(&(multid_stat[free_i][dequeue_i][prep_i]));
        free_sme_stats(&(multid_stat[free_i][dequeue_i][prep_i]));
      }
      xfree(multid_stat[free_i][dequeue_i]);
    }
    xfree(multid_stat[free_i]);
  }
  xfree(multid_stat);
#endif
#endif

#if 0
  for (i = 0; i < N_HYPACE; i++) {
    print_generic_q(pac_obj[i]->pkts_q);
    cleanup_generic_q(pac_obj[i]->pkts_q);
  }
#endif

  for (i = 0; i < N_HYPACE; i++) {
    cleanup_all_stats(pac_obj[i]);
  }

  for (i = 0; i < N_HYPACE; i++) {
    xfree(pac_obj[i]);
  }

  return 0;
}

long
do_pacer_pmap_ihash(XEN_GUEST_HANDLE_PARAM(void) arg)
{
  int ret = 0;
  msg_hdr_t xarg;
  char *pmap_buf = NULL;
  int pmap_buf_len = 0;

  XEN_GUEST_HANDLE_PARAM(msg_hdr_t) xops =
    guest_handle_cast(arg, msg_hdr_t);

  if (guest_handle_is_null(xops))
    return -EINVAL;

  if (!guest_handle_okay(xops, sizeof(msg_hdr_t)))
    return -EFAULT;

  ret = __copy_from_guest(&xarg, xops, 1);
  if (ret != 0) {
    xprintk(LVL_ERR, "args copy failed, ret %d", ret);
    return ret;
  }

  pmap_buf_len = sizeof(msg_hdr_t) + xarg.length;
  if (!guest_handle_okay(arg, pmap_buf_len))
    return -EFAULT;

  pmap_buf = xzalloc_array(char, pmap_buf_len);
  if (!pmap_buf)
    return -ENOMEM;

  ret = __copy_from_guest((void *) pmap_buf, arg, pmap_buf_len);
  if (ret != 0) {
    xprintk(LVL_ERR, "pmap buf copy failed, ret %d", ret);
    if (pmap_buf)
      xfree(pmap_buf);

    return ret;
  }

  xprintk(LVL_INFO, "pmap buf copy len %d, ret %d", pmap_buf_len, ret);
  ret = init_profiles_from_buf(&pmap_htbl, pmap_buf+sizeof(msg_hdr_t),
      pmap_buf_len-sizeof(msg_hdr_t));
  xprintk(LVL_INFO, "htable insert ret %d", ret);

  if (pmap_buf)
    xfree(pmap_buf);

  return 0;
}

long
do_pacer_prof_update(XEN_GUEST_HANDLE_PARAM(void) arg)
{
  int ret = 0;
  prof_update_arg_t parg;

  XEN_GUEST_HANDLE_PARAM(prof_update_arg_t) pops =
    guest_handle_cast(arg, prof_update_arg_t);

  if (guest_handle_is_null(pops))
    return -EINVAL;

  if (!guest_handle_okay(pops, sizeof(prof_update_arg_t)))
    return -EFAULT;

  ret = __copy_from_guest(&parg, pops, 1);
  if (ret != 0) {
    xprintk(LVL_ERR, "args copy failed, ret %d", ret);
    return ret;
  }

  if (parg.profq_idx < 0 || parg.cmd_type < P_UPDATE_PROF_ID
      || parg.cmd_type > P_EXTEND_PROF_RETRANS) {
    xprintk(LVL_EXP, "Invalid arg %d extn %d", parg.profq_idx, parg.cmd_type);
    return -EINVAL;
  }

#if 0
  if (HYPACE_CFG < HP_BATCH) {
    profq_elem_t *gprof = NULL;
    gprof = guest_profq_hdr.queue[parg.profq_idx];
    ret = active_timerq_update_profile(NULL, &active_timerq, &pac_timerq, gprof,
        &pmap_htbl, parg.cmd_type, doorbell_timer.true_expires,
        &global_ptimer_heap);
  } else {
    put_prof_update_q(&pac_obj[parg.hypace_idx]->prof_updateq, &parg);
  }
#else
  put_prof_update_q(&pac_obj[parg.hypace_idx]->prof_updateq, &parg);
#endif

  return ret;
}

long
do_pacer_cwnd_update(XEN_GUEST_HANDLE_PARAM(void) arg)
{
  int ret = 0;
  prof_update_arg_t parg;

  XEN_GUEST_HANDLE_PARAM(prof_update_arg_t) pops =
    guest_handle_cast(arg, prof_update_arg_t);

  if (guest_handle_is_null(pops))
    return -EINVAL;

  if (!guest_handle_okay(pops, sizeof(prof_update_arg_t)))
    return -EFAULT;

  ret = __copy_from_guest(&parg, pops, 1);
  if (ret != 0) {
    xprintk(LVL_ERR, "args copy failed, ret %d", ret);
    return ret;
  }

  if (parg.profq_idx < 0 || parg.cmd_type < P_UPDATE_CWND_ACK
      || parg.cmd_type > P_UPDATE_CWND_LOSS) {
    xprintk(LVL_EXP, "Invalid arg %d", parg.profq_idx);
    return -EINVAL;
  }

#if 0
  if (HYPACE_CFG < HP_BATCH) {
    profq_elem_t *gprof = NULL;
    gprof = guest_profq_hdr.queue[parg.profq_idx];
    ret = active_timerq_update_cwnd(NULL, &active_timerq, &pac_timerq, gprof,
        parg.cmd_type, doorbell_timer.true_expires,
        &global_ptimer_heap);
  } else {
    put_prof_update_q(&pac_obj[parg.hypace_idx]->prof_updateq, &parg);
  }
#else
  put_prof_update_q(&pac_obj[parg.hypace_idx]->prof_updateq, &parg);
#endif

  return ret;
}

long
do_pacer_prof_free(XEN_GUEST_HANDLE_PARAM(void) arg)
{
  int ret = 0;
  prof_update_arg_t parg;

  XEN_GUEST_HANDLE_PARAM(prof_update_arg_t) pops =
    guest_handle_cast(arg, prof_update_arg_t);

  if (guest_handle_is_null(pops))
    return -EINVAL;

  if (!guest_handle_okay(pops, sizeof(prof_update_arg_t)))
    return -EFAULT;

  ret = __copy_from_guest(&parg, pops, 1);
  if (ret != 0) {
    xprintk(LVL_ERR, "args copy failed, ret %d", ret);
    return ret;
  }

  if (parg.profq_idx < 0) {
    xprintk(LVL_EXP, "Invalid arg %d", parg.profq_idx);
    return -EINVAL;
  }

#if 0
  if (HYPACE_CFG < HP_BATCH) {
    profq_elem_t *gprof = NULL;
    gprof = guest_profq_hdr.queue[parg.profq_idx];
    ret = active_timerq_free_profile(&active_timerq,
        gprof, &guest_profq_hdr, &guest_profq_fl,
        &pac_timerq);
  } else
#endif
  {
    tofreeq_elem_t tofree_elem;
    tofree_elem.profq_idx = parg.profq_idx;
    put_tofreeq(&pac_obj[parg.hypace_idx]->prof_tofreeq, &tofree_elem);
  }

  return ret;
}

long
do_pacer_wakeup2(void)
{

#if 0
  if (HYPACE_CFG < HP_BATCH) {
    return do_fetch_one_profile();
  } else {
    int other_count = 0, other_overflow = 0;
    dequeue_profiles(pac_obj[0], 1, doorbell_timer.true_expires,
        stime2tsc(doorbell_timer.true_expires), &other_count, &other_overflow);
    return 0;
  }
#endif

  return 0;
}

long
do_sme_pacer(unsigned int op, XEN_GUEST_HANDLE_PARAM(void) arg)
{

  switch ( op ) {
    case PACER_OP_SET_ADDRS:
      {
        return do_pacer_set_addrs(arg);
      }
      break;

    case PACER_OP_UNSET_ADDRS:
      {
        return do_pacer_unset_addrs();
      }
      break;

    case PACER_OP_PMAP_IHASH:
      {
        return do_pacer_pmap_ihash(arg);
      }
      break;

    case PACER_OP_WAKEUP:
      {
        return do_pacer_wakeup2();

      }
      break;

    case PACER_OP_PROF_UPDATE:
      {
        return do_pacer_prof_update(arg);
      }
      break;

    case PACER_OP_CWND_UPDATE:
      {
        return do_pacer_cwnd_update(arg);
      }
      break;

    case PACER_OP_PROF_FREE:
      {
        return do_pacer_prof_free(arg);
      }
      break;

    case PACER_OP_INIT_SPIN_BENCH:
      {
        return init_spin_bench(arg);
      }
      break;
    case PACER_OP_CLEANUP_SPIN_BENCH:
      {
        return cleanup_spin_bench(arg);
      }
      break;
    case PACER_OP_INIT_DB_BENCH:
      {
        return init_doorbell_bench(arg);
      }
      break;
    case PACER_OP_CLEANUP_DB_BENCH:
      {
        return cleanup_doorbell_bench(arg);
      }
      break;
  }

  return 0;
}

