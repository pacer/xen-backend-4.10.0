/*
 * sme_pkt.h
 *
 * created on: Jun 10, 2018
 * author: aasthakm
 *
 * API to manipulate dma'ed packet buffers in NIC queue
 */

#ifndef __SME_PKT_H__
#define __SME_PKT_H__

#include "sme_guest.h" // for tcphdr

#define DUMMY_IPHDR_OFF 14
#define DUMMY_TCPHDR_OFF 34
#define DUMMY_PAYLOAD_OFF 100

#define IPPROTO_TCP 6

typedef __u16 __bitwise __sum16;
typedef __u32 __bitwise __wsum;

// TODO: make guest send mac addresses, else packets will not be
// transmitted on other machines.

static inline void
macstr(char *mac, char *buf, int buflen)
{
  int i;
  char c;
  int off = 0;

  memset(buf, 0, buflen);

  for (i = 0; i < ETH_ALEN; i++) {
    c = mac[i];
    if (i == 0)
      snprintf(buf + off, 3, "%02x", (uint8_t) c);
    else
      snprintf(buf + off, 4, ":%02x", (uint8_t) c);
    off = strlen(buf);
  }
}

static inline __sum16 csum_fold(__wsum sum)
{
  asm("  addl %1,%0\n"
      "  adcl $0xffff,%0"
      : "=r" (sum)
      : "r" ((__force u32)sum << 16),
        "0" ((__force u32)sum & 0xffff0000));
  return (__force __sum16)(~(__force u32)sum >> 16);
}

static inline __wsum
csum_tcpudp_nofold(__be32 saddr, __be32 daddr, __u32 len, __u8 proto, __wsum sum)
{
  asm("  addl %1, %0\n"
      "  adcl %2, %0\n"
      "  adcl %3, %0\n"
      "  adcl $0, %0\n"
      : "=r" (sum)
      : "g" (daddr), "g" (saddr),
        "g" ((len + proto)<<8), "0" (sum));
  return sum;
}

static inline __sum16 csum_tcpudp_magic(__be32 saddr, __be32 daddr,
    __u32 len, __u8 proto,
    __wsum sum)
{
  return csum_fold(csum_tcpudp_nofold(saddr, daddr, len, proto, sum));
}

/*
 * Calculate(/check) TCP checksum
 */
static inline __sum16 tcp_v4_check(int len, __be32 saddr, __be32 daddr,
    __wsum base)
{
  return csum_tcpudp_magic(saddr, daddr, len, IPPROTO_TCP, base);
}

#ifndef do_csum
static inline unsigned short from32to16(unsigned int x)
{
	/* add up 16-bit and 16-bit for 16+c bit */
	x = (x & 0xffff) + (x >> 16);
	/* add up carry.. */
	x = (x & 0xffff) + (x >> 16);
	return x;
}

static unsigned int do_csum(const unsigned char *buff, int len)
{
	int odd;
	unsigned int result = 0;

	if (len <= 0)
		goto out;
	odd = 1 & (unsigned long) buff;
	if (odd) {
#ifdef __LITTLE_ENDIAN
		result += (*buff << 8);
#else
		result = *buff;
#endif
		len--;
		buff++;
	}
	if (len >= 2) {
		if (2 & (unsigned long) buff) {
			result += *(unsigned short *) buff;
			len -= 2;
			buff += 2;
		}
		if (len >= 4) {
			const unsigned char *end = buff + ((unsigned)len & ~3);
			unsigned int carry = 0;
			do {
				unsigned int w = *(unsigned int *) buff;
				buff += 4;
				result += carry;
				result += w;
				carry = (w > result);
			} while (buff < end);
			result += carry;
			result = (result & 0xffff) + (result >> 16);
		}
		if (len & 2) {
			result += *(unsigned short *) buff;
			buff += 2;
		}
	}
	if (len & 1)
#ifdef __LITTLE_ENDIAN
		result += *buff;
#else
		result += (*buff << 8);
#endif
	result = from32to16(result);
	if (odd)
		result = ((result >> 8) & 0xff) | ((result & 0xff) << 8);
out:
	return result;
}
#endif

static inline unsigned add32_with_carry(unsigned a, unsigned b)
{
  asm("addl %2,%0\n\t"
      "adcl $0,%0"
      : "=r" (a)
      : "0" (a), "rm" (b));
  return a;
}

static __wsum csum_partial(const void *buff, int len, __wsum sum)
{
  return (__force __wsum)add32_with_carry(do_csum(buff, len),
      (__force u32) sum);
}

#if 0
static inline __sum16 ip_fast_csum(const void *iph, unsigned int ihl)
{
  return (__force __sum16)~do_csum(iph, ihl*4);
}
#endif

/**
 * ip_fast_csum - Compute the IPv4 header checksum efficiently.
 * iph: ipv4 header
 * ihl: length of header / 4
 */
static inline __sum16 ip_fast_csum(const void *iph, unsigned int ihl)
{
	unsigned int sum;

	asm("  movl (%1), %0\n"
	    "  subl $4, %2\n"
	    "  jbe 2f\n"
	    "  addl 4(%1), %0\n"
	    "  adcl 8(%1), %0\n"
	    "  adcl 12(%1), %0\n"
	    "1: adcl 16(%1), %0\n"
	    "  lea 4(%1), %1\n"
	    "  decl %2\n"
	    "  jne	1b\n"
	    "  adcl $0, %0\n"
	    "  movl %0, %2\n"
	    "  shrl $16, %0\n"
	    "  addw %w2, %w0\n"
	    "  adcl $0, %0\n"
	    "  notl %0\n"
	    "2:"
	/* Since the input registers which are loaded with iph and ihl
	   are modified, we must also specify them as outputs, or gcc
	   will assume they contain their original values. */
	    : "=r" (sum), "=r" (iph), "=r" (ihl)
	    : "1" (iph), "2" (ihl)
	    : "memory");
	return (__force __sum16)sum;
}

static inline void ether_addr_copy(u8 *dst, const u8 *src)
{
#if defined(CONFIG_HAVE_EFFICIENT_UNALIGNED_ACCESS)
  *(u32 *) dst = *(const u32 *) src;
  *(u16 *) (dst + 4) = *(const u16 *) (src + 4);
#else
  u16 *a = (u16 *)dst;
  const u16 *b = (u16 *)src;

  a[0] = b[0];
  a[1] = b[1];
  a[2] = b[2];
#endif
}

static inline void populate_eth_hdr(struct ethhdr *eth, char *s_mac, char *d_mac)
{
  if (!eth)
    return;

  if (s_mac)
    ether_addr_copy((u8 *) eth->h_source, (u8 *) s_mac);
  if (d_mac)
    ether_addr_copy((u8 *) eth->h_dest, (u8 *) d_mac);

  eth->h_proto = htons(ETH_P_IP);
}

static inline void populate_ip_hdr(struct iphdr *iph, uint32_t skb_len,
    uint32_t ip_ihl, uint32_t s_addr, uint32_t d_addr, uint8_t protocol)
{
  if (!iph)
    return;

  iph->version = 4;
  iph->ihl = ip_ihl;
  iph->tot_len = htons(skb_len);
  iph->frag_off = htons(IP_DF);
  iph->protocol = protocol;
  iph->saddr = s_addr;
  iph->daddr = d_addr;
  iph->tos = 0;
  iph->ttl = 64;
  iph->id = 0;
  iph->check = 0;

  iph->check = ip_fast_csum((u8 *) iph, iph->ihl);
}

static inline void populate_tcp_hdr(struct tcphdr *tcph, uint32_t skb_len,
    int transport_hdr_len, uint32_t ip_ihl, uint32_t s_addr, uint32_t d_addr,
    uint16_t s_port, uint16_t d_port, uint32_t seqno, uint32_t seqack,
    uint32_t dummy_seq, uint8_t fin, uint8_t syn, uint8_t rst, uint8_t psh,
    uint8_t ack, uint8_t urg, uint8_t ece, uint8_t cwr, uint16_t window)
{
  int tcplen = 0;

  if (!tcph)
    return;

  tcph->source = htons(s_port);
  tcph->dest = htons(d_port);
  tcph->doff = transport_hdr_len/4;
  tcph->res1 = 0;
  tcph->cwr = cwr;
  tcph->ece = ece;
  tcph->urg = urg;
  tcph->ack = ack;
  tcph->psh = psh;
  tcph->rst = rst;
  tcph->syn = syn;
  tcph->fin = fin;
  tcph->seq = seqno;
  tcph->ack_seq = seqack;
  tcph->urg_ptr = dummy_seq;
  tcph->window = htons(window);
  tcph->check = 0;
  if (CONFIG_SW_CSUM) {
    tcplen = skb_len - (ip_ihl << 2);
    tcph->check = tcp_v4_check(tcplen, s_addr, d_addr,
        csum_partial((char *) tcph, tcplen, 0));
    xprintk(LVL_DBG, "PORT %u %u seq %u:%u len %d tcplen %d check 0x%0x 0x%0x"
        , s_port, d_port, seqno, seqack, skb_len, tcplen, tcph->check
        , ntohs(tcph->check)
        );
  }
}

static inline struct iphdr *get_iphdr_in_dummy(char *data)
{
  return (struct iphdr *) ((char *) data + DUMMY_IPHDR_OFF);
}

static inline struct tcphdr *get_tcphdr_in_dummy(char *data)
{
  return (struct tcphdr *) ((char *) data + DUMMY_TCPHDR_OFF);
}

static inline int set_hdr_in_dummy(char *data, char *s_mac, char *d_mac,
    uint32_t s_addr, uint32_t d_addr, uint16_t s_port, uint16_t d_port,
    uint32_t seqno, uint32_t seqack, uint16_t dummy_seq, uint16_t window)
{
#if SME_DEBUG_LVL <= LVL_INFO
  char s_str[16], d_str[16];
  char s_macstr[ETH_ALEN*3], d_macstr[ETH_ALEN*3];
#endif

  struct ethhdr *eth = NULL;
  struct iphdr *iph = NULL;
  struct tcphdr *tcph = NULL;
  int ip_ihl = 5;
  int transport_hdr_len = 32;

  int ip_tot_len = 0;
  int real_payload_len = 0;

  if (!data)
    return -EINVAL;

  eth = (struct ethhdr *) data;
  iph = get_iphdr_in_dummy(data);
  tcph = get_tcphdr_in_dummy(data);

  if (TCP_MTU == 1460)
    transport_hdr_len = 20;

#if SME_DEBUG_LVL <= LVL_INFO
  macstr(s_mac, s_macstr, ETH_ALEN*3);
  macstr(d_mac, d_macstr, ETH_ALEN*3);

  ipstr(iph->saddr, s_str);
  ipstr(iph->daddr, d_str);

  xprintk(LVL_DBG, "src %s %u dst %s %u seq %u:%u urg %u check %u\n"
      "MAC global src %s dst %s"
      , s_str, htons(tcph->source), d_str, htons(tcph->dest)
      , htonl(tcph->seq), htonl(tcph->ack_seq), tcph->urg_ptr, tcph->check
      , s_macstr, d_macstr
      );

  ipstr(s_addr, s_str);
  ipstr(d_addr, d_str);
#endif

  populate_eth_hdr(eth, s_mac, d_mac);
#if CONFIG_DUMMY == DUMMY_NOACK
  ip_tot_len = sizeof(struct iphdr) + transport_hdr_len + real_payload_len;
  populate_ip_hdr(iph, ip_tot_len, ip_ihl, s_addr, d_addr, IPPROTO_TCP);
  populate_tcp_hdr(tcph, ip_tot_len, transport_hdr_len, ip_ihl, s_addr, d_addr,
      s_port, d_port, seqno, seqack, 0, 0, 0, 0, 0, 1, 0, 0, 0, window);
#elif CONFIG_DUMMY == DUMMY_OOB
  real_payload_len = TCP_MTU;
  ip_tot_len = sizeof(struct iphdr) + transport_hdr_len + real_payload_len;
  populate_ip_hdr(iph, ip_tot_len, ip_ihl, s_addr, d_addr, IPPROTO_TCP);
  populate_tcp_hdr(tcph, ip_tot_len, transport_hdr_len, ip_ihl, s_addr, d_addr,
      s_port, d_port, seqno, seqack, 0, 0, 0, 0, 0, 1, 1, 0, 0, window);
#else
#error "Check CONFIG_DUMMY!"
  return -EINVAL;
#endif

#if SME_DEBUG_LVL <= LVL_INFO
  xprintk(LVL_DBG, "src %s %u dst %s %u seq %u:%u orig %u:%u "
      "urg %u dummy %u check %u\n"
      "MAC local src %s dst %s window %u"
      , s_str, htons(tcph->source), d_str, htons(tcph->dest)
      , htonl(tcph->seq), htonl(tcph->ack_seq), seqno, seqack
      , htons(tcph->urg_ptr), dummy_seq
      , tcph->check
      , s_macstr, d_macstr, htons(tcph->window)
      );
#endif

  return 0;
}

static inline int set_payload_in_dummy(void *data, uint16_t gid, uint16_t real,
    uint16_t dummy, uint16_t nxt, uint16_t num_timers, uint32_t cwm,
    uint16_t bd_prod, uint16_t bd_cons, uint16_t pkt_prod, uint16_t pkt_cons)
{
  int off = 0;

  if (!data)
    return -EINVAL;

  ((uint16_t *) data + off)[0] = gid;
  ((uint16_t *) data + off)[1] = real;
  ((uint16_t *) data + off)[2] = dummy;
  ((uint16_t *) data + off)[3] = nxt;
  ((uint16_t *) data + off)[4] = num_timers;
  ((uint16_t *) data + off)[5] = bd_prod;
  ((uint16_t *) data + off)[6] = bd_cons;
  ((uint16_t *) data + off)[7] = pkt_prod;
  ((uint16_t *) data + off)[8] = pkt_cons;
  off += (9 * sizeof(uint16_t));

  ((uint32_t *) data + off)[0] = cwm;

  return 0;
}

#endif /* __SME_PKT_H__ */
