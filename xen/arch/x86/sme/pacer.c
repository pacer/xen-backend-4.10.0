/*
 * pacer.c
 *
 * created on: Jan 19, 2019
 * author: aasthakm
 *
 * xen_pacer API
 */

#include "xconfig.h"
#include "xdebug.h"
#include "xstats.h"
#include "mem_trans.h"
#include "pacer_obj.h"
#include "sme_pkt.h"
#include "xprofq_hdr.h"
#include "xnicq_hdr.h"
#include "pacer_utils.h"
#include "pacer.h"
#include "xtxdata.h"
#include "xstats_decl.h"

extern union db_prod global_real_db;
extern union db_prod old_real_db;
extern union db_prod old_dummy_db;
extern s_time_t global_db_write_time;
extern uint64_t event_ts;

extern timerq_t pac_timerq;
extern active_timerq_t active_timerq;
extern pacer_obj_t pac_obj;
extern xtxdata_t guest_txdata;
extern xglobal_nicq_hdr_t guest_nicq;
extern xglobal_nicq_hdr_t guest_dummy_nicq;
extern xnic_freelist_t guest_nfl;
extern xnic_txintq_t guest_txintq;
extern sme_htable_t pmap_htbl;
extern int global_dummy_nicq_prod_idx;

/*
 * locks needed currently, to ensure that the timer obj is not freed,
 * although the a timer is scheduled in the timer obj, and is only triggering now.
 */
void
do_merged_nicq_doorbell_io(timer_obj_t *tobj)
{
  xtxdata_t *x_txdata = NULL;
  u16 bd_prod;
  profq_elem_t *gprof = NULL;
  xprofile_t *xprof = NULL;
  nic_txdata_t *nic_data_elem = NULL;
  struct eth_tx_start_bd *start_bd = NULL;
  u64 data_dma_addr = 0;
  void *data_vaddr = NULL;
  void *tcph = NULL;
#if SME_DEBUG_LVL <= LVL_INFO
  uint32_t print_seqno = 0, print_seqack = 0;
  uint16_t orig_bd_prod = 0;
  uint32_t old_prod, new_prod;
  int guest_prof_slot_idx = 0;
  int32_t push_timerq_idx = 0;
  int curr_idx = 0;
  char x_out_ipstr[16];
  char x_dst_ipstr[16];
#endif
  uint32_t seqno = 0, seqack = 0;
  char *s_mac = NULL, *d_mac = NULL;
  uint32_t s_addr = 0, d_addr = 0;
  uint16_t s_port = 0, d_port = 0;
  int nbd = 2;
#if !CONFIG_XEN_PACER_SLOTSHARE
  int cons_idx = 0;
#endif
  int send_real = 0;
  int tx_avail = 0;
  int iter = 0;
  s_time_t delay = 0;

  /*
   * irq save not needed right now, since we are already protecting
   * do_doorbell_io with spin lock on timer obj
   */
//  SME_INIT_TIMER(nicq_rm_delay);

//  SME_INIT_TIMER(prep_real_delay);
//  SME_INIT_TIMER(real_io_delay);

//  SME_INIT_TIMER(prep_dummy_delay);

//  SME_START_TIMER(nicq_rm_delay);

  xprof = &tobj->xen_profp;
  gprof = tobj->profq_addr;
  x_txdata = &guest_txdata;
  iter = 0;
  delay = 0;

#if SME_DEBUG_LVL <= LVL_INFO
  old_prod = x_txdata->tx_db->data.prod;
  orig_bd_prod = *(x_txdata->tx_bd_prod_p);
  guest_prof_slot_idx = gprof->profq_idx;
  push_timerq_idx = tobj - &pac_timerq.queue[0];
  curr_idx = tobj->timer_idx;
  ipstr(xprof->conn_id.out_ip, x_out_ipstr);
  ipstr(xprof->conn_id.dst_ip, x_dst_ipstr);
#endif

  /*
   * write to hw producer index and doorbell
   */

  tx_avail = bnx2x_tx_avail(x_txdata);

#if CONFIG_NIC_QUEUE == CONFIG_NQ_STATIC
  send_real = (gprof->nic_cons_idx != gprof->nic_prod_idx);
#else
  cons_idx = nicq_remove_first(gprof->pcm_nicq_p, &guest_nicq, gprof);
  send_real = ((cons_idx >= 0 && cons_idx < guest_nicq.qsize) ? 1 : 0);
#endif

//  SME_END_TIMER(nicq_rm_delay);

  bd_prod = TX_BD(x_txdata->tx_db->data.prod);
  start_bd = &x_txdata->tx_desc_ring[bd_prod].start_bd;

  if (send_real) {

//    SME_START_TIMER(prep_real_delay);

#if CONFIG_NIC_QUEUE == CONFIG_NQ_STATIC
    cons_idx = gprof->nic_cons_idx;
    nic_data_elem = tobj->nic_txdata_ptr_arr[gprof->nic_cons_idx];
    gprof->nic_cons_idx = (gprof->nic_cons_idx+1) % NIC_DATA_QSIZE;
#else
    nic_data_elem = guest_nicq.queue[cons_idx];
#endif

    nbd = prep_merged_nicq_slot(tobj, nic_data_elem, send_real, x_txdata,
        pac_obj.txdata, &guest_txintq);

    data_dma_addr = BD_UNMAP_ADDR(start_bd);
    data_vaddr = get_vaddr_from_guest_maddr(data_dma_addr);

    tcph = data_vaddr + 34;
    seqno = __cpu_to_be32(*(uint32_t *) (tcph + sizeof(uint32_t)));
    seqack = __cpu_to_be32(*(uint32_t *) (tcph + 2*sizeof(uint32_t)));

#if SME_DEBUG_LVL <= LVL_INFO
    print_seqno = seqno;
    print_seqack = seqack;
#endif

//    SME_END_TIMER(prep_real_delay);
  } else {
//    SME_START_TIMER(prep_dummy_delay);

    nic_data_elem = guest_dummy_nicq.queue[global_dummy_nicq_prod_idx];
    global_dummy_nicq_prod_idx =
      (global_dummy_nicq_prod_idx+1) % guest_dummy_nicq.qsize;

    nbd = prep_merged_nicq_slot(tobj, nic_data_elem, send_real, x_txdata,
        pac_obj.txdata, &guest_txintq);

    data_dma_addr = BD_UNMAP_ADDR(start_bd);
    data_vaddr = get_vaddr_from_guest_maddr(data_dma_addr);

    s_addr = xprof->conn_id.out_ip;
    d_addr = xprof->conn_id.dst_ip;
    s_port = xprof->conn_id.out_port;
    d_port = xprof->conn_id.dst_port;
    s_mac = gprof->src_mac_p;
    d_mac = gprof->dst_mac_p;
    seqack = *(gprof->last_real_ack_p);
    update_shared_pointers(gprof, &seqno);

#if SME_DEBUG_LVL <= LVL_INFO
    print_seqno = seqno;
    print_seqack = htonl(seqack);
#endif

    // in this case, we are getting the seq# from tcp_sock, so need to htonl
    set_hdr_in_dummy(data_vaddr, s_mac, d_mac, s_addr, d_addr, s_port, d_port,
        htonl(seqno), seqack, 0, gprof->last_skb_snd_window);

//    SME_END_TIMER(prep_dummy_delay);
  }

  // update max_seqno in shared profile before sending pkt on wire
  // incoming ack is guaranteed to arrive after max_seqno updated in prof
  if (seqno > gprof->max_seqno)
    gprof->max_seqno = seqno;

  x_txdata->tx_db->data.prod += nbd;

  /*
   * spin until the timer actually expires
   */
  while (NOW() < tobj->true_expires)
    iter++;

  delay = NOW() - tobj->true_expires;

//  SME_START_TIMER(real_io_delay);

  barrier();

  writel((u32) x_txdata->tx_db->raw, pac_obj.db_vaddr);

  barrier();
  smp_mb();

//  SME_END_TIMER(real_io_delay);

  if (send_real) {
    xprofile_update_gprof_packet_counter(xprof, gprof, 1, PKT_REAL, 0);

#if CONFIG_NIC_QUEUE == CONFIG_NQ_DYNAMIC
    free_nicq_slot(&guest_nfl, cons_idx);
#endif

  } else {
    xprofile_update_gprof_packet_counter(xprof, gprof, 1, PKT_DUMMY, 0);
  }

#if SME_CONFIG_STAT
  if (iter != 0) {
    SME_ADD_COUNT_POINT(spin_count, iter);
//    SME_ADD_COUNT_POINT(delay_after_spin, delay);
  } else {
    SME_ADD_COUNT_POINT(delayed, delay);
  }
#endif

#if SME_DEBUG_LVL <= LVL_INFO
  new_prod = x_txdata->tx_db->data.prod;

  xprintk(LVL_EXP, "T(%d:%d) G%d [%s %u, %s %u] expired %ld delay %ld iter %d "
      "P %d NXT %d CWM %d\n"
      "NIC DB %u => %u bd %u %u pkt %u %u"
      "\nmapping %0lx vaddr %p start_bd %p seq %u:%u max %u "
      "real %d dummy %d tx_avail %d SCONS %d"
      , push_timerq_idx, curr_idx, guest_prof_slot_idx
      , x_out_ipstr, xprof->conn_id.out_port
      , x_dst_ipstr, xprof->conn_id.dst_port, tobj->true_expires, delay
      , iter, gprof->num_timers, gprof->next_timer_idx, gprof->cwnd_watermark
      , old_prod, new_prod, orig_bd_prod, *x_txdata->tx_bd_cons_p
      , *x_txdata->tx_pkt_prod_p, *x_txdata->tx_pkt_cons_p
      , data_dma_addr, data_vaddr, start_bd
      , print_seqno, print_seqack, gprof->max_seqno
      , atomic_read(&gprof->real_counter), gprof->dummy_counter
      , tx_avail, cons_idx
      );
#endif
}

void
xen_pacer(void *arg)
{
  int curr_idx = 0;
  int ret = 0;
  uint64_t next_ts = 0;
#if SME_DEBUG_LVL <= LVL_DBG
  char out_ipstr[16];
  char dst_ipstr[16];
  char x_out_ipstr[16];
  char x_dst_ipstr[16];
  char x_hash[64];
  char hash[64];
  uint64_t old_prev_ts = 0;
  int old_timer_idx = 0;
#endif
  profq_elem_t *gprof = NULL;
  xprofile_t *xprof = NULL;
  timer_obj_t *tobj = (timer_obj_t *) arg;
  unsigned long xprof_flag = 0;
#if SME_CONFIG_STAT
  s_time_t now = 0;
  s_time_t wakeup_delay = 0;
#endif

  SME_INIT_TIMER(timer_lock_delay);

#if SME_CONFIG_STAT
  wakeup_delay = NOW() - (tobj->true_expires - PACER_SPIN_THRESHOLD);
//  SME_ADD_COUNT_POINT(wakeup_delay, wakeup_delay);
#endif

  SME_START_TIMER(timer_lock_delay);

  spin_lock_irqsave(&tobj->xen_profp.timers_lock, xprof_flag);

  SME_END_TIMER(timer_lock_delay);

  curr_idx = tobj - &pac_timerq.queue[0];

#if SME_CONFIG_STAT
  if (event_ts == 0)
    event_ts = NOW();
  else {
    now = NOW();
    SME_ADD_COUNT_POINT(event_delay, now - event_ts);
    event_ts = now;
  }
#endif

  /*
   * follow the pointer in tobj to profile entry in primary queue
   * if profile updated, and if not expired, skip doorbell writes
   */
  xprof = &tobj->xen_profp;
  gprof = tobj->profq_addr;

#if SME_DEBUG_LVL <= LVL_DBG
  ipstr(xprof->conn_id.out_ip, x_out_ipstr);
  ipstr(xprof->conn_id.dst_ip, x_dst_ipstr);
  ipstr(gprof->conn_id.out_ip, out_ipstr);
  ipstr(gprof->conn_id.dst_ip, dst_ipstr);
  prt_hash(xprof->prof_id.byte, sizeof(ihash_t), x_hash);
  prt_hash(gprof->id_ihash.byte, sizeof(ihash_t), hash);
  ret = xprofile_get_prev_ts_unlocked(xprof, gprof, &old_prev_ts);
  old_timer_idx = gprof->next_timer_idx;
#endif

  if (tobj->timer_idx == 0) {
    ret = replace_xprofile(xprof, gprof, &pmap_htbl, 0, 0);
    if (ret < 0) {
      xprof->pid_replace_status = PIDR_UNCHANGED;
      goto do_doorbell;
    }

    // switch profile
    reset_xprofile_unlocked(&tobj->xen_profp, gprof, &pmap_htbl, PIDR_REPLACED);

    // "replay" the first timer from the new profile first
    ret = xprofile_get_prev_ts_unlocked(xprof, gprof, &next_ts);

    if (ret >= 0) {
      timer_obj_set_timer(tobj, next_ts, ret, PACER_SPIN_THRESHOLD);
    }

#if SME_DEBUG_LVL <= LVL_DBG
    xprintk(LVL_INFO, "REPLACE T(%d:%d) xprof [%s %u, %s %u] TS %lu %ld truetime %ld\n"
        "prof [%s %u, %s %u] reqts %lu %ld\nfirst timer %lu => %lu hash %s => %s"
        , curr_idx, tobj->timer_idx, x_out_ipstr, xprof->conn_id.out_port
        , x_dst_ipstr, xprof->conn_id.dst_port
        , xprof->req_ts, xprof->req_stime, tobj->true_expires
        , out_ipstr, gprof->conn_id.out_port
        , dst_ipstr, gprof->conn_id.dst_port
        , gprof->req_ts, get_s_time_fixed2(gprof->req_ts)
        , old_prev_ts, next_ts, x_hash, hash
        );
#endif

    spin_unlock_irqrestore(&tobj->xen_profp.timers_lock, xprof_flag);

    goto do_poll;
  }

  if (gprof->next_timer_idx > gprof->cwnd_watermark && gprof->cwnd_watermark >= 0) {
    atomic_set(&gprof->is_paused, 1);
    xprof->last_pause_stime = NOW();
    gprof->next_timer_idx = gprof->cwnd_watermark;
#if SME_DEBUG_LVL <= LVL_DBG
    xprintk(LVL_INFO, "T(%d:%d) LATE PAUSE [%s %u, %s %u] "
        "#timers %d nxt %d => %d cwm %d real %d dummy %d"
        , curr_idx, tobj->timer_idx
        , x_out_ipstr, xprof->conn_id.out_port
        , x_dst_ipstr, xprof->conn_id.dst_port
        , gprof->num_timers, old_timer_idx, gprof->next_timer_idx
        , gprof->cwnd_watermark, atomic_read(&gprof->real_counter)
        , gprof->dummy_counter
        );
#endif

    spin_unlock_irqrestore(&tobj->xen_profp.timers_lock, xprof_flag);

    return;
  }

do_doorbell:

  /*
   * core of the xen_pacer timer interrupt handler
   * actually write to NIC doorbell
   */
  do_merged_nicq_doorbell_io(tobj);

  spin_unlock_irqrestore(&tobj->xen_profp.timers_lock, xprof_flag);

  /*
   * schedule next timer from this profile.
   * if profile completed, clean up the timer slot, i.e. remove
   * it from active_timerq, and add it back to free timerq.
   */
  do_set_next_timer(tobj);

do_poll:

  /*
   * check shared profile queue for a new profile whose first event
   * might be earlier than anything we have seen so far.
   */
  do_fetch_one_profile();
}

