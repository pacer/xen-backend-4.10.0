#ifndef __PTIMER_H__
#define __PTIMER_H__

#include <xen/time.h>
#include <xen/lib.h>
#include <xen/spinlock.h>

typedef struct ptimer {
  s_time_t expires;
  uint32_t heap_offset;
  void *parent_tobj;
} ptimer_t;

typedef struct ptimer_heap {
  int n_elems;
  int size;
  ptimer_t **arr;
  spinlock_t heap_lock;
} ptimer_heap_t;

#define GET_PHEAP_SIZE(pth)       ((pth)->n_elems)
#define SET_PHEAP_SIZE(pth, v)    ((pth)->n_elems = (v))

#define GET_PHEAP_LIMIT(pth)      ((pth)->size)
#define SET_PHEAP_LIMIT(pth, v)   ((pth)->size = (v))

int init_ptimer_heap(ptimer_heap_t *pth, int size);
void cleanup_ptimer_heap(ptimer_heap_t *pth);

int remove_from_ptimer_heap(ptimer_heap_t *pth, ptimer_t *pt);
int add_to_ptimer_heap(ptimer_heap_t *pth, ptimer_t *pt);

int set_ptimer(ptimer_heap_t *pth, ptimer_t *pt, s_time_t expires, void *tobj);
void stop_ptimer(ptimer_heap_t *pth, ptimer_t *pt);

#endif /* __PTIMER_H__ */
