/*
 * sme_guest.h
 *
 * created on: Mar 23, 2018
 * author: aasthakm
 *
 * datastructures shared with guest
 */

#ifndef __SME_GUEST_H__
#define __SME_GUEST_H__

#include <asm/atomic.h>
#include <xen/list.h>
#include <asm/byteorder.h>

#include "xconfig.h"
#include "sme_spinlock.h"

/*
 * ==================
 * skb datastructures
 * ==================
 */
#define MAX_SKB_FRAGS 17

/*
 * ========================
 * linux tcp datastructures
 * ========================
 */

#define ETH_ALEN 6
#define ETH_P_IP 0x0800
#define IP_DF 0x4000

/*
 * Ethernet frame header
 */
struct ethhdr {
  unsigned char h_dest[ETH_ALEN];
  unsigned char h_source[ETH_ALEN];
  __be16 h_proto;
} __attribute__((packed));

/*
 * IP header
 */
struct iphdr {
#if defined(__LITTLE_ENDIAN_BITFIELD)
  __u8 ihl:4,
       version:4;
#elif defined(__BIG_ENDIAN_BITFIELD)
  __u8 version:4,
       ihl:4;
#else
#error "Please fix <asm/byteorder.h>"
#endif
  __u8 tos;
  __be16 tot_len;
  __be16 id;
  __be16 frag_off;
  __u8 ttl;
  __u8 protocol;
  __be16 check;
  __be32 saddr;
  __be32 daddr;
  /* The options start here. */
};

/*
 * TCP header
 */
struct tcphdr {
  __be16 source;
  __be16 dest;
  __be32 seq;
  __be32 ack_seq;
#if defined(__LITTLE_ENDIAN_BITFIELD)
  __u16 res1:4,
        doff:4,
        fin:1,
        syn:1,
        rst:1,
        psh:1,
        ack:1,
        urg:1,
        ece:1,
        cwr:1;
#elif defined(__BIG_ENDIAN_BITFIELD)
  __u16 doff:4,
        res1:4,
        cwr:1,
        ece:1,
        urg:1,
        ack:1,
        psh:1,
        rst:1,
        syn:1,
        fin:1;
#else
#error "Adjust your <asm/byteorder.h> defines"
#endif
  __be16 window;
  __be16 check; // in linux this is __sum16, which is same as __be16
  __be16 urg_ptr;
};

#define ___htonl(x) __cpu_to_be32(x)
#define ___htons(x) __cpu_to_be16(x)
#define ___ntohl(x) __be32_to_cpu(x)
#define ___ntohs(x) __be16_to_cpu(x)

#define htonl(x) ___htonl(x)
#define ntohl(x) ___ntohl(x)
#define htons(x) ___htons(x)
#define ntohs(x) ___ntohs(x)

static inline bool before(__u32 seq1, __u32 seq2)
{
  return (__s32)(seq1-seq2) < 0;
}
#define after(seq2, seq1)   before(seq1, seq2)

/*
 * ====================
 * Pacer datastructures
 * ====================
 */

/*
 * hypercall args
 */
enum {
  PACER_OP_SET_ADDRS = 0,
  PACER_OP_UNSET_ADDRS,
  PACER_OP_WAKEUP,
  PACER_OP_PMAP_IHASH,
  PACER_OP_PROF_UPDATE,
  PACER_OP_CWND_UPDATE,
  PACER_OP_PROF_FREE,
  PACER_OP_INIT_SPIN_BENCH,
  PACER_OP_CLEANUP_SPIN_BENCH,
  PACER_OP_INIT_DB_BENCH,
  PACER_OP_CLEANUP_DB_BENCH,
  MAX_PACER_OPS
};

typedef struct mem_desc {
  uint64_t maddr;
  uint16_t n_elems;
} mem_desc_t;

typedef struct hypace_arg {
#define MAX_TXBUF_FRAGS 4
  uint16_t txbuf_n_md;
  mem_desc_t txbuf_md_arr[MAX_TXBUF_FRAGS];
  uint64_t txdata_maddr;
  uint64_t doorbell_maddr;
  uint64_t profq_hdr_maddr;
  uint64_t profq_fl_maddr;
  uint64_t nicq_maddr;
  uint64_t dummy_nicq_maddr;
  uint64_t nic_fl_maddr;
  uint64_t nic_txint_maddr;
  uint16_t pacer_core;
} hypace_arg_t;

typedef struct sme_pacer_arg {
  uint32_t pmap_htbl_size;
  uint32_t pac_timerq_size;
  uint32_t nic_data_qsize;
  uint32_t cwnd_update_latency;
  uint32_t rexmit_update_latency;
  uint32_t pacer_spin_threshold;
  uint32_t global_nic_data_qsize;
  uint32_t epoch_size;
  uint32_t ptimer_heap_size;
  uint32_t max_dequeue_time;
  uint32_t max_prof_free_time;
  uint32_t max_pkts_per_epoch;
  uint32_t max_other_per_epoch;
  uint16_t config_tcp_mtu;
  uint16_t config_sw_csum;
  uint16_t max_hp_per_irq;
  uint16_t pacer_config;
  uint16_t n_hp_args;
  hypace_arg_t hparr[MAX_HP_ARGS];
} sme_pacer_arg_t;

enum {
  P_UPDATE_PROF_ID = 0,
  P_EXTEND_PROF_RETRANS,
  P_UPDATE_CWND_ACK,
  P_UPDATE_CWND_LOSS,
  MAX_P_CMDS
};

typedef struct prof_update_arg {
  int32_t profq_idx;
  int32_t hypace_idx;
  int cmd_type;
} prof_update_arg_t;

/*
 * marker message
 */
enum {
  MSG_T_IPPORT = 0,
  MSG_T_PROFILE,
  MSG_T_PMAP,
  MSG_T_PROF_ID,
  MSG_T_MARKER,
  MSG_T_DATA,
  MSG_T_PROF_REQ,
  MSG_T_PROF_RSP,
  MSG_T_PMAP_IHASH,
  MSG_T_PMAP_IHASH_HASHTABLE,
  MAX_NUM_MSG_TYPES
};

typedef struct msg_hdr {
  uint64_t cmd;
  uint64_t length;
} msg_hdr_t;

typedef struct list_head list_t;

/*
 * profile ID hash
 */
typedef struct ihash {
  char byte[16];
} ihash_t;

/*
 * conn ID: pair of 5-tuple
 */
typedef struct conn {
  uint32_t src_ip;
  uint32_t in_ip;
  uint32_t out_ip;
  uint32_t dst_ip;
  uint16_t src_port;
  uint16_t in_port;
  uint16_t out_port;
  uint16_t dst_port;
} conn_t;

/*
 * structure of the profile in profile DB
 */
typedef struct profile_int {
  /*
   * #outgoing requests per incoming request
   */
  uint16_t num_out_reqs;
  /*
   * over provisioning for enforcement
   * for each outgoing request on the out channel
   */
  uint16_t *num_extra_slots;
  /*
   * size(msg) = n*MTU
   * array of #frames
   * for each outgoing request on the out channel
   */
  uint64_t *num_out_frames;
  /*
   * inter-frame time
   * for each outgoing request on the out channel
   */
  uint64_t *spacing;
  /*
   * latency for first packet of the response
   * for each outgoing response on the out channel
   */
  uint64_t *latency;
} profile_int_t;

enum {
  PSTATE_FREELIST = 0,
  PSTATE_PROFQ,
  PSTATE_XEN_COMPL,
};

/*
 * datastructure of the profile queue slot
 * shared with the guest
 */
typedef struct profile {
  ihash_t id_ihash;
  conn_t conn_id;
  void *priv;
  list_t profile_listp;
} profile_t;

typedef struct nic_txdata {
  uint64_t tx_tsc;
  uint64_t skb_maddr;
  atomic_t next;
  __le32 pbd2_parsing_data;
  __le32 txbd_addr_lo;
  __le32 txbd_addr_hi;
  __le16 txbd_nbytes;
  __le16 txbd_vlan_or_ethertype;
  u8 txbd_flags;
  u8 txbd_general_data;
  u8 txbuf_flags;
  u8 s_mac[ETH_ALEN];
  u8 d_mac[ETH_ALEN];
} nic_txdata_t;

typedef struct global_nicq_hdr {
  int qsize;
#define MAX_NICQ_FRAGS  4
  uint16_t n_md;
  mem_desc_t md_arr[MAX_NICQ_FRAGS];
  nic_txdata_t *queue;
} global_nicq_hdr_t;

typedef struct nicq_hdr {
  int qsize;
  atomic_t head;
  atomic_t tail;
} nicq_hdr_t;

typedef struct nic_fl_elem {
  int32_t offset;
} nic_fl_elem_t;

typedef struct nic_freelist {
  int fl_size;
  uint32_t prod_idx;
  uint32_t cons_idx;
#define MAX_NICFL_FRAGS 4
  uint16_t n_md;
  mem_desc_t md_arr[MAX_NICFL_FRAGS];
  nic_fl_elem_t *nfl_arr;
  // spinlock in guest, should not be accessed
} nic_freelist_t;

typedef struct nic_txint_elem {
  uint64_t skb_maddr;
  uint8_t is_real;
} nic_txint_elem_t;

typedef struct nic_txintq {
  int qsize;
#define MAX_TXINTQ_FRAGS 4
  uint16_t n_md;
  mem_desc_t md_arr[MAX_TXINTQ_FRAGS];
  nic_txint_elem_t *queue;
  nic_txint_elem_t **queue_p;
} nic_txintq_t;

typedef struct profq_elem {
  ihash_t id_ihash;
  conn_t conn_id;
  uint64_t req_ts;
  uint64_t ack_ts;
  uint64_t loss_ts;
  uint64_t prof_maddr;
  uint64_t cwnd_latency_shift;
  uint32_t profq_idx;
  uint32_t tobj_idx;
  uint32_t hypace_idx;
  atomic_t next;
  int32_t cwnd_watermark;
#if CONFIG_DUMMY == DUMMY_OOB
  uint32_t *shared_seq_p;
  uint32_t *shared_dummy_out_p;
  uint32_t *shared_dummy_lsndtime_p;
  uint64_t shared_seq_maddr;
  uint64_t shared_dummy_out_maddr;
  uint64_t shared_dummy_lsndtime_maddr;
#endif
  char *src_mac_p;
  uint64_t src_mac_maddr;
  char *dst_mac_p;
  uint64_t dst_mac_maddr;
  atomic_t real_counter;
  atomic_t is_paused;
  /*
   * # of dummies transmitted on this profile. used to
   * ensure we do not eat into cwnd due to transmitting dummies.
   */
  uint32_t dummy_counter;
  uint32_t initial_segs_out;
  uint32_t max_seqno;
  uint32_t tail;
  uint32_t *last_real_ack_p;
  uint64_t last_real_ack_maddr;
  uint16_t last_skb_snd_window;
  uint16_t id:14,
           state:2;
  uint32_t num_timers;
  uint32_t next_timer_idx;
#if !CONFIG_XEN_PACER_SLOTSHARE
#if CONFIG_NIC_QUEUE == CONFIG_NQ_STATIC
#define MAX_NDATA_FRAGS 2
  uint16_t n_md;
  mem_desc_t md_arr[MAX_NDATA_FRAGS];
  uint16_t nic_prod_idx;
  uint16_t nic_cons_idx;
  nic_txdata_t *nic_data;
  nic_txdata_t **nic_data_p; // unused in hypace
#else
  nicq_hdr_t *pcm_nicq_p;
  uint64_t pcm_nicq_maddr;
#endif /* CONFIG_NIC_QUEUE */
#endif /* CONFIG_XEN_PACER_SLOTSHARE */
  struct profile *pmap_prof;
} profq_elem_t;

typedef struct profq_hdr {
  uint64_t qsize;
  union {
    struct {
      atomic_t head;
      atomic_t tail;
      tkt_lock_t tkt_lock;
    } sorted2;
  } u;
#define MAX_QUEUE_FRAGS 4
  uint16_t n_md;
  mem_desc_t md_arr[MAX_QUEUE_FRAGS];
  profq_elem_t *queue;
  profq_elem_t **queue_p;
} profq_hdr_t;

/*
 * FIFO freelist for the profile queue slots,
 * shared with the guest.
 */
typedef struct profq_freelist2 {
  uint64_t idx_list_maddr;
  int64_t idx_list_size;
  atomic_t prod_idx;
  atomic_t cons_idx;
#define MAX_FL_FRAGS 4
  uint16_t n_md;
  mem_desc_t md_arr[MAX_FL_FRAGS];
  // virtual address of guest allocated memory, should not be accessed
  int *idx_list;
} profq_freelist2_t;


/*
 * ====================
 * bnx2x datastructures
 * ====================
 */
struct sw_tx_bd {
  void *skb;
  u16 first_bd;
  u8 flags;
};

struct doorbell_hdr {
  u8 header;
};

struct doorbell_set_prod {
//#if defined(__BIG_ENDIAN)
//  u16 prod;
//  u8 zero_fill1;
//  struct doorbell_hdr header;
//#elif defined(__LITTLE_ENDIAN)
  struct doorbell_hdr header;
  u8 zero_fill1;
  u16 prod;
//#endif
};

union db_prod {
  struct doorbell_set_prod data;
  u32 raw;
};

/*
 * structure for easy accessibility to assembler
 */
struct eth_tx_bd_flags {
  u8 as_bitfield;
};

/*
 * The eth Tx Buffer Descriptor
 */
struct eth_tx_start_bd {
  __le32 addr_lo;
  __le32 addr_hi;
  __le16 nbd;
  __le16 nbytes;
  __le16 vlan_or_ethertype;
  struct eth_tx_bd_flags bd_flags;
  u8 general_data;
};

/*
 * Tx regular BD structure
 */
struct eth_tx_bd {
  __le32 addr_lo;
  __le32 addr_hi;
  __le16 total_pkt_bytes;
  __le16 nbytes;
  u8 reserved[4];
};

/*
 * destination and source mac address.
 */
struct eth_mac_addresses {
#if defined(__BIG_ENDIAN)
	__le16 dst_mid;
	__le16 dst_lo;
#elif defined(__LITTLE_ENDIAN)
	__le16 dst_lo;
	__le16 dst_mid;
#endif
#if defined(__BIG_ENDIAN)
	__le16 src_lo;
	__le16 dst_hi;
#elif defined(__LITTLE_ENDIAN)
	__le16 dst_hi;
	__le16 src_lo;
#endif
#if defined(__BIG_ENDIAN)
	__le16 src_hi;
	__le16 src_mid;
#elif defined(__LITTLE_ENDIAN)
	__le16 src_mid;
	__le16 src_hi;
#endif
};

/* tunneling related data */
struct eth_tunnel_data {
	__le16 dst_lo;
	__le16 dst_mid;
	__le16 dst_hi;
	__le16 fw_ip_hdr_csum;
	__le16 pseudo_csum;
	u8 ip_hdr_start_inner_w;
	u8 flags;
};

/* union for mac addresses and for tunneling data.
 * considered as tunneling data only if (tunnel_exist == 1).
 */
union eth_mac_addr_or_tunnel_data {
	struct eth_mac_addresses mac_addr;
	struct eth_tunnel_data tunnel_data;
};

/*
 * Tx parsing BD structure for ETH E2
 */
struct eth_tx_parse_bd_e2 {
	union eth_mac_addr_or_tunnel_data data;
	__le32 parsing_data;
};

/*
 * Tx 2nd parsing BD structure for ETH packet
 */
struct eth_tx_parse_2nd_bd {
	__le16 global_data;
	u8 bd_type;
	u8 reserved3;
	u8 tcp_flags;
	u8 reserved4;
	u8 tunnel_udp_hdr_start_w;
	u8 fw_ip_hdr_to_payload_w;
	__le16 fw_ip_csum_wo_len_flags_frag;
	__le16 hw_ip_id;
	__le32 tcp_send_seq;
};

/* The last BD in the BD memory will hold a pointer to the next BD memory */
struct eth_tx_next_bd {
	__le32 addr_lo;
	__le32 addr_hi;
	u8 reserved[8];
};

union eth_tx_bd_types {
  struct eth_tx_start_bd start_bd;
  struct eth_tx_bd reg_bd;
  struct eth_tx_parse_bd_e2 parse_bd_e2;
  struct eth_tx_parse_2nd_bd parse_2nd_bd;
  struct eth_tx_next_bd next_bd;
};

struct bnx2x_fp_txdata {
  struct sw_tx_bd *tx_buf_ring;
  union eth_tx_bd_types *tx_desc_ring;
  u64 tx_desc_mapping;
  u32 cid;
  union db_prod tx_db;
  u16 tx_pkt_prod;
  u16 tx_pkt_cons;
  u16 tx_bd_prod;
  u16 tx_bd_cons;
  u16 swi_tx_pkt_prod;
  u16 swi_tx_bd_prod;
  unsigned long tx_pkt;
  __le16 *tx_cons_sb;
  int txq_index;
  void *parent_fp;
  int tx_ring_size;
};

static inline void bnx2x_set_fw_mac_addr(__le16 *fw_hi, __le16 *fw_mid,
    __le16 *fw_lo, u8 *mac)
{
  ((u8 *) fw_hi)[0]   = mac[1];
  ((u8 *) fw_hi)[1]   = mac[0];
  ((u8 *) fw_mid)[0]  = mac[3];
  ((u8 *) fw_mid)[1]  = mac[2];
  ((u8 *) fw_lo)[0]   = mac[5];
  ((u8 *) fw_lo)[1]   = mac[4];
}

#define SUB_S16(a, b)   (s16)((s16)(a) - (s16)(b))
#define BCM_PAGE_SHIFT		12
#define BCM_PAGE_SIZE		(1 << BCM_PAGE_SHIFT)

#define NUM_TX_RINGS		16
#define TX_DESC_CNT		(BCM_PAGE_SIZE / sizeof(union eth_tx_bd_types))
#define NEXT_PAGE_TX_DESC_CNT	1
#define MAX_TX_DESC_CNT		(TX_DESC_CNT - NEXT_PAGE_TX_DESC_CNT)
#define NUM_TX_BD		(TX_DESC_CNT * NUM_TX_RINGS)
#define MAX_TX_BD		(NUM_TX_BD - 1)
#define TX_BD(x)		((x) & MAX_TX_BD)
#define TX_BD_POFF(x)		((x) & MAX_TX_DESC_CNT)

#define NEXT_TX_IDX(x)		((((x) & MAX_TX_DESC_CNT) == \
				  (MAX_TX_DESC_CNT - 1)) ? \
					(x) + 1 + NEXT_PAGE_TX_DESC_CNT : \
					(x) + 1)

#define NEXT_CNT_PER_TX_PKT(bds)  \
  (((bds) + MAX_TX_DESC_CNT - 1)  / \
   MAX_TX_DESC_CNT * NEXT_PAGE_TX_DESC_CNT)

#define BDS_PER_TX_PKT  4
#define MAX_BDS_PER_TX_PKT  (MAX_SKB_FRAGS + BDS_PER_TX_PKT)
#define MAX_DESC_PER_TX_PKT (MAX_BDS_PER_TX_PKT + \
    NEXT_CNT_PER_TX_PKT(MAX_BDS_PER_TX_PKT))

#define HILO_U64(hi, lo)  ((((u64)(hi)) << 32) + (lo))

#define BD_UNMAP_ADDR(bd) HILO_U64(le32_to_cpu((bd)->addr_hi),  \
    le32_to_cpu((bd)->addr_lo))

#endif /* __SME_GUEST_H__ */
