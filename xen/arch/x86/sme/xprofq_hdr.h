/*
 * xprofq_hdr.h
 *
 * created on: May 17, 2018
 * author: aasthakm
 *
 * xen local version of shared profq_hdr struct
 */

#ifndef __XPROFQ_HDR_H__
#define __XPROFQ_HDR_H__

#include "sme_spinlock.h"
#include "sme_guest.h"

//#define PROF_REF_MASK (_AC(1,UL) << 30)
//#define PROF_REF_UNMASK ~(_AC(1,UL) << 30)

#define PROF_REF_MASK (1 << 30)
#define PROF_REF_UNMASK ~(1 << 30)

typedef struct xprofq_hdr {
  // readonly
  uint64_t qsize;
  atomic_t head;
  atomic_t tail;
  tkt_lock_t *lock_p;
  // local copy of pointers to physical pages
  profq_elem_t **queue;
} xprofq_hdr_t;

void init_x_profq_hdr(xprofq_hdr_t *xprofq_hdr, profq_hdr_t *gprofq_hdr,
    nic_txdata_t **nic_txdata_ptr_arr);
void cleanup_x_profq_hdr(xprofq_hdr_t *xprofq_hdr);

typedef struct xprofq_freelist2 {
  uint64_t idx_list_size;
  // pointer to idx fields in profq_freelist2
  atomic_t *prod_idx_p;
  atomic_t *cons_idx_p;
  int32_t **idx_list_vaddr_p;
} xprofq_freelist2_t;

typedef struct profq_key {
  conn_t conn_id;
  uint64_t req_ts;
  int64_t first_latency;
} profq_key_t;

void init_x_profq_freelist(xprofq_freelist2_t *xprofq_fl,
    profq_freelist2_t *gprofq_fl);
void cleanup_x_profq_freelist(xprofq_freelist2_t *xprofq_fl);

int is_marked_reference(int idx);
int get_unmarked_reference(int idx);
int get_marked_reference(int idx);

int remove(xprofq_hdr_t *profq_hdr, profq_key_t *key);
int remove_first(xprofq_hdr_t *profq_hdr);
int remove_locked(xprofq_hdr_t *profq_hdr, int *earliest_idx);
int put_one_profq_freelist2(xprofq_freelist2_t *xprofq_fl, int32_t idx);

#endif /* __XPROFQ_HDR_H__ */
