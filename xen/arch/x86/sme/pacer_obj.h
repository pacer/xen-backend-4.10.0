/*
 * pacer_obj.h
 *
 * created on: Jan 19, 2018
 * author: aasthakm
 *
 * data structure to hold all guest shared datastructures
 */

#ifndef __PACER_OBJ_H__
#define __PACER_OBJ_H__

#include "sme_guest.h"
#include "timerobj.h"
#include "xtxdata.h"
#include "xnicq_hdr.h"
#include "tofree_q.h"
#include "statq.h"

#include "aes.h"
#include "openssl_wrap.h"

#include "xstats.h"

typedef struct pacer_obj {
  // xen local fields
  s_time_t global_db_write_time;
  int16_t pbo_idx;
  int16_t fpu_set;
  int global_dummy_nicq_prod_idx;
  int nic_overflow_start;
  int nic_overflow_end;
  int nic_overflow_bd_start;
  int nic_overflow_pkt_start;
  int nic_overflow_prep_start;
  union db_prod global_real_db;
  union db_prod old_real_db;
  u16 old_real_bd_prod;
  u16 old_real_pkt_prod;
  xtxdata_t guest_txdata;
  xprofq_hdr_t guest_profq_hdr;
  xprofq_freelist2_t guest_profq_fl;
  xglobal_nicq_hdr_t guest_nicq;
  xglobal_nicq_hdr_t guest_dummy_nicq;
  xnic_freelist_t guest_nfl;
  xnic_txintq_t guest_txintq;
  timerq_t pac_timerq;
  active_timerq_t active_timerq;
  ptimer_heap_t global_ptimer_heap;
  tofreeq_t prof_tofreeq;
  prof_update_q_t prof_updateq;
#if HYPACE_CONFIG_STAT
  generic_q_t nicstall_q;
  generic_q_t spurious_q;
  multi_stat_t ****prof_stat;
#endif
//  generic_q_t pkts_q;
  timer_obj_t doorbell_timer;
  // guest shared addresses to unmap at cleanup
  struct bnx2x_fp_txdata *txdata;
  void *db_vaddr;
  profq_hdr_t *profq_hdr_vaddr;
  profq_freelist2_t *profq_fl_vaddr;
  global_nicq_hdr_t *global_nicq_vaddr;
  global_nicq_hdr_t *global_dummy_nicq_vaddr;
  nic_freelist_t *global_nic_fl_vaddr;
  nic_txintq_t *nic_txintq_vaddr;
  nic_txdata_t **nic_txdata_ptr_arr;
#if CONFIG_PACER_CRYPTO == CRYPTO_MBEDTLS
  mbedtls_aes_context ctx;
  unsigned int nonce[4];
  size_t nc_off;
  unsigned char stream_block[16];
  unsigned char key[32];
  unsigned char output[ENC_BUF_SIZE]; // 92 blocks
  unsigned char input[ENC_BUF_SIZE];
#endif

#if CONFIG_PACER_CRYPTO == CRYPTO_OPENSSL
  EVP_CIPHER_CTX *e_ctx;
  EVP_CIPHER_CTX *d_ctx;
  ctr_state_t e_state;
  ctr_state_t d_state;
  unsigned char iv[16];
  unsigned char key[32];
  unsigned char output[ENC_BUF_SIZE]; // 92 blocks
  unsigned char input[ENC_BUF_SIZE];
#endif

#if SME_CONFIG_STAT
  SME_DECL_STAT(spin_count);
  SME_DECL_STAT(delayed);
  SME_DECL_STAT(event_delay);
  SME_DECL_STAT_ARR(delay_after_spin, 1);
  SME_DECL_STAT_ARR(relevant_delay_after_spin, 1);
  SME_DECL_STAT_ARR(wakeup_delay, 1);
  SME_DECL_STAT_ARR(relevant_db_wakeup_delay, 1);

  SME_DECL_STAT(active_lock_delay);
  SME_DECL_STAT(timer_lock_delay);
  SME_DECL_STAT_ARR(heap_insert_delay, 1);
  SME_DECL_STAT_ARR(heap_remove_delay, 1);

  SME_DECL_STAT_ARR(dq_one_prof, 1);
  SME_DECL_STAT_ARR(dq_insert_active, 1);

  SME_DECL_STAT_ARR(pkt_prep_delay, 1);
  SME_DECL_STAT_ARR(nicq_rm_delay, 1);
  SME_DECL_STAT_ARR(prep_real_delay, 1);
  SME_DECL_STAT_ARR(prep_dummy_delay, 1);
  SME_DECL_STAT_ARR(dq_prof_count, 1);
  SME_DECL_STAT_ARR(free_prof_count, 1);
  SME_DECL_STAT_ARR(free_prof_delay, 1);
  SME_DECL_STAT_ARR(free_dummy_cnt, 1);
  SME_DECL_STAT_ARR(free_real_cnt, 1);
  SME_DECL_STAT_ARR(first_dummy_idx, 1);
  SME_DECL_STAT_ARR(last_real_idx, 1);

  SME_DECL_STAT_ARR(real_count, 1);
  SME_DECL_STAT_ARR(dummy_count, 1);
  SME_DECL_STAT_ARR(io_count, 1);
  SME_DECL_STAT_ARR(prof_switch_count, 1);
  SME_DECL_STAT_ARR(pkt_overflow_count, 1);
  SME_DECL_STAT_ARR(other_overflow_count, 1);

  SME_DECL_STAT_ARR(tot_io_delay, 1);
  SME_DECL_STAT_ARR(tot_spin_delay, 1);
  SME_DECL_STAT_ARR(real_io_delay, 1);
  SME_DECL_STAT_ARR(db_delay, 1);
  SME_DECL_STAT_ARR(relevant_db_delay, 1);
  SME_DECL_STAT_ARR(db_delay2, 1);
  SME_DECL_STAT_ARR(tot_prep_delay, 1);

  SME_DECL_STAT_ARR(sched_install, 1);
  SME_DECL_STAT_ARR(ack_update, 1);
  SME_DECL_STAT_ARR(rtx_update, 1);
  SME_DECL_STAT_ARR(prof_update1_delay, 1);
  SME_DECL_STAT_ARR(prof_update2_delay, 1);
  SME_DECL_STAT_ARR(cwnd_update3_delay, 1);
  SME_DECL_STAT_ARR(cwnd_update4_delay, 1);

  SME_DECL_STAT_ARR(nic_overflow_delay, 1);
  SME_DECL_STAT_ARR(bufenc, 1);
  SME_DECL_STAT_ARR(cpyenc, 1);
#endif

} pacer_obj_t;

#endif /* __PACER_OBJ_H__ */
