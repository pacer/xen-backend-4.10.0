#include "tofree_q.h"
#include <xen/lib.h>

int init_tofreeq(tofreeq_t *tofreeq, int size)
{
  tofreeq->arr = (tofreeq_elem_t *) xzalloc_array(tofreeq_elem_t, size);
  if (!tofreeq->arr)
    return -1;

  tofreeq->size = size;
  tofreeq->prod_idx = 0;
  tofreeq->cons_idx = 0;
  spin_lock_init(&tofreeq->lock);

  return 0;
}

void cleanup_tofreeq(tofreeq_t *tofreeq)
{
  if (tofreeq->arr)
    xfree(tofreeq->arr);

  tofreeq->arr = NULL;
//  tofreeq->size = 0;
//  tofreeq->prod_idx = 0;
//  tofreeq->cons_idx = 0;
}

int put_tofreeq(tofreeq_t *tofreeq, tofreeq_elem_t *v)
{
  tofreeq_elem_t *e;
  if (tofreeq_full(tofreeq))
    return -1;

  if (!tofreeq->arr)
    return -1;

  spin_lock(&tofreeq->lock);

  e = &tofreeq->arr[tofreeq->prod_idx];
  memcpy((void *) e, (void *) v, sizeof(tofreeq_elem_t));
  tofreeq->prod_idx = (tofreeq->prod_idx + 1) % tofreeq->size;

  spin_unlock(&tofreeq->lock);

  return 0;
}

int get_tofreeq(tofreeq_t *tofreeq, tofreeq_elem_t *v)
{
  tofreeq_elem_t *e;
  if (tofreeq_empty(tofreeq))
    return -1;

  if (!tofreeq->arr)
    return -1;

  e = &tofreeq->arr[tofreeq->cons_idx];
  memcpy((void *) v, (void *) e, sizeof(tofreeq_elem_t));
  tofreeq->cons_idx = (tofreeq->cons_idx + 1) % tofreeq->size;

  return 0;
}

void print_tofreeq(tofreeq_t *tofreeq)
{
  int i;
  for (i = tofreeq->cons_idx; i  < tofreeq->prod_idx; i++) {
    printk(XENLOG_ERR "idx %d\n", tofreeq->arr[i].profq_idx);
  }
}

// === prof update q ===

int init_prof_update_q(prof_update_q_t *q, int size)
{
  q->arr = (prof_update_arg_t *) xzalloc_array(prof_update_arg_t, size);
  if (!q->arr)
    return -1;

  q->size = size;
  q->prod_idx = 0;
  q->cons_idx = 0;
  spin_lock_init(&q->lock);

  return 0;
}

void cleanup_prof_update_q(prof_update_q_t *q)
{
  if (q->arr)
    xfree(q->arr);

  q->arr = NULL;
//  q->size = 0;
//  q->prod_idx = 0;
//  q->cons_idx = 0;
}

int put_prof_update_q(prof_update_q_t *q, prof_update_arg_t *v)
{
  prof_update_arg_t *e;
  if (prof_update_q_full(q))
    return -1;

  if (!q->arr)
    return -1;

  spin_lock(&q->lock);

  e = &q->arr[q->prod_idx];
  memcpy((void *) e, (void *) v, sizeof(prof_update_arg_t));
  q->prod_idx = (q->prod_idx + 1) % q->size;
//  q->prod_idx += 1;

  spin_unlock(&q->lock);

  return 0;
}

int get_prof_update_q(prof_update_q_t *q, prof_update_arg_t *v)
{
  prof_update_arg_t *e;
  if (prof_update_q_empty(q))
    return -1;

  if (!q->arr)
    return -1;

  e = &q->arr[q->cons_idx];
  memcpy((void *) v, (void *) e, sizeof(prof_update_arg_t));
  q->cons_idx = (q->cons_idx + 1) % q->size;
//  q->cons_idx += 1;

  return 0;
}
