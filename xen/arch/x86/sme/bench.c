/*
 * bench.c
 *
 * created on: Jan 5, 2019
 * author: aasthakm
 *
 * benchmarking code
 */

#include <xen/guest_access.h>

#include "xconfig.h"
#include "xstats.h"
#include "xdebug.h"
#include "xtxdata.h"
#include "xnicq_hdr.h"
#include "mem_trans.h"

#include "bench.h"

bench_timer_obj_t bench_timer;
s_time_t global_start = 0;
u64 global_irq_count = 0;
DEFINE_XEN_GUEST_HANDLE(hc_spin_t);
DEFINE_XEN_GUEST_HANDLE(hc_doorbell_t);
int once = 0;
static union db_prod global_dummy_db;

#if SME_CONFIG_STAT
static SME_DECL_STAT(prefetch_ns);
static SME_DECL_STAT(irq_wakeup_delay);
static SME_DECL_STAT(full);
static SME_DECL_STAT(prep_txint);
static SME_DECL_STAT(prep_txq);
static SME_DECL_STAT(db_write);
static SME_DECL_STAT(barrier_ns);
static SME_DECL_STAT(do_io);
static SME_DECL_STAT(db_delay);
static SME_DECL_STAT(epoch_delay);
static SME_DECL_STAT(sample_ns);
#endif

static void
init_all_stats(void)
{
  SME_INIT_STAT(prefetch_ns, "prefetch latency (ns)", 0, NSEC_PER_USEC, 10);
  SME_INIT_STAT(irq_wakeup_delay, "intr wakeup delay (ns)", 0,
      60*NSEC_PER_USEC, 10);
  SME_INIT_STAT(full, "queue full", 0, 100000, 1000);
  SME_INIT_STAT(prep_txint, "prep txint (ns)", 0, NSEC_PER_USEC, 10);
  SME_INIT_STAT(prep_txq, "prep txq (ns)", 0, NSEC_PER_USEC, 10);
  SME_INIT_STAT(db_write, "doorbell write (ns)", 0, NSEC_PER_USEC, 10);
  SME_INIT_STAT(barrier_ns, "barrier latency (ns)", 0, NSEC_PER_USEC, 10);
  SME_INIT_STAT(do_io, "prep io (ns)", 0, NSEC_PER_USEC, 10);
  SME_INIT_STAT(db_delay, "db delay (ns)", 0, 60*NSEC_PER_USEC, 10);
  SME_INIT_STAT(epoch_delay, "epoch delay (ns)", 0, 60*NSEC_PER_USEC, 10);
  SME_INIT_STAT(sample_ns, "ns spike latency (ns)", 0, NSEC_PER_USEC, 10);
}

static void
cleanup_all_stats(void)
{
  SME_DEST_STAT(prefetch_ns, 1);
  SME_DEST_STAT(irq_wakeup_delay, 1);
  SME_DEST_STAT(full, 1);
  SME_DEST_STAT(prep_txint, 1);
  SME_DEST_STAT(prep_txq, 1);
  SME_DEST_STAT(db_write, 1);
  SME_DEST_STAT(barrier_ns, 1);
  SME_DEST_STAT(do_io, 1);
  SME_DEST_STAT(db_delay, 1);
  SME_DEST_STAT(epoch_delay, 1);
  SME_DEST_STAT(sample_ns, 1);
}


/*
 * ==========================
 * simple spin loop benchmark
 * ==========================
 */
void
spin_handler(void *arg)
{
  bench_timer_obj_t *bench_timer = (bench_timer_obj_t *) arg;
  s_time_t t1 = 0, t2 = 0, start = 0;
  s_time_t diff = 0;
  s_time_t max_sample = 0, max_t = 0;
  u64 local_iter_count = 0;

  local_irq_disable();

  global_irq_count++;

  start = NOW();

  while ((NOW() - start) < NSEC_PER_MSEC * bench_timer->handler_duration
      && bench_timer->xtimer.status != TIMER_STATUS_killed) {
    t1 = NOW();
    t2 = NOW();
    diff = t2 - t1;
    local_iter_count++;

    if (diff < 0) {
      xprintk(LVL_EXP, "%s\n", "Time running backwards");
      return;
    }

    if (diff > max_sample) {
      max_sample = diff;
      max_t = t1;
    }

    SME_ADD_COUNT_POINT(sample_ns, diff);
  }

  local_irq_enable();

  if (max_sample > bench_timer->spike_threshold * NSEC_PER_USEC) {
    xprintk(LVL_EXP, "T %ld G %ld I %ld S %ld"
        , max_t, global_irq_count, local_iter_count, max_sample);
  }

  // if timer killed, set_timer has no effect, and no new timer will be set
  bench_timer->true_expires =
    NOW() + (bench_timer->next_timer_delay * NSEC_PER_MSEC);
  set_timer(&bench_timer->xtimer, bench_timer->true_expires);
}

long
init_spin_bench(XEN_GUEST_HANDLE_PARAM(void) arg)
{
  int ret = 0;
  hc_spin_t xarg;
  XEN_GUEST_HANDLE_PARAM(hc_spin_t) xops = guest_handle_cast(arg, hc_spin_t);

  if (guest_handle_is_null(xops))
    return -EINVAL;

  if (!guest_handle_okay(xops, sizeof(hc_spin_t)))
    return -EFAULT;

  ret = __copy_from_guest(&xarg, xops, 1);
  if (ret != 0)
    return ret;

  // first init stats
  init_all_stats();

  // init global time relative to which subsequent time events will be logged
  global_start = NOW();
  global_irq_count = 0;

  // init timer
  bench_timer.handler_duration = xarg.handler_duration;
  bench_timer.next_timer_delay = xarg.next_timer_delay;
  bench_timer.spike_threshold = xarg.spike_threshold;
  init_timer(&(bench_timer.xtimer), spin_handler, &bench_timer, xarg.handler_core);
  bench_timer.true_expires = NOW() + bench_timer.next_timer_delay;
  set_timer(&bench_timer.xtimer, bench_timer.true_expires);

  return 0;
}

long
cleanup_spin_bench(XEN_GUEST_HANDLE_PARAM(void) arg)
{
  kill_timer(&bench_timer.xtimer);

  // pretty sure this is going to create trouble with timer handler
  cleanup_all_stats();

  return 0;
}

/*
 * ==================
 * doorbell benchmark
 * ==================
 */

struct bnx2x_fp_txdata *dummy_txdata = NULL;
void *dummy_db_vaddr = NULL;
xtxdata_t bench_dummy_txdata;
nic_txintq_t *nic_txintq = NULL;
xnic_txintq_t bench_nic_txintq;

int
prep_nic_txintq_slot(xtxdata_t *x_txdata, xnic_txintq_t *xnic_txintq)
{
  nic_txint_elem_t *nic_txint_elem = NULL;
  u16 pkt_prod = *(x_txdata->tx_pkt_prod_p);
  nic_txint_elem = xnic_txintq->queue_p[TX_BD(pkt_prod)];
  nic_txint_elem->skb_maddr = rdtsc_ordered(); // temporarily overload this field

  return 0;
}

static void
do_doorbell_io_txint(void *arg)
{
  u16 bd_prod;
  int nbd = 0;
  int iter = 0;

  bench_timer_obj_t *bench_timer = (bench_timer_obj_t *) arg;

  SME_INIT_TIMER(db_write);
  SME_INIT_TIMER(barrier_ns);
  SME_INIT_TIMER(prep_txint);
  SME_INIT_TIMER(prep_txq);
  SME_INIT_TIMER(do_io);

  SME_START_TIMER(db_write);

  writel((u32) global_dummy_db.raw, dummy_db_vaddr);

  SME_END_TIMER(db_write);

  SME_START_TIMER(barrier_ns);

  barrier();

  SME_END_TIMER(barrier_ns);

  SME_ADD_COUNT_POINT(db_delay, (NOW() - bench_timer->true_expires));

  SME_START_TIMER(do_io);

  for (iter = 0; iter < bench_timer->increment_per_op; iter++) {
    SME_START_TIMER(prep_txint);

    prep_nic_txintq_slot(&bench_dummy_txdata, &bench_nic_txintq);
    *(bench_dummy_txdata.tx_pkt_prod_p) = *(bench_dummy_txdata.tx_pkt_prod_p) + 1;

    SME_END_TIMER(prep_txint);

    SME_START_TIMER(prep_txq);

    bd_prod = TX_BD(global_dummy_db.data.prod);
    bd_prod = TX_BD(NEXT_TX_IDX(bd_prod));
    bd_prod = TX_BD(NEXT_TX_IDX(bd_prod));
    nbd = 2;
    if (TX_BD_POFF(bd_prod) < nbd)
      nbd += 1;

    global_dummy_db.data.prod += nbd;

    SME_END_TIMER(prep_txq);
  }

  SME_END_TIMER(do_io);

  SME_ADD_COUNT_POINT(epoch_delay, (NOW() - bench_timer->true_expires));
}

static void
do_doorbell_io(void *arg)
{
  u16 bd_prod;
  int nbd = 0;
  int iter = 0;

  bench_timer_obj_t *bench_timer = (bench_timer_obj_t *) arg;

  SME_INIT_TIMER(db_write);
  SME_INIT_TIMER(barrier_ns);
  SME_INIT_TIMER(prep_txq);
  SME_INIT_TIMER(do_io);

  SME_START_TIMER(do_io);

  for (iter = 0; iter < bench_timer->increment_per_op; iter++) {
    SME_START_TIMER(prep_txq);

    bd_prod = TX_BD(global_dummy_db.data.prod);
    bd_prod = TX_BD(NEXT_TX_IDX(bd_prod));
    bd_prod = TX_BD(NEXT_TX_IDX(bd_prod));
    nbd = 2;
    if (TX_BD_POFF(bd_prod) < nbd)
      nbd += 1;

    global_dummy_db.data.prod += nbd;

    SME_END_TIMER(prep_txq);
  }

  SME_START_TIMER(db_write);

  writel((u32) global_dummy_db.raw, dummy_db_vaddr);

  SME_END_TIMER(db_write);

  SME_START_TIMER(barrier_ns);

  barrier();

  SME_END_TIMER(barrier_ns);

  SME_END_TIMER(do_io);

  SME_ADD_COUNT_POINT(db_delay, (NOW() - bench_timer->true_expires));
}

void
do_doorbell_nop(void *arg)
{
  bench_timer_obj_t *bench_timer = (bench_timer_obj_t *) arg;

  SME_INIT_TIMER(db_write);
  SME_INIT_TIMER(barrier_ns);
  SME_INIT_TIMER(do_io);

  SME_START_TIMER(do_io);

  SME_START_TIMER(db_write);

  writel((u32) global_dummy_db.raw, dummy_db_vaddr);

  SME_END_TIMER(db_write);

  SME_START_TIMER(barrier_ns);

  barrier();

  SME_END_TIMER(barrier_ns);

  SME_END_TIMER(do_io);

  SME_ADD_COUNT_POINT(db_delay, (NOW() - bench_timer->true_expires));
  bench_timer = arg;
}

static void
do_doorbell_reverse_io(void *arg)
{
  u16 bd_prod;
  int nbd = 0;
  int iter = 0;

  bench_timer_obj_t *bench_timer = (bench_timer_obj_t *) arg;

  SME_INIT_TIMER(db_write);
  SME_INIT_TIMER(barrier_ns);
  SME_INIT_TIMER(prep_txq);
  SME_INIT_TIMER(do_io);

  SME_START_TIMER(db_write);

  writel((u32) global_dummy_db.raw, dummy_db_vaddr);

  SME_END_TIMER(db_write);

  SME_START_TIMER(barrier_ns);

  barrier();

  SME_END_TIMER(barrier_ns);

  SME_ADD_COUNT_POINT(db_delay, (NOW() - bench_timer->true_expires));

  SME_START_TIMER(do_io);

  for (iter = 0; iter < bench_timer->increment_per_op; iter++) {
    SME_START_TIMER(prep_txq);

    bd_prod = TX_BD(global_dummy_db.data.prod);
    bd_prod = TX_BD(NEXT_TX_IDX(bd_prod));
    bd_prod = TX_BD(NEXT_TX_IDX(bd_prod));
    nbd = 2;
    if (TX_BD_POFF(bd_prod) < nbd)
      nbd += 1;

    global_dummy_db.data.prod += nbd;

    SME_END_TIMER(prep_txq);
  }

  SME_END_TIMER(do_io);

  SME_ADD_COUNT_POINT(epoch_delay, (NOW() - bench_timer->true_expires));
}

// benchmark for doorbell register write
void
doorbell_handler(void *arg)
{
  bench_timer_obj_t *bench_timer = (bench_timer_obj_t *) arg;
  u64 local_iter_count = 0;
  s_time_t wakeup_delay = 0;

  SME_INIT_TIMER(sample_ns);
//  SME_INIT_TIMER(prefetch_ns);

  wakeup_delay = NOW() - bench_timer->true_expires;
  SME_ADD_COUNT_POINT(irq_wakeup_delay, wakeup_delay);

  local_irq_disable();

  global_irq_count++;

#if 0
  SME_START_TIMER(prefetch_ns);

  prefetchw(dummy_db_vaddr);

  SME_END_TIMER(prefetch_ns);
#endif

  while (local_iter_count < bench_timer->handler_duration
      && bench_timer->xtimer.status != TIMER_STATUS_killed) {

    SME_START_TIMER(sample_ns);

    bench_timer->do_io((void *) bench_timer);

    SME_END_TIMER(sample_ns);

    local_iter_count++;
  }

  local_irq_enable();

  // if timer killed, set_timer has no effect, and no new timer will be set
  bench_timer->true_expires =
    NOW() + (bench_timer->next_timer_delay * NSEC_PER_USEC);
  set_timer(&bench_timer->xtimer, bench_timer->true_expires);
}

long
init_doorbell_bench(XEN_GUEST_HANDLE_PARAM(void) arg)
{
  int ret = 0;
  hc_doorbell_t xarg;
  XEN_GUEST_HANDLE_PARAM(hc_doorbell_t) xops = guest_handle_cast(arg, hc_doorbell_t);

  if (guest_handle_is_null(xops))
    return -EINVAL;

  if (!guest_handle_okay(xops, sizeof(hc_doorbell_t)))
    return -EFAULT;

  ret = __copy_from_guest(&xarg, xops, 1);
  if (ret != 0)
    return ret;

  // get local vaddrs from shared memory region
  dummy_db_vaddr = ioremap((paddr_t) xarg.dummy_doorbell_maddr, 4);
  dummy_txdata = get_vaddr_from_guest_maddr(xarg.dummy_txdata_maddr);

  bench_dummy_txdata.tx_buf_ring = xzalloc_array(struct sw_tx_bd *, NUM_TX_BD);
  generate_vaddrs_from_mdesc(xarg.dummy_txbuf_md_arr, xarg.dummy_txbuf_n_md,
      sizeof(struct sw_tx_bd), (void **) bench_dummy_txdata.tx_buf_ring, NUM_TX_BD);

  bench_dummy_txdata.tx_desc_ring =
    get_vaddr_from_guest_maddr(dummy_txdata->tx_desc_mapping);
  bench_dummy_txdata.tx_db = &(dummy_txdata->tx_db);
  bench_dummy_txdata.tx_pkt_prod_p = &(dummy_txdata->tx_pkt_prod);
  bench_dummy_txdata.tx_pkt_cons_p = &(dummy_txdata->tx_pkt_cons);
  bench_dummy_txdata.tx_bd_prod_p = &(dummy_txdata->tx_bd_prod);
  bench_dummy_txdata.tx_bd_cons_p = &(dummy_txdata->tx_bd_cons);
  bench_dummy_txdata.tx_ring_size_p = &(dummy_txdata->tx_ring_size);

  nic_txintq = get_vaddr_from_guest_maddr(xarg.nic_txint_maddr);
  init_x_nic_txintq(&bench_nic_txintq, nic_txintq);

  global_irq_count = 0;
  // use local variables for doorbell state, do not write to memory
  // shared with guest, to avoid cache conflicts
  global_dummy_db.raw = dummy_txdata->tx_db.raw;

  xprintk(LVL_EXP, "pkt %u %u bd %u %u DB %u ring sz %u increment %d"
      , *(bench_dummy_txdata.tx_pkt_prod_p), *(bench_dummy_txdata.tx_pkt_cons_p)
      , *(bench_dummy_txdata.tx_bd_prod_p), *(bench_dummy_txdata.tx_bd_cons_p)
      , bench_dummy_txdata.tx_db->data.prod, *(bench_dummy_txdata.tx_ring_size_p)
      , xarg.increment
      );

  // first init stats
  init_all_stats();

  // init global time relative to which subsequent time events will be logged
  global_start = NOW();

  // init timer
  bench_timer.handler_duration = xarg.handler_duration;
  bench_timer.next_timer_delay = xarg.next_timer_delay;
  bench_timer.spike_threshold = xarg.spike_threshold;
  bench_timer.increment = xarg.increment;
  bench_timer.increment_per_op = xarg.increment_per_op;

  ret = 0;
  switch (bench_timer.increment) {
    case 0: bench_timer.do_io = &do_doorbell_nop;
            break;
    case 1: bench_timer.do_io = &do_doorbell_io;
            break;
    case 2: bench_timer.do_io = &do_doorbell_io_txint;
            break;
    case 3: bench_timer.do_io = &do_doorbell_reverse_io;
            break;
    default: xprintk(LVL_EXP, "Invalid doorbell handler %d", bench_timer.increment);
             ret = 1;
             break;
  }

  if (ret) {
    cleanup_doorbell_bench(arg);
    return -1;
  }

  init_timer(&(bench_timer.xtimer), doorbell_handler, &bench_timer,
      xarg.handler_core);
  bench_timer.true_expires = NOW() + bench_timer.next_timer_delay;
  set_timer(&bench_timer.xtimer, bench_timer.true_expires);

  return 0;
}

long
cleanup_doorbell_bench(XEN_GUEST_HANDLE_PARAM(void) arg)
{
  int i;

  kill_timer(&bench_timer.xtimer);

  // pretty sure this is going to create trouble with timer handler
  cleanup_all_stats();

  dummy_txdata->tx_db.raw = global_dummy_db.raw;
  *(bench_dummy_txdata.tx_pkt_prod_p) = *(bench_dummy_txdata.tx_pkt_cons_p);
  wmb();

  // last write required if executing reverse_io handler
  if (bench_timer.increment >= 2) {
    writel((u32) dummy_txdata->tx_db.raw, dummy_db_vaddr);
    barrier();
  }

  cleanup_x_nic_txintq(&bench_nic_txintq);
  if (nic_txintq)
    unmap_domain_page_global(nic_txintq);

  if (bench_dummy_txdata.tx_desc_ring) {
    unmap_domain_page_global(bench_dummy_txdata.tx_desc_ring);
  }

  if (bench_dummy_txdata.tx_buf_ring) {
    for (i = 0; i < NUM_TX_BD; i++) {
      if (bench_dummy_txdata.tx_buf_ring[i])
        unmap_domain_page_global(bench_dummy_txdata.tx_buf_ring[i]);
    }
    xfree(bench_dummy_txdata.tx_buf_ring);
  }

  if (dummy_txdata)
    unmap_domain_page_global(dummy_txdata);
  if (dummy_db_vaddr)
    iounmap(dummy_db_vaddr);

  return 0;
}
