/*
 * sme_hashtable.h
 *
 * created on: Nov 8, 2017
 * author: aasthakm
 *
 * hashtable for pcm lookup data structure
 */

#ifndef __SME_HASHTABLE_H__
#define __SME_HASHTABLE_H__

#include "sme_guest.h"

typedef struct hash_fn {
  uint32_t (*hash) (void *key, int klen, int seed);
  int (*lookup) (list_t *ht_list, void *key, int klen, void **entry);
  int (*free) (list_t *ht_list);
} hash_fn_t;

extern hash_fn_t pmap_hash_fn;

typedef struct sme_htable {
  int size;
  list_t *ht_list;
  hash_fn_t *ht_func;
} sme_htable_t;

int init_htable(sme_htable_t *htbl, int size, hash_fn_t *hfn);
void cleanup_htable(sme_htable_t *htbl);
int htable_key_idx(sme_htable_t *htbl, void *key, int keylen);
void htable_insert(sme_htable_t *htbl, void *key, int klen, list_t *elem_listp);
void *htable_lookup(sme_htable_t *htbl, void *key, int klen);

#endif /* __SME_HASHTABLE_H__ */
