#include <xen/lib.h>
#include <xen/time.h>
#include <xen/domain_page.h>
#include <xen/guest_access.h>

#include "xprofq_hdr.h"
#include "xprofile.h"
#include "xdebug.h"
#include "mem_trans.h"

extern sme_htable_t pmap_htbl;

/*
 * ==========
 * xprofq_hdr
 * ==========
 */

void
init_x_profq_hdr(xprofq_hdr_t *xprofq_hdr, profq_hdr_t *gprofq_hdr,
    nic_txdata_t **nic_txdata_ptr_arr)
{
  int ret = 0;

  xprofq_hdr->qsize = gprofq_hdr->qsize;
  xprofq_hdr->head = gprofq_hdr->u.sorted2.head;
  xprofq_hdr->tail = gprofq_hdr->u.sorted2.tail;
  xprofq_hdr->lock_p = &(gprofq_hdr->u.sorted2.tkt_lock);
  xprofq_hdr->queue = xzalloc_array(profq_elem_t *, xprofq_hdr->qsize);

  ret = generate_vaddrs_from_mdesc_palign(gprofq_hdr->md_arr, gprofq_hdr->n_md,
      sizeof(profq_elem_t), (void **) xprofq_hdr->queue, xprofq_hdr->qsize);

  xprintk(LVL_INFO, "XPROFQ %p Q %p Q[H]p %p Q[1]p %p addr diff %lx"
      , xprofq_hdr, xprofq_hdr->queue, xprofq_hdr->queue[0], xprofq_hdr->queue[1]
      , ((unsigned long) xprofq_hdr->queue[1] - (unsigned long) xprofq_hdr->queue[0]));

#if CONFIG_NIC_QUEUE == CONFIG_NQ_STATIC
  {
    int i, j;
    j = 0;
    for (i = 0; i < xprofq_hdr->qsize; i++) {
      // ignore sentinel elements
      if (i == 0 || i == xprofq_hdr->qsize - 1)
        continue;

      ret = generate_vaddrs_from_mdesc_palign(xprofq_hdr->queue[i]->md_arr,
          xprofq_hdr->queue[i]->n_md, sizeof(nic_txdata_t),
          (void **) &nic_txdata_ptr_arr[j], NIC_DATA_QSIZE);

      j += NIC_DATA_QSIZE;
    }
  }
#endif
}

void
cleanup_x_profq_hdr(xprofq_hdr_t *xprofq_hdr)
{
  int i = 0;

  if (xprofq_hdr->queue) {
    for (i = 0; i < xprofq_hdr->qsize; i++) {
      if (xprofq_hdr->queue[i])
        unmap_domain_page_global(xprofq_hdr->queue[i]);
    }
    xfree(xprofq_hdr->queue);
  }
}

/*
 * ================
 * xprofq_freelist2
 * ================
 */

void
init_x_profq_freelist(xprofq_freelist2_t *xprofq_fl, profq_freelist2_t *gprofq_fl)
{
  int ret = 0;

  /*
   * init profq_fl
   */
  xprofq_fl->idx_list_size = gprofq_fl->idx_list_size;
  xprofq_fl->prod_idx_p = &(gprofq_fl->prod_idx);
  xprofq_fl->cons_idx_p = &(gprofq_fl->cons_idx);
  xprofq_fl->idx_list_vaddr_p = xzalloc_array(int32_t *, xprofq_fl->idx_list_size);

  ret = generate_vaddrs_from_mdesc(gprofq_fl->md_arr, gprofq_fl->n_md,
      sizeof(int32_t), (void **) xprofq_fl->idx_list_vaddr_p,
      xprofq_fl->idx_list_size);

  xprintk(LVL_INFO, "Freelist #md %d maddr %0lx %0lx size %ld\n"
      "idx [prod %p m %0lx %d cons %p m %0lx %d] ret %d\n"
      , gprofq_fl->n_md, gprofq_fl->idx_list_maddr, gprofq_fl->md_arr[0].maddr
      , xprofq_fl->idx_list_size
      , xprofq_fl->prod_idx_p, __pa(xprofq_fl->prod_idx_p)
      , atomic_read(xprofq_fl->prod_idx_p)
      , xprofq_fl->cons_idx_p, __pa(xprofq_fl->cons_idx_p)
      , atomic_read(xprofq_fl->cons_idx_p), ret
      );
}

void
cleanup_x_profq_freelist(xprofq_freelist2_t *xprofq_fl)
{
  int i = 0;

  if (xprofq_fl->idx_list_vaddr_p) {
    for (i = 0; i < xprofq_fl->idx_list_size; i++) {
      if (xprofq_fl->idx_list_vaddr_p[i])
        unmap_domain_page_global(xprofq_fl->idx_list_vaddr_p[i]);
    }
    xfree(xprofq_fl->idx_list_vaddr_p);
  }
}

void
init_profq_key(profq_key_t *k, conn_t *c, uint64_t req_ts, int64_t first_latency)
{
  if (!k || !c)
    return;

  memcpy((void *) &k->conn_id, (void *) c, sizeof(conn_t));
  k->req_ts = req_ts;
  k->first_latency = first_latency;
}

// return 1 if k1 < k2, 0 otherwise, negative if invalid keys
int
compare_key_lt(profq_key_t *k1, profq_key_t *k2)
{
  char k1_src_ipstr[16];
  char k1_dst_ipstr[16];
  char k2_src_ipstr[16];
  char k2_dst_ipstr[16];
  if (!k1 || !k2)
    return -EINVAL;

  memset(k1_src_ipstr, 0, 16);
  memset(k1_dst_ipstr, 0, 16);
  memset(k2_src_ipstr, 0, 16);
  memset(k2_dst_ipstr, 0, 16);

  ipstr(k1->conn_id.out_ip, k1_src_ipstr);
  ipstr(k1->conn_id.dst_ip, k1_dst_ipstr);
  ipstr(k2->conn_id.out_ip, k2_src_ipstr);
  ipstr(k2->conn_id.dst_ip, k2_dst_ipstr);

#if 0
  xprintk(LVL_INFO, "k1 [%s %u, %s %u] (%ld %ld)\n"
      "k2 [%s %u, %s %u] (%ld %ld)"
      , k1_src_ipstr, k1->conn_id.out_port, k1_dst_ipstr, k1->conn_id.dst_port
      , k1->req_ts, k1->first_latency
      , k2_src_ipstr, k2->conn_id.out_port, k2_dst_ipstr, k2->conn_id.dst_port
      , k2->req_ts, k2->first_latency);
#endif

  if ((k1->req_ts + k1->first_latency) < (k2->req_ts + k2->first_latency))
    return 1;

  if ((k1->req_ts + k1->first_latency) == (k2->req_ts + k2->first_latency)) {
    if (memcmp((void *) &k1->conn_id, (void *) &k2->conn_id, sizeof(conn_t)) == 0)
      BUG();

    if (memcmp((void *) &k1->conn_id, (void *) &k2->conn_id, sizeof(conn_t)) < 0)
      return 1;
  }

  return 0;
}

// return 1 if k1 == k2, 0 otherwise, negative if invalid keys
int
compare_key_eq(profq_key_t *k1, profq_key_t *k2)
{
  char k1_src_ipstr[16];
  char k1_dst_ipstr[16];
  char k2_src_ipstr[16];
  char k2_dst_ipstr[16];
  if (!k1 || !k2)
    return -EINVAL;

  memset(k1_src_ipstr, 0, 16);
  memset(k1_dst_ipstr, 0, 16);
  memset(k2_src_ipstr, 0, 16);
  memset(k2_dst_ipstr, 0, 16);

  ipstr(k1->conn_id.out_ip, k1_src_ipstr);
  ipstr(k1->conn_id.dst_ip, k1_dst_ipstr);
  ipstr(k2->conn_id.out_ip, k2_src_ipstr);
  ipstr(k2->conn_id.dst_ip, k2_dst_ipstr);

#if 0
  xprintk(LVL_INFO, "k1 [%s %u, %s %u] (%ld %ld)\n"
      "k2 [%s %u, %s %u] (%ld %ld)"
      , k1_src_ipstr, k1->conn_id.out_port, k1_dst_ipstr, k1->conn_id.dst_port
      , k1->req_ts, k1->first_latency
      , k2_src_ipstr, k2->conn_id.out_port, k2_dst_ipstr, k2->conn_id.dst_port
      , k2->req_ts, k2->first_latency);
#endif

  if ((memcmp((void *) &k1->conn_id, (void *) &k2->conn_id, sizeof(conn_t)) == 0)
      && (k1->req_ts == k2->req_ts) && (k1->first_latency == k2->first_latency))
    return 1;

  return 0;
}

int
put_one_profq_freelist2(xprofq_freelist2_t *xprofq_fl, int32_t idx)
{
  if (!xprofq_fl)
    return -EINVAL;

  *(xprofq_fl->idx_list_vaddr_p[
    atomic_read(xprofq_fl->prod_idx_p) % xprofq_fl->idx_list_size])
    = idx;
  atomic_inc(xprofq_fl->prod_idx_p);
//  *(xprofq_fl->prod_idx_p) = (*(xprofq_fl->prod_idx_p) + 1);

  return 0;
}

int
is_marked_reference(int idx)
{
  return (int) (idx & PROF_REF_MASK);
}

int
get_unmarked_reference(int idx)
{
  return (idx & PROF_REF_UNMASK);
}

int
get_marked_reference(int idx)
{
  return (idx | PROF_REF_MASK);
}

int
search(xprofq_hdr_t *profq_hdr, profq_key_t *search_key)
{
  int idx, idx_next, idx_next_um, idx_last_unmarked, idx_last_unmarked_next;
  int ret = 0;
  int loop_count = 0;
  profq_elem_t *t_pelem = NULL;
  profile_t *t_p = NULL;
  profq_key_t t_key;
  int idx_tail;
#if SME_DEBUG_LVL <= LVL_EXP
  char k1_src_ipstr[16];
  char k1_dst_ipstr[16];
  char hbuf[64];
  int hbuf_len = 64;
  memset(k1_src_ipstr, 0, 16);
  memset(k1_dst_ipstr, 0, 16);
  memset(hbuf, 0, hbuf_len);
#endif

  do {

    // queue head node
    idx = atomic_read(&profq_hdr->head);
    // current first node (possibly tail)
    idx_next = atomic_read(&profq_hdr->queue[idx]->next);
    idx_tail = atomic_read(&profq_hdr->tail);
    // save last unmarked node we encountered
    idx_last_unmarked = idx;
    // and its current successor
    idx_last_unmarked_next = idx_next;

    // get_unmarked_reference is not required,
    // since the head node should never be marked
    t_pelem = profq_hdr->queue[idx_next];

    // tail node does not have valid pmap_prof
    if (get_unmarked_reference(idx_next) != idx_tail) {
      t_p = htable_lookup(&pmap_htbl, (void *) &t_pelem->id_ihash, sizeof(ihash_t));
      if (!t_p) {
#if SME_DEBUG_LVL <= LVL_INFO
        if (idx_next != 0) {
        uint64_t t_mfn = 0;
        prt_hash(t_pelem->id_ihash.byte, sizeof(ihash_t), hbuf);
        ipstr(t_pelem->conn_id.out_ip, k1_src_ipstr);
        ipstr(t_pelem->conn_id.dst_ip, k1_dst_ipstr);
        t_mfn = domain_page_map_to_mfn(t_pelem);

        xprintk(LVL_EXP, "BUG!!! Potentially a non-atomic ihash update\n"
            "loop cnt %d ihash %s key [%s %u, %s %u] "
            "idx %d idx.next %d idx_nxt %d idx_nxt.nxt %d %d\n"
            "t_pelem@idx_nxt %p t_pelem maddr 0x%lx headp %p %p head %d tail %d "
            "idx addr diff %lx"
            , loop_count, hbuf
            , k1_src_ipstr, t_pelem->conn_id.out_port
            , k1_dst_ipstr, t_pelem->conn_id.dst_port
            , idx, atomic_read(&profq_hdr->queue[idx]->next)
            , idx_next, atomic_read(&(profq_hdr->queue[idx_next]->next))
            , get_unmarked_reference(atomic_read(&(profq_hdr->queue[idx_next]->next)))
            , t_pelem
            , (mfn_to_maddr(t_mfn) + ((unsigned long) t_pelem & (PAGE_SIZE - 1)))
            , profq_hdr->queue, profq_hdr->queue[atomic_read(&profq_hdr->head)]
            , atomic_read(&profq_hdr->head), atomic_read(&profq_hdr->tail)
            , ((unsigned long) t_pelem - (unsigned long) profq_hdr->queue)
            );
        }
#endif
        return -1;
//        BUG();
      }

      init_profq_key(&t_key, &t_pelem->conn_id, t_pelem->req_ts,
          get_latency_for_req(t_p, 0));
    } else {
      init_profq_key(&t_key, &t_pelem->conn_id, t_pelem->req_ts, -1);
    }

    while (((idx_next_um = get_unmarked_reference(idx_next)) !=
            atomic_read(&profq_hdr->tail))
      && (is_marked_reference(atomic_read(&profq_hdr->queue[idx_next_um]->next))
          || compare_key_lt(&t_key, search_key))) {

      // idx's successor is not the tail node AND
      // either the successor is marked or its key is smaller than the search key

      if (!is_marked_reference(idx_next)) {
        // idx is not marked, save it and its successor
        idx_last_unmarked = idx;
        idx_last_unmarked_next = idx_next;
      }

      idx = idx_next_um;  // we could be skipping one or more concurrently
                          // inserted nodes between idx and idx_next
                         // that's OK, because they must have keys < search_key
      idx_next = get_unmarked_reference(atomic_read(&profq_hdr->queue[idx]->next));

      t_pelem = profq_hdr->queue[get_unmarked_reference(idx_next)];

      // tail node does not have valid pmap_prof
      if (get_unmarked_reference(idx_next) != idx_tail) {
        t_p = htable_lookup(&pmap_htbl, (void *) &t_pelem->id_ihash, sizeof(ihash_t));
        init_profq_key(&t_key, &t_pelem->conn_id, t_pelem->req_ts,
            get_latency_for_req(t_p, 0));
      } else {
        init_profq_key(&t_key, &t_pelem->conn_id, t_pelem->req_ts, -1);
      }
    }

    // either idx's successor is the tail node
    // or idx's successor is currently unmarked and has a key >= the search key
    // idx may be marked
    // idx_last_unmarked, idx_last_unmarked_next refer to
    // last unmarked node we encountered and its successor, resp.

    // idx is not marked, we're done
    if (!is_marked_reference(idx_next)) {
      return idx;
    }

    // try to delete marked nodes between idx_last_unmarked and idx_next
    ret = atomic_cmpxchg(&profq_hdr->queue[idx_last_unmarked]->next,
        idx_last_unmarked_next, idx_next_um);

    if (ret == idx_last_unmarked_next) {
      // success, we removed marked nodes
      return idx_last_unmarked;
    }

    // unable to remove marked nodes due to concurrent modifications, retry
    loop_count++;

  } while(1);
}

// remove element with smallest key >= argument key, if any
// returns the index of the removed node, or NULL if none exists
int
remove(xprofq_hdr_t *profq_hdr, profq_key_t *key)
{
  int loop_count = 0;
  int idx, idx_next, idx_nextnext;
  int ret = 0;

#if SME_DEBUG_LVL <= LVL_DBG
  profq_elem_t *t_pelem = NULL;
  char hbuf[64];
  int hbuf_len = 64;
  char k1_src_ipstr[16];
  char k1_dst_ipstr[16];
  int i_next;
  uint64_t r_mfn;
#endif

  profq_elem_t *r_pelem = NULL;
  profile_t *t_p = NULL;
  profq_key_t r_key;

  if (!profq_hdr || !key)
    return 0;

  do {
    // find index of predecessor of node with smallest key >= argument key,
    // or predecessor of tail if no such node exists
    idx = search(profq_hdr, key);
    if (idx < 0)
      return idx;

    idx_next = get_unmarked_reference(atomic_read(&profq_hdr->queue[idx]->next));

    if (idx_next == atomic_read(&profq_hdr->tail)) {
      // element not found, nothing to remove
      return -1;
    }

    // try to atomically mark the node at idx_next. It is not necessarily the
    // node with smallest key >= key under concurrent insertions
    idx_nextnext = get_unmarked_reference(
        atomic_read(&profq_hdr->queue[idx_next]->next));
    ret = atomic_cmpxchg(&profq_hdr->queue[idx_next]->next, idx_nextnext,
        get_marked_reference(idx_nextnext));
    if (ret == idx_nextnext)
      break; // success

    // another thread marked the node or changed its successor, retry
    loop_count++;

  } while(1);

  // node at idx_next is now marked, at this point we are committed to removing
  // the node atomically
  // try changing queue[idx].next to point to idx_nextnext
  ret = atomic_cmpxchg(&profq_hdr->queue[idx]->next, idx_next, idx_nextnext);

#if SME_DEBUG_LVL <= LVL_DBG
  xprintk(LVL_DBG, "l %d i %d r %d => l %d r %d => ret: %d"
      , idx, idx_next, idx_nextnext, idx
      , atomic_read(&profq_hdr->queue[idx]->next), ret);
#endif

  if (ret == idx_next) {
#if SME_DEBUG_LVL <= LVL_DBG
    t_pelem = profq_hdr->queue[get_unmarked_reference(idx_next)];
    memset(k1_src_ipstr, 0, 16);
    memset(k1_dst_ipstr, 0, 16);
    memset(hbuf, 0, hbuf_len);
    prt_hash(t_pelem->id_ihash.byte, sizeof(ihash_t), hbuf);
    ipstr(t_pelem->conn_id.out_ip, k1_src_ipstr);
    ipstr(t_pelem->conn_id.dst_ip, k1_dst_ipstr);
    i_next = atomic_read(&profq_hdr->queue[idx]->next);
    xprintk(LVL_EXP, "profile l %d d %d r %d => l %d r %d ret %d r.nxt %d ihash %s"
      , idx, idx_next, idx_nextnext, idx
      , i_next, ret, atomic_read(&profq_hdr->queue[i_next]->next), hbuf);
#endif
    return idx_next;  // success
  }
  // a concurrent thread has changed or marked the ptr pointing to idx_next
  // could be caused by one or more insertions before the marked node,
  // or because the previous node was marked
  // do the search again to clean up the marked node(s)

  r_pelem = profq_hdr->queue[idx_next];
  t_p = htable_lookup(&pmap_htbl, (void *) &r_pelem->id_ihash, sizeof(ihash_t));
  init_profq_key(&r_key, &r_pelem->conn_id, r_pelem->req_ts,
    get_latency_for_req(t_p, 0));
  if (!t_p) {
#if SME_DEBUG_LVL <= LVL_DBG
    prt_hash(r_pelem->id_ihash.byte, sizeof(ihash_t), hbuf);
    ipstr(r_pelem->conn_id.out_ip, k1_src_ipstr);
    ipstr(r_pelem->conn_id.dst_ip, k1_dst_ipstr);
    r_mfn = domain_page_map_to_mfn(r_pelem);

    xprintk(LVL_EXP, "BUG!!! loop cnt %d ihash %s key [%s %u, %s %u] "
        "idx %d idx.next %d idx_nxt %d idx_nxt.nxt %d t_pelem@idx_nxt %p "
        "t_pelem maddr %lu"
        , loop_count, hbuf
        , k1_src_ipstr, r_pelem->conn_id.out_port
        , k1_dst_ipstr, r_pelem->conn_id.dst_port
        , idx, atomic_read(&profq_hdr->queue[idx]->next)
        , idx_next, atomic_read(&profq_hdr->queue[idx_next]->next)
        , r_pelem, (r_mfn + ((unsigned long) r_pelem & (PAGE_SIZE - 1)))
        );
#endif
    BUG();
  }

  idx = search(profq_hdr, &r_key);

  return idx_next;
}

// remove element with smallest positive key
// not guaranteed to be first by the time we remove it
int
remove_first(xprofq_hdr_t *profq_hdr)
{
  profq_key_t zero_key;
  conn_t zero_conn;

  memset(&zero_conn, 0, sizeof(conn_t));
  init_profq_key(&zero_key, &zero_conn, 0, 0);

  return remove(profq_hdr, &zero_key);
}

int
remove_locked(xprofq_hdr_t *profq_hdr, int *earliest_idx)
{
  int head_idx = 0;
  int tail_idx = 0;
  int earliest_idx_next = 0;

  if (!profq_hdr || !earliest_idx)
    return 0;

  acquire_tkt_lock(profq_hdr->lock_p);
  head_idx = atomic_read(&profq_hdr->head);
  tail_idx = atomic_read(&profq_hdr->tail);

  *earliest_idx = atomic_read(&(profq_hdr->queue[head_idx]->next));
  if (*earliest_idx == tail_idx) {
    release_tkt_lock(profq_hdr->lock_p);
    return 0;
  }

  earliest_idx_next = atomic_read(&(profq_hdr->queue[*earliest_idx]->next));
  if (earliest_idx_next == 0) {
    char x_out_ipstr[16];
    char x_dst_ipstr[16];
    profq_elem_t *gprof = profq_hdr->queue[*earliest_idx];

    ipstr(gprof->conn_id.out_ip, x_out_ipstr);
    ipstr(gprof->conn_id.dst_ip, x_dst_ipstr);
    xprintk(LVL_INFO, "h.nxt %d h.nxt.nxt %d "
        "xprof [%s %u, %s %u] reqts %lu next idx %d cwnd %d"
        , *earliest_idx, earliest_idx_next
        , x_out_ipstr, gprof->conn_id.out_port
        , x_dst_ipstr, gprof->conn_id.dst_port, gprof->req_ts
        , gprof->next_timer_idx, gprof->cwnd_watermark
        );
    dump_execution_state();
  }
  atomic_set(&(profq_hdr->queue[head_idx]->next), earliest_idx_next);
  atomic_set(&(profq_hdr->queue[*earliest_idx]->next), -1);

  release_tkt_lock(profq_hdr->lock_p);

#if SME_DEBUG_LVL <= LVL_DBG
  xprintk(LVL_DBG, "earliest %d => %d", *earliest_idx, earliest_idx_next);
#endif

  return 1;
}
