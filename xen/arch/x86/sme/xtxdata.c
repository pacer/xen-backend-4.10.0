#include "xtxdata.h"
#include "xdebug.h"
#include "mem_trans.h"

extern union db_prod global_real_db;
extern union db_prod old_real_db;
extern u16 old_real_bd_prod;
extern u16 old_real_pkt_prod;

void
init_x_txdata(xtxdata_t *xtxdata, struct bnx2x_fp_txdata *gtxdata,
    sme_pacer_arg_t *xarg, int hp_i, union db_prod *global_real_db,
    union db_prod *old_real_db, u16 *old_real_bd_prod_p, u16 *old_real_pkt_prod_p)
{
  int ret = 0;
  void *txdesc_vaddr = get_vaddr_from_guest_maddr(gtxdata->tx_desc_mapping);
  xtxdata->tx_buf_ring = xzalloc_array(struct sw_tx_bd *, NUM_TX_BD);
  ret = generate_vaddrs_from_mdesc(xarg->hparr[hp_i].txbuf_md_arr,
      xarg->hparr[hp_i].txbuf_n_md, sizeof(struct sw_tx_bd),
      (void **) xtxdata->tx_buf_ring, NUM_TX_BD);

  xtxdata->tx_desc_ring = txdesc_vaddr;
  xtxdata->tx_db = &(gtxdata->tx_db);
  xtxdata->tx_pkt_prod_p = &(gtxdata->tx_pkt_prod);
  xtxdata->tx_pkt_cons_p = &(gtxdata->tx_pkt_cons);
  xtxdata->tx_bd_prod_p = &(gtxdata->tx_bd_prod);
  xtxdata->tx_bd_cons_p = &(gtxdata->tx_bd_cons);
  xtxdata->tx_ring_size_p = &(gtxdata->tx_ring_size);

  global_real_db->raw = gtxdata->tx_db.raw;
  old_real_db->raw = gtxdata->tx_db.raw;
  *(old_real_bd_prod_p) = gtxdata->tx_bd_prod;
  *(old_real_pkt_prod_p) = gtxdata->tx_pkt_prod;
  xprintk(LVL_EXP, "[HP %d] pkt %u %u bd %u %u db %u tx_desc_mapping %0lx "
      "ring guest vaddr %0llx xen vaddr %p"
      , hp_i, *(xtxdata->tx_pkt_prod_p), *(xtxdata->tx_pkt_cons_p)
      , *(xtxdata->tx_bd_prod_p), *(xtxdata->tx_bd_cons_p)
      , xtxdata->tx_db->data.prod
      , gtxdata->tx_desc_mapping, (unsigned long long) gtxdata->tx_desc_ring
      , xtxdata->tx_desc_ring);
}

void
cleanup_x_txdata(xtxdata_t *xtxdata, struct bnx2x_fp_txdata *gtxdata, int real)
{
  int i = 0;
  if (xtxdata->tx_desc_ring) {
    unmap_domain_page_global(xtxdata->tx_desc_ring);
  }
  if (xtxdata->tx_buf_ring) {
    for (i = 0; i < NUM_TX_BD; i++) {
      if (xtxdata->tx_buf_ring[i])
        unmap_domain_page_global(xtxdata->tx_buf_ring[i]);
    }
    xfree(xtxdata->tx_buf_ring);
  }

#if 0
  if (EPOCH_SIZE != 0) {
    if (real)
      gtxdata->tx_db.raw = global_real_db.raw;
    else
      gtxdata->tx_db.raw = global_dummy_db.raw;
  }
#endif
}

