/*
 * bench.h
 *
 * created on: Jan 5, 2019
 * author: aasthakm
 *
 * hypercall datastructures and others for benchmark
 */

#ifndef __BENCH_H__
#define __BENCH_H__

#include <xen/lib.h>
#include <xen/timer.h>
#include <xen/hypercall.h>
#include <asm/hypercall.h>
#include <xen/guest_access.h>

#include "sme_guest.h"

#define NSEC_PER_USEC 1000
#define NSEC_PER_MSEC (1000 * NSEC_PER_USEC)

typedef struct hc_spin {
  /*
   * core on which the timer interrupt handler must be pinned
   */
  uint64_t handler_core;
  /*
   * duration of loop in a single call of the timer interrupt handler
   */
  uint64_t handler_duration;
  /*
   * delay from NOW for which the next timer event must be set
   */
  uint64_t next_timer_delay;
  /*
   * latency threshold above which it is considered a spike (us)
   */
  uint64_t spike_threshold;
} hc_spin_t;

typedef struct hc_doorbell {
  /*
   * core on which the timer interrupt handler must be pinned
   */
  uint64_t handler_core;
  /*
   * duration of loop in a single call of the timer interrupt handler
   */
  uint64_t handler_duration;
  /*
   * delay from NOW for which the next timer event must be set
   */
  uint64_t next_timer_delay;
  /*
   * latency threshold above which it is considered a spike (us)
   */
  uint64_t spike_threshold;
  /*
   * maddr of doorbell register of NIC queue
   */
  uint64_t dummy_doorbell_maddr;
  /*
   * maddr of bnx2x_fp_txdata of NIC queue
   */
  uint64_t dummy_txdata_maddr;
  /*
   * if 0, overwrite doorbell register with the same value
   */
  uint16_t increment;
  uint16_t increment_per_op;
#define MAX_TXBUF_FRAGS 4
  uint16_t dummy_txbuf_n_md;
  mem_desc_t dummy_txbuf_md_arr[MAX_TXBUF_FRAGS];
  uint64_t nic_txint_maddr;
} hc_doorbell_t;

extern long init_spin_bench(XEN_GUEST_HANDLE_PARAM(void) arg);
extern long cleanup_spin_bench(XEN_GUEST_HANDLE_PARAM(void) arg);
extern long init_doorbell_bench(XEN_GUEST_HANDLE_PARAM(void) arg);
extern long cleanup_doorbell_bench(XEN_GUEST_HANDLE_PARAM(void) arg);

typedef struct bench_xtxdata {
  void *dummy_db;
  union db_prod *tx_db;
  int *tx_ring_size_p;
} bench_xtxdata_t;

typedef struct bench_timer_obj {
  struct timer xtimer;
  uint64_t handler_duration;
  uint64_t next_timer_delay;
  uint64_t spike_threshold;
  s_time_t true_expires;
  uint16_t increment;
  uint16_t increment_per_op;
  void (*do_io) (void *arg);
} bench_timer_obj_t;

extern bench_timer_obj_t bench_timer;

#endif /* __BENCH_H__ */
