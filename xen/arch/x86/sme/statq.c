#include "statq.h"
#include <xen/lib.h>

int init_statq(statq_t *statq, int size)
{
  statq->arr = (statq_elem_t *) xzalloc_array(statq_elem_t, size);
  if (!statq->arr)
    return -1;

  statq->size = size;
  statq->prod_idx = 0;
  statq->cons_idx = 0;

  return 0;
}

void cleanup_statq(statq_t *statq)
{
  if (statq->arr)
    xfree(statq->arr);

  statq->arr = NULL;
  statq->size = 0;
  statq->prod_idx = 0;
  statq->cons_idx = 0;
}

int put_statq(statq_t *statq, statq_elem_t *v)
{
  statq_elem_t *e;
  if (statq_full(statq))
    return -1;

  e = &statq->arr[statq->prod_idx];
  memcpy((void *) e, (void *) v, sizeof(statq_elem_t));
  statq->prod_idx = (statq->prod_idx + 1) % statq->size;

  return 0;
}

int get_statq(statq_t *statq, statq_elem_t *v)
{
  statq_elem_t *e;
  if (statq_empty(statq))
    return -1;

  e = &statq->arr[statq->cons_idx];
  memcpy((void *) v, (void *) e, sizeof(statq_elem_t));
  statq->cons_idx = (statq->cons_idx + 1) % statq->size;

  return 0;
}

void print_statq(statq_t *statq)
{
  int i;
  for (i = statq->cons_idx; i  < statq->prod_idx; i++) {
    printk(XENLOG_ERR "%ld %d\n", statq->arr[i].start_ts, statq->arr[i].latency);
  }
}

void do_print_global_pkt_q(generic_q_t *queue)
{
  int i;
  global_pkt_elem_t *e = NULL;
  for (i = queue->cons_idx; i < queue->prod_idx; i++) {
    e = (global_pkt_elem_t *) ((unsigned long) queue->elem_arr
        + (i * queue->elem_size));
    printk(XENLOG_ERR "[%u] G%d T%d (%u:%u) BD %u %u"
        " R %d D %d P %d W %d\n"
        , e->port, e->guest_prof_slot_idx, e->timer_idx, e->seqno, e->seqack
        , e->db_val, e->bd_cons, e->real_cnt, e->dummy_cnt, e->num_timers, e->cwm
        );
  }
}

void do_print_fine_stat_q(generic_q_t *queue)
{
  int i;
  fine_stat_elem_t *e = NULL;
  for (i = queue->cons_idx; i < queue->prod_idx; i++) {
    e = (fine_stat_elem_t *) ((unsigned long) queue->elem_arr
        + (i * queue->elem_size));
    printk(XENLOG_ERR "[%u] %u %u %u %d %u %d %u %d\n"
        , e->epoch_id, e->wakeup_delay, e->db_delay, e->free_delay, e->n_free
        , e->dequeue_delay, e->n_dequeue, e->prep_delay, e->n_prep
        );
  }
}

void do_print_spurious_q(generic_q_t *queue)
{
  uint64_t i;
  spurious_elem_t *e = NULL;

  if (generic_q_empty(queue))
    return;

  printk(XENLOG_ERR "=== SPURIOUS EVENTS ===\n");
  for (i = queue->cons_idx; i < queue->prod_idx; i++) {
    e = (spurious_elem_t *) ((unsigned long) queue->elem_arr
        + (i * queue->elem_size));
    printk(XENLOG_ERR "[%u %d %d %d %u] (%d %d %d %d) %lu %lu %lu %lu\n"
        , e->db, e->n_pkt, e->n_prep, e->n_other, e->overflow
        , e->n_ev1, e->n_ev2, e->n_ev3, e->n_ev4
        , e->wakeup_delay, e->db_delay, e->prep_delay, e->last_delay
        );
  }
}

void do_print_nicstall_q(generic_q_t *queue)
{
  uint64_t i;
  nicstall_elem_t *e = NULL;

  if (generic_q_empty(queue))
    return;

  printk(XENLOG_ERR "=== SPURIOUS EVENTS ===\n");
  for (i = queue->cons_idx; i < queue->prod_idx; i++) {
    e = (nicstall_elem_t *) ((unsigned long) queue->elem_arr
        + (i * queue->elem_size));
    printk(XENLOG_ERR "[%lu - %lu] %u %u %lu %d\n"
        , e->start_time, e->end_time, e->start_bd, e->start_pkt
        , (e->end_time - e->start_time), e->start_prep
        );
  }
}

void do_print_pkts_q(generic_q_t *queue)
{
  uint64_t i;
  pkts_elem_t *e = NULL;

  if (generic_q_empty(queue))
    return;

  printk(XENLOG_ERR "=== PKTS Q ===\n");
  for (i = queue->cons_idx; i < queue->prod_idx; i++) {
    e = (pkts_elem_t *) ((unsigned long) queue->elem_arr
        + (i * queue->elem_size));
    printk(XENLOG_ERR "%lu [%d %5u] %u %u %u %u %u %u %lu\n"
        , e->ts, e->pkt_type, e->ip_id, e->port, e->seqno, e->seqack
        , e->db, e->pkt_prod, e->pkt_cons
        , i
        );
  }
}
