#include "ptimer.h"
#include "xconfig.h"
#include "xdebug.h"

int
init_ptimer_heap(ptimer_heap_t *pth, int size)
{
  pth->arr = xzalloc_array(ptimer_t *, size);
  if (!pth->arr)
    return -ENOMEM;

  SET_PHEAP_LIMIT(pth, size);
  SET_PHEAP_SIZE(pth, 0);
  spin_lock_init(&pth->heap_lock);

  return 0;
}

void
cleanup_ptimer_heap(ptimer_heap_t *pth)
{
  if (!pth || !pth->arr)
    return;

  xfree(pth->arr);
  pth->arr = NULL;
  SET_PHEAP_LIMIT(pth, 0);
  SET_PHEAP_SIZE(pth, 0);
}

static void
down_ptimer_heap(ptimer_heap_t *pth, int pos)
{
  int sz, nxt;
  ptimer_t *pt = NULL;

  sz = GET_PHEAP_SIZE(pth);
  pt = pth->arr[pos];

  while ((nxt = (pos << 1)) <= sz) {
    if (((nxt+1) <= sz) && (pth->arr[nxt+1]->expires < pth->arr[nxt]->expires))
      nxt++;

    if (pth->arr[nxt]->expires > pt->expires)
      break;

    pth->arr[pos] = pth->arr[nxt];
    pth->arr[pos]->heap_offset = pos;
    pos = nxt;
  }

  pth->arr[pos] = pt;
  pt->heap_offset = pos;
}

static void
up_ptimer_heap(ptimer_heap_t *pth, int pos)
{
  ptimer_t *pt = pth->arr[pos];
  while ((pos > 1) && (pt->expires < pth->arr[pos>>1]->expires)) {
    pth->arr[pos] = pth->arr[pos>>1];
    pth->arr[pos]->heap_offset = pos;
    pos >>=1;
  }

  pth->arr[pos] = pt;
  pt->heap_offset = pos;
}

int
remove_from_ptimer_heap(ptimer_heap_t *pth, ptimer_t *pt)
{
  int sz = GET_PHEAP_SIZE(pth);
  int pos = pt->heap_offset;

  //xprintk(LVL_EXP, "pt %p sz %d pos %d pth[%d] %p pth[%d] %p"
  //    , pt, sz, pos, sz, pth->arr[sz], pos, pth->arr[pos]);

  xprintk(LVL_DBG, "pt %p sz %d pos %d limit %d pth->arr %p"
      , pt, sz, pos, GET_PHEAP_LIMIT(pth), pth->arr);

  // pt already not in heap, make it a nop
  // needed for active_timerq_free_profile
  if (pos == 0 || sz == 0)
    return 0;

  if (unlikely(pos == sz)) {
    SET_PHEAP_SIZE(pth, sz-1);
    goto out;
  }

  pth->arr[pos] = pth->arr[sz];
  pth->arr[pos]->heap_offset = pos;

  SET_PHEAP_SIZE(pth, --sz);

  if ((pos > 1) && (pth->arr[pos]->expires < pth->arr[pos>>1]->expires))
    up_ptimer_heap(pth, pos);
  else
    down_ptimer_heap(pth, pos);

out:
  pt->heap_offset = 0;
  return (pos == 1);
}

int
add_to_ptimer_heap(ptimer_heap_t *pth, ptimer_t *pt)
{
  int sz = GET_PHEAP_SIZE(pth);

  if (unlikely(sz == GET_PHEAP_LIMIT(pth)))
    return 0;

  SET_PHEAP_SIZE(pth, ++sz);
  pth->arr[sz] = pt;
  pt->heap_offset = sz;
  up_ptimer_heap(pth, sz);

  xprintk(LVL_DBG, "pt %p sz %d pos %d pth[%d] %p pth[0] %p"
      , pt, sz, pt->heap_offset, sz, pth->arr[sz], pth->arr[0]);

  return (pt->heap_offset == 1);
}

int
set_ptimer(ptimer_heap_t *pth, ptimer_t *pt, s_time_t expires, void *tobj)
{
  int ret = 0;
  pt->expires = expires;
  pt->heap_offset = 0;
  pt->parent_tobj = tobj;
  ret = add_to_ptimer_heap(pth, pt);
  if (pt->heap_offset != 0)
    return ret;

  return -ENOMEM;
}

void
stop_ptimer(ptimer_heap_t *pth, ptimer_t *pt)
{
  remove_from_ptimer_heap(pth, pt);
}
