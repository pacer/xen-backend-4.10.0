#!/bin/bash

ncore=64

date;
sudo make install -j $ncore > make_install.out 2>&1
sudo chown -R sme:sme ./tools/qemu-xen-traditional-dir/config-host.h.new
date;
